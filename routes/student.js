var express = require('express');
var router = express.Router();

// Model
var activityModel = require('../models/Activity');
var materialModel = require('../models/Material');
var collectionModel = require('../models/Collection');
var logModel = require('../models/Log');
var config = require('../config');
var async = require('async');

//學生探索活動
router.get('/activity/discover', function(req, res, next){
  if(req.session.user){
    activityModel.studentGetDiscoverActivities({
      search:req.body.q,
      start:0,
      end:10,
      sort:'activity.create_date',
      order:'desc',
      teacher_id:null,
      subject_id:null,
      type:null
    }, function(err, data){
      res.render('student/discover',{ 
        title: 'Student Discover', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activities:data,
        current:1
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生取得我的活動
router.get('/activity', function(req, res, next) {
  if(req.session.user){
    var category = req.query.category; //all,public,collect
    switch(category){
      case 'public':
        async.auto({
          getCollections: function(callback){
            collectionModel.getCollections([req.session.user.userId], function(err,data){
              callback(null,data);
            });  
          },
          getActivities: function(callback){
            activityModel.studentGetJoinActivities({
              user_cooc_id:req.session.user.userId,
              search:req.body.q,
              start:0,
              end:10,
              sort:'student_activity_join.join_date',
              order:'desc',
              teacher_id:null,
              subject_id:null,
              type:null,
              progress:null
            }, function(err, data){
              callback(null, data);
            })
          }
        }, function(error, results){
          res.render('student',{ 
            title: 'Student', 
            cooc_header:req.app.locals.cooc_header_login,
            cooc_footer:req.app.locals.cooc_footer,
            user:req.session.user,
            activities:results['getActivities'],
            category:'public',
            current:req.query.p,
            collections:results['getCollections']
          });
        });
        break;
      default:
        async.auto({
          getCollections: function(callback){
            collectionModel.getCollections([req.session.user.userId], function(err,data){
              callback(null,data);
            });  
          },
          getBelongGroupIds: function(callback){
            activityModel.studentGetBelongGroupIds([req.session.user.userId], function(err, data){
              var ids = [];
              for(var i=0;i<data.length;i++){
                ids.push(data[i].group_id);
              }
              callback(null, ids);
            });
          },
          getActivities: ['getBelongGroupIds', function(results, callback){
            activityModel.studentGetAssignActivities({
              user_cooc_id:req.session.user.userId,
              activityIds:results['getBelongGroupIds'],
              search:req.body.q,
              start:0,
              end:10,
              sort:'activity_assign.assign_date',
              order:'desc',
              teacher_id:null,
              subject_id:null,
              type:null,
              progress:null
            }, function(err, data){
              callback(null, data);
            });
          }]
        }, function(error, results){
          res.render('student',{ 
            title: 'Student', 
            cooc_header:req.app.locals.cooc_header_login,
            cooc_footer:req.app.locals.cooc_footer,
            user:req.session.user,
            activities:results['getActivities'],
            category:'assign',
            current:req.query.p,
            collections:results['getCollections']
          });
        });
    }
  }else{
    res.redirect('/');
  }
});

//學生取得收藏活動
router.get('/activity/collect', function(req, res, next){
  if(req.session.user){
    activityModel.studentGetCollectActivity([req.session.user.userId], function(err,data){
      res.render('student/activity_collect',{ 
        title: 'Student', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        collections: data
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生從探索活動進入檢視活動
router.get('/activity/view/:creator_id/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      checkStudentActivityJoined: function(callback){
        activityModel.checkStudentActivityJoined([
          req.params.activity_id,
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      },
      getBelongGroupIds: function(callback){
        activityModel.studentGetBelongGroupIds([req.session.user.userId], function(err, data){
          var ids = [];
          for(var i=0;i<data.length;i++){
            ids.push(data[i].group_id);
          }
          callback(null, ids);
        });
      },
      checkStudentActivityAssigned: ['getBelongGroupIds', function(results, callback){
        activityModel.checkStudentActivityAssigned([
          req.params.activity_id,
          req.params.creator_id,
          results['getBelongGroupIds'],
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      }],
    }, function(err, checkResults){
      if(checkResults['checkStudentActivityJoined']==0 && checkResults['checkStudentActivityAssigned']==0){
        async.auto({
          studentGetActivityDetails: function(callback){
            activityModel.studentGetActivityDetails([
              req.params.activity_id
            ], function(err, data){
              callback(null, data);
            });
          },
          studentGetMaterials: ['studentGetActivityDetails', function(results, callback){
            materialModel.studentGetMaterialsByUserId([req.params.activity_id], function(err, data){
              results['studentGetActivityDetails']['materials'] = data;
              callback(null, results['studentGetActivityDetails']);
            })
          }]
        }, function(err1, results1){
          res.render('student/activity_view',{ 
            title: 'Student Activity', 
            cooc_header:req.app.locals.cooc_header_login,
            cooc_footer:req.app.locals.cooc_footer,
            user:req.session.user,
            activity:results1['studentGetActivityDetails']
          });
        });
      }else{
        async.auto({
          getActivityCreator: function(callback){
            activityModel.getActivityCreator([req.params.activity_id], function(err, data){
              callback(null, data);
            });
          },
          activityJoin: ['getActivityCreator', function(results, callback){
            activityModel.studentInsertActivityJoin([
              req.params.activity_id,
              results['getActivityCreator'].creator_id,
              results['getActivityCreator'].creator_cooc_id,
              req.session.user.id,
              req.session.user.userId,
              new Date(),
              req.params.activity_id,
              req.session.user.userId
            ], function(err, data){
              callback(null, data);
            });
          }],
          studentGetJoinedActivityDetails: ['activityJoin', function(results, callback){
            activityModel.studentGetJoinedActivityDetails([
              req.params.activity_id,
              req.session.user.userId
            ], function(err,data){
              callback(null, data);
            });
          }],
          studentGetMaterials: ['studentGetJoinedActivityDetails', function(results, callback){
            materialModel.studentGetMaterialsByUserId([req.params.activity_id], function(err, data){
              results['studentGetJoinedActivityDetails']['materials'] = data;
              callback(null, results['studentGetActivityDetails']);
            })
          }]
        }, function(err, results){
          res.render('student/activity',{ 
            title: 'Student Activity', 
            cooc_header:req.app.locals.cooc_header_login,
            cooc_footer:req.app.locals.cooc_footer,
            user:req.session.user,
            activity:results['studentGetJoinedActivityDetails']
          });
        });
      }
    });
  }else{
    res.redirect('/');
  }
});

//學生從我的活動進入被指派活動(一般活動)
router.get('/activity/assign/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityCreator: function(callback){
        activityModel.getActivityCreator([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      activityJoin: ['getActivityCreator', function(results, callback){
        activityModel.studentInsertActivityJoin([
          req.params.activity_id,
          results['getActivityCreator'].creator_id,
          results['getActivityCreator'].creator_cooc_id,
          req.session.user.id,
          req.session.user.userId,
          new Date(),
          req.params.activity_id,
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      }],
      studentGetJoinedActivityDetails: ['activityJoin', function(results, callback){
        activityModel.studentGetJoinedActivityDetails([
          req.params.activity_id,
          req.session.user.userId
        ], function(err,data){
          callback(null, data);
        });
      }],
      studentGetMaterials: ['studentGetJoinedActivityDetails', function(results, callback){
        materialModel.studentGetMaterialDetailWithProgress([
          req.session.user.userId,
          req.params.activity_id
        ], function(err, data){
          results['studentGetJoinedActivityDetails']['materials'] = data;
          callback(null, results['studentGetActivityDetails']);
        });
      }]
    }, function(err, results){
      res.render('student/activity',{ 
        title: 'Student Activity', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['studentGetJoinedActivityDetails']
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生從我的活動進入被指派活動(適性活動)
router.get('/activity/assign/adaptive/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      studentGetAdaptiveMaterialProgress: function(callback){
        materialModel.studentGetAdaptiveMaterialProgress([
          req.params.activity_id,
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      },
      getActivityCreator: function(callback){
        activityModel.getActivityCreator([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      activityJoin: ['getActivityCreator', function(results, callback){
        activityModel.studentInsertActivityJoin([
          req.params.activity_id,
          results['getActivityCreator'].creator_id,
          results['getActivityCreator'].creator_cooc_id,
          req.session.user.id,
          req.session.user.userId,
          new Date(),
          req.params.activity_id,
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      }],
      studentGetJoinedActivityDetails: ['activityJoin', function(results, callback){
        activityModel.studentGetJoinedActivityDetails([
          req.params.activity_id,
          req.session.user.userId
        ], function(err,data){
          callback(null, data);
        });
      }]
    }, function(err, results){
      res.render('student/adaptive_activity_view',{ 
        title: 'Student Activity', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['studentGetJoinedActivityDetails'],
        progress:results['studentGetAdaptiveMaterialProgress']
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生從我的活動進入被指派活動(前後測活動)
router.get('/activity/assign/assessment/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityCreator: function(callback){
        activityModel.getActivityCreator([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      activityJoin: ['getActivityCreator', function(results, callback){
        activityModel.studentInsertActivityJoin([
          req.params.activity_id,
          results['getActivityCreator'].creator_id,
          results['getActivityCreator'].creator_cooc_id,
          req.session.user.id,
          req.session.user.userId,
          new Date(),
          req.params.activity_id,
          req.session.user.userId
        ], function(err, data){
          callback(null, data);
        });
      }],
      studentGetJoinedActivityDetails: ['activityJoin', function(results, callback){
        activityModel.studentGetJoinedActivityDetails([
          req.params.activity_id,
          req.session.user.userId
        ], function(err,data){
          callback(null, data);
        });
      }],
      studentGetMaterials: ['studentGetJoinedActivityDetails', function(results, callback){
        materialModel.studentGetMaterialDetailWithProgress([
          req.session.user.userId,
          req.params.activity_id
        ], function(err, data){
          results['studentGetJoinedActivityDetails']['materials'] = data;
          callback(null, results['studentGetActivityDetails']);
        });
      }]
    }, function(err, results){
      res.render('student/adaptive_activity_view',{ 
        title: 'Student Activity', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['studentGetJoinedActivityDetails']
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生從我的活動進入我的公開活動
router.get('/activity/public/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      studentGetJoinedActivityDetails: function(callback){
        activityModel.studentGetJoinedActivityDetails([
          req.params.activity_id,
          req.session.user.userId
        ], function(err,data){
          callback(null, data);
        });
      },
      studentGetMaterials: ['studentGetJoinedActivityDetails', function(results, callback){
        materialModel.studentGetMaterialsByUserId([req.params.activity_id], function(err, data){
          results['studentGetJoinedActivityDetails']['materials'] = data;
          callback(null, results['studentGetJoinedActivityDetails']);
        })
      }]
    }, function(err, results){
      res.render('student/activity',{ 
        title: 'Student Activity', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['studentGetJoinedActivityDetails']
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生進行活動學習
router.get('/activity/:activity_id/play/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      studentGetActivityDetails: function(callback){
        activityModel.studentGetActivityDetails([req.params.activity_id], function(err,data){
          callback(null, data);
        });
      },
      studentGetMaterialNotes: function(callback){
        logModel.getMaterialNotes([req.params.material_id,req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      studentLearnMaterial: function(callback){
        materialModel.studentLearnMaterial([
          req.params.activity_id,
          req.session.user.id,
          req.session.user.userId,
          new Date(),
          req.params.material_id,
          new Date()
        ], function(err, data){
          callback(null, data);
        }); 
      },
      studentGetMaterials: ['studentGetActivityDetails', 'studentLearnMaterial', function(results,callback){
        materialModel.studentGetMaterialDetailWithProgress([
          req.session.user.userId,
          req.params.activity_id
        ], function(err, data){
          results['studentGetActivityDetails']['materials'] = data;
          callback(null, results['studentGetActivityDetails']);
        });
      }]
    }, function(err, results){
      var materials = results['studentGetActivityDetails']['materials'];
      var renderPage = 'student/activity';
      for(var i=0;i<materials.length;i++){
        var materialType = materials[i]['type'];
        if(materials[i].id==req.params.material_id){
          switch(materialType){
            case 1: //link
              renderPage = 'student/play_web';
              break;
            case 2: //video
              renderPage = 'student/play_video';
              break;
            case 3: //pdf
              renderPage = 'student/play_pdf';
              break;
            case 4: //assessment
              renderPage = 'student/play_assessment';
              break;
            case 5: //article
              renderPage = 'student/play_article';
              break;
          }
          break;
        }
      }
      if(renderPage.indexOf('assessment')!=-1){
        activityModel.studentGetAssessmentRecords([
          req.params.activity_id,
          req.params.material_id,
          req.session.user.userId
        ], function(erra, dataa){
          res.render(renderPage,{ 
            title: 'Play Activity', 
            user:req.session.user,
            activity:results['studentGetActivityDetails'],
            material_id:req.params.material_id,
            host:req.protocol+'://'+req.hostname,
            teacher:false,
            notes:results['studentGetMaterialNotes'],
            assessment_results:dataa,
            lrs:config.xapi.lrs
          });
        });
      }else{
        res.render(renderPage,{ 
          title: 'Play Activity', 
          user:req.session.user,
          activity:results['studentGetActivityDetails'],
          material_id:req.params.material_id,
          host:req.protocol+'://'+req.hostname,
          teacher:false,
          notes:results['studentGetMaterialNotes']
        });
      }
      
    });
  }else{
    res.redirect('/');
  }
});

//學生進行適性活動學習
router.get('/activity/adaptive/:activity_id/play/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      checkStudentAdaptiveActivityProgress: function(callback){
        activityModel.checkStudentAdaptiveActivityProgress([
          req.session.user.id,
          req.params.activity_id,
          req.query.id
        ], function(err, data){
          callback(null, data);
        });
      },
      recordStudentAdaptiveActivityProgress:['checkStudentAdaptiveActivityProgress', function(results, callback){
        if(results['checkStudentAdaptiveActivityProgress']==0){
          activityModel.recordStudentAdaptiveActivityProgress([
            req.session.user.userId,
            req.params.activity_id,
            req.query.id
          ], function(err, data){
            callback(null, data);
          });
        }else{
          callback(null, true);
        }
      }],
      studentGetActivityDetails: function(callback){
        activityModel.studentGetActivityDetails([req.params.activity_id], function(err,data){
          callback(null, data);
        });
      },
      studentGetMaterialNotes: function(callback){
        logModel.getAdaptiveMaterialNotes([
          req.params.material_id,
          req.session.user.userId,
          req.query.id
        ], function(err,data){
          callback(null,data);
        });
      },
      studentLearnAdaptiveMaterial: function(callback){
        materialModel.studentLearnAdaptiveMaterial([
          req.params.activity_id,
          req.session.user.userId,
          new Date(),
          req.params.material_id,
          req.query.id,
          new Date()
        ], function(err, data){
          callback(null, data);
        }); 
      },
      studentGetTargetAdaptiveMaterial: function(callback){
        materialModel.studentGetTargetAdaptiveMaterial([
          req.params.activity_id,
          req.session.user.userId,
          req.params.material_id
        ], function(err, data){
          callback(null, data);
        });
      }
    }, function(err, results){
      var targetMaterial = results['studentGetTargetAdaptiveMaterial'];
      var renderPage = 'student/activity/assign/adaptive/'+req.params.activity_id;
      switch(targetMaterial.type){
        case 1: //link
          renderPage = 'student/play_web_adaptive';
          break;
        case 2: //video
          renderPage = 'student/play_video_adaptive';
          break;
        case 3: //pdf
          renderPage = 'student/play_pdf_adaptive';
          break;
        case 4: //assessment
          renderPage = 'student/play_assessment_adaptive';
          break;
        case 5: //article
          renderPage = 'student/play_article_adaptive';
          break;
      }
      if(renderPage.indexOf('assessment')!=-1){
        activityModel.studentGetAdaptiveAssessmentRecords([
          req.params.activity_id,
          req.params.material_id,
          req.session.user.userId,
          req.query.id
        ], function(erra, dataa){
          res.render(renderPage,{ 
            title: 'Play Activity', 
            user:req.session.user,
            activity:results['studentGetActivityDetails'],
            material_id:req.params.material_id,
            host:req.protocol+'://'+req.hostname,
            teacher:false,
            notes:results['studentGetMaterialNotes'],
            assessment_results:dataa,
            lrs:config.xapi.lrs,
            targetMaterial:targetMaterial,
            material_uuid:req.query.id
          });
        });
      }else{
        res.render(renderPage,{ 
          title: 'Play Activity', 
          user:req.session.user,
          activity:results['studentGetActivityDetails'],
          material_id:req.params.material_id,
          host:req.protocol+'://'+req.hostname,
          teacher:false,
          notes:results['studentGetMaterialNotes'],
          targetMaterial:targetMaterial,
          material_uuid:req.query.id
        });
      }
      
    });
  }else{
    res.redirect('/');
  }
});

//學生儲存文章(適性)
router.post('/activity/:activity_id/adaptive/article/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      saveArticle: function(callback){
        materialModel.saveArticle([
          req.params.activity_id,
          req.params.material_id,
          req.session.user.id,
          req.session.user.userId,
          req.body.article,
          req.body.time_spent,
          req.body.material_uuid
        ], function(err,data){
          callback(data);
        });
      },
      updateArticleStatus: function(callback){
        materialModel.studentUpdateAdaptiveArticleStatus([
          req.body.total_time_spent,
          req.params.activity_id,
          req.params.material_id,
          req.session.user.userId,
          req.body.material_uuid
        ], function(err, data){
          callback(data);
        });
      }
    }, function(err, results){
      res.json({success:true});
    });
  }else{
    res.redirect('/');
  }
});

//學生儲存文章
router.post('/activity/:activity_id/article/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      saveArticle: function(callback){
        materialModel.saveArticle([
          req.params.activity_id,
          req.params.material_id,
          req.session.user.id,
          req.session.user.userId,
          req.body.article,
          req.body.time_spent
        ], function(err,data){
          callback(data);
        });
      },
      updateArticleStatus: function(callback){
        materialModel.studentUpdateArticleStatus([
          req.body.total_time_spent,
          req.params.activity_id,
          req.params.material_id,
          req.session.user.userId
        ], function(err, data){
          callback(data);
        });
      },
      studentUpdateActivityCompletion: function(callback){
        if(req.body.is_completion!=='true'){
          activityModel.studentUpdateActivityCompletion([
            req.body.activity_completion,
            req.body.time_spent,
            req.params.activity_id,
            req.session.user.userId
          ], function(err, data){
              callback(data);
          });
        }else{
          callback(true);
        }
      }
    }, function(err, results){
      res.json({success:true});
    });
  }else{
    res.redirect('/');
  }
});
//學生取得文章
router.get('/activity/:activity_id/article/:material_id', function(req, res, next){
  if(req.session.user){
    materialModel.getLatestArticleByUserId([
      req.params.activity_id,
      req.params.material_id,
      req.session.user.userId
    ], function(err,data){
      res.json({content:data});
    });
  }else{
    res.redirect('/');
  }
});

//學生繳交測驗
router.post('/assessment/submit', function(req, res, next){
  if(req.session.user){
    async.auto({
      studentSubmitAssessment: function(callback){
        if(req.body.material_uuid){
          activityModel.studentSubmitAdaptiveAssessment([
            req.body.activity_id,
            req.body.material_id,
            req.body.score,
            req.body.time_spent,
            req.session.user.id,
            req.session.user.userId,
            req.body.material_uuid
          ], function(err, data){
            callback(null, data);
          });
        }else{
          activityModel.studentSubmitAssessment([
            req.body.activity_id,
            req.body.material_id,
            req.body.score,
            req.body.time_spent,
            req.session.user.id,
            req.session.user.userId
          ], function(err, data){
            callback(null, data);
          });
        }
      },
      studentUpdateAssessmentStatus: function(callback){
        if(req.body.material_uuid){
          logModel.studentUpdateAdaptiveAssessmentStatus([
            req.body.time_spent,
            req.body.activity_id,
            req.body.material_id,
            req.session.user.userId,
            req.body.material_uuid
          ], function(err, data){
            callback(null, data);
          });
        }else{
          logModel.studentUpdateAssessmentStatus([
            req.body.time_spent,
            req.body.activity_id,
            req.body.material_id,
            req.session.user.userId
          ], function(err, data){
            callback(null, data);
          });
        }
      }
    }, function(err, results){
      if(req.body.is_teacher=='false'){
        if(req.body.is_adaptive=='false'){
          res.redirect('/student/activity/'+req.body.activity_id+'/play/'+req.body.material_id);
        }else{
          res.redirect('/student/activity/adaptive/'+req.body.activity_id+'/play/'+req.body.material_id+'?id='+req.body.material_uuid);
        }
      }else{
        res.redirect('/teacher/activity/preview/'+req.body.activity_id+'/'+req.body.material_id);
      }
    });
  }else{
    res.redirect('/');
  }
});

module.exports = router;