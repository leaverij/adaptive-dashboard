var express = require('express');
var router = express.Router();

var activityModel = require('../models/Activity');
var forumModel = require('../models/Forum');
var async = require('async');

router.get('/', function(req, res, next){
    res.send('forum router');
});

//進入活動討論區
router.get('/teacher/activity/:activity_id/questions', function(req, res, next){
    if(req.session.user){
        async.auto({
            getAllQuestionCounts: function(callback){
                forumModel.getAllQuestionCounts([req.params.activity_id], function(err,data){
                    callback(null, data);
                })
            },
            getAllTraceQuestionCounts: function(callback){
                forumModel.getAllTraceQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getMyQuestionCounts: function(callback){
                forumModel.getMyQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getActivityDetails: function(callback){
                activityModel.getActivityDetails([req.params.activity_id],function(err,data){
                  callback(null,data);
                });
            },
            getForumQuestions: function(callback){
                forumModel.getQuestions({
                    activity_id:req.params.activity_id,
                    search:req.query.q,
                    sort:'post_date',
                    order:'desc',
                    start:0,
                    end:10
                }, function(err,data){
                    callback(null, data);
                });
            },
            getHotQuestions:function(callback){
                forumModel.getHotQuestions([req.params.activity_id], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.render('forum', {
                title: 'Forum', 
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                activity:results['getActivityDetails'],
                questions:results['getForumQuestions'],
                hot_questions:results['getHotQuestions'],
                search:false,
                trace:false,
                my:false,
                question_counts:results['getAllQuestionCounts'],
                trace_question_counts:results['getAllTraceQuestionCounts'],
                my_question_counts:results['getMyQuestionCounts']
            });
        });
    }else{
        res.redirect('/');
    }
});

//搜尋討論區問題
router.post('/teacher/activity/:activity_id/questions', function(req, res, next){
    if(req.session.user){
        async.auto({
            getAllQuestionCounts: function(callback){
                forumModel.getAllQuestionCounts([req.params.activity_id], function(err,data){
                    callback(null, data);
                })
            },
            getAllTraceQuestionCounts: function(callback){
                forumModel.getAllTraceQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getMyQuestionCounts: function(callback){
                forumModel.getMyQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getActivityDetails: function(callback){
                activityModel.getActivityDetails([req.params.activity_id],function(err,data){
                  callback(null,data);
                });
            },
            getForumQuestions: function(callback){
                forumModel.getQuestions({
                    activity_id:req.params.activity_id,
                    search:(req.body.q)?req.body.q:'',
                    sort:'post_date',
                    order:'desc',
                    start:0,
                    end:10
                }, function(err,data){
                    callback(null, data);
                });
            },
            getHotQuestions:function(callback){
                forumModel.getHotQuestions([req.params.activity_id], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.render('forum', {
                title: 'Forum', 
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                activity:results['getActivityDetails'],
                questions:results['getForumQuestions'],
                search:true,
                trace:false,
                my:false,
                searchVal:(req.body.q)?req.body.q:'',
                hot_questions:results['getHotQuestions'],
                question_counts:results['getAllQuestionCounts'],
                trace_question_counts:results['getAllTraceQuestionCounts'],
                my_question_counts:results['getMyQuestionCounts']
            });
        });
    }else{
        res.redirect('/');
    }
});

//點擊已加星號問題
router.get('/teacher/activity/:activity_id/questions/trace', function(req, res, next){
    if(req.session.user){
        async.auto({
            getAllQuestionCounts: function(callback){
                forumModel.getAllQuestionCounts([req.params.activity_id], function(err,data){
                    callback(null, data);
                })
            },
            getAllTraceQuestionCounts: function(callback){
                forumModel.getAllTraceQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getMyQuestionCounts: function(callback){
                forumModel.getMyQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getActivityDetails: function(callback){
                activityModel.getActivityDetails([req.params.activity_id],function(err,data){
                  callback(null,data);
                });
            },
            getForumQuestions: function(callback){
                forumModel.getTraceQuestions({
                    activity_id:req.params.activity_id,
                    user_cooc_id:req.session.user.userId,
                    search:(req.body.q)?req.body.q:'',
                    sort:'post_date',
                    order:'desc',
                    start:0,
                    end:10
                }, function(err,data){
                    callback(null, data);
                });
            },
            getHotQuestions:function(callback){
                forumModel.getHotQuestions([req.params.activity_id], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.render('forum', {
                title: 'Forum', 
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                activity:results['getActivityDetails'],
                questions:results['getForumQuestions'],
                search:false,
                trace:true,
                my:false,
                searchVal:(req.body.q)?req.body.q:'',
                hot_questions:results['getHotQuestions'],
                question_counts:results['getAllQuestionCounts'],
                trace_question_counts:results['getAllTraceQuestionCounts'],
                my_question_counts:results['getMyQuestionCounts']
            });
        });
    }else{
        res.redirect('/');
    }
});

//點擊我的問題
router.get('/teacher/activity/:activity_id/questions/my', function(req, res, next){
    if(req.session.user){
        async.auto({
            getAllQuestionCounts: function(callback){
                forumModel.getAllQuestionCounts([req.params.activity_id], function(err,data){
                    callback(null, data);
                })
            },
            getAllTraceQuestionCounts: function(callback){
                forumModel.getAllTraceQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getMyQuestionCounts: function(callback){
                forumModel.getMyQuestionCounts([
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null, data);
                });
            },
            getActivityDetails: function(callback){
                activityModel.getActivityDetails([req.params.activity_id],function(err,data){
                  callback(null,data);
                });
            },
            getForumQuestions: function(callback){
                forumModel.getMyQuestions({
                    activity_id:req.params.activity_id,
                    user_cooc_id:req.session.user.userId,
                    search:(req.body.q)?req.body.q:'',
                    sort:'post_date',
                    order:'desc',
                    start:0,
                    end:10
                }, function(err,data){
                    callback(null, data);
                });
            },
            getHotQuestions:function(callback){
                forumModel.getHotQuestions([req.params.activity_id], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.render('forum', {
                title: 'Forum', 
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                activity:results['getActivityDetails'],
                questions:results['getForumQuestions'],
                search:false,
                trace:false,
                my:true,
                searchVal:(req.body.q)?req.body.q:'',
                hot_questions:results['getHotQuestions'],
                question_counts:results['getAllQuestionCounts'],
                trace_question_counts:results['getAllTraceQuestionCounts'],
                my_question_counts:results['getMyQuestionCounts']
            });
        });
    }else{
        res.redirect('/');
    }
});

//進入討論區問題
router.get('/teacher/activity/:activity_id/questions/:question_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            getActivityDetails: function(callback){
                activityModel.getActivityDetails([req.params.activity_id],function(err,data){
                  callback(null,data);
                });
            },
            viewQuestion: function(callback){
                forumModel.updateQuestionViewCounts([
                    req.params.question_id,
                    req.params.activity_id
                ], function(err,data){
                    callback(null,true);
                });
            },
            getSingleQuestionDetail: function(callback){
                forumModel.getSingleQuestionDetail([req.params.question_id,req.params.activity_id], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.render('forum_view', {
                title: 'Forum Detail', 
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                activity:results['getActivityDetails'],
                question:results['getSingleQuestionDetail']
            });
        });
    }else{
        res.redirect('/');
    }
});

//提出問題
router.post('/activity/:activity_id/questions', function(req, res, next){
    if(req.session.user){
        var post_date = new Date();
        forumModel.askQuestion([
            req.params.activity_id,
            req.body.creator_id,
            req.body.creator_cooc_id,
            req.body.title,
            req.body.content,
            post_date,
            req.session.user.id,
            req.session.user.userId
        ], function(err,data){
            res.redirect('/forum/teacher/activity/'+req.params.activity_id+'/questions');
        });
    }else{
        res.redirect('/');
    }
});

module.exports = router;