var express = require('express');
var router = express.Router();

var groupModel = require('../models/Group');
var userModal = require('../models/User');

var async = require('async');

//群組首頁
router.get('/', function(req, res, next){
    if(req.session.user){
        groupModel.getAllGroupsByTeacherId({
            creator_id:req.session.user.id,
            search:null,
            start:0,
            end:10,
            sort:'create_date',
            order:'desc'
        },function(err,data){
            res.render('group',{
                title:'Group',
                cooc_header:req.app.locals.cooc_header_login,
                cooc_footer:req.app.locals.cooc_footer,
                user:req.session.user,
                groups:data
            });
        });
    }else{
        res.redirect('/');
    }
});

//群組建立
router.post('/create', function(req, res, next){
    if(req.session.user){
        var groupName = req.body.group_name?req.body.group_name:'New Group';
        groupModel.createNewGroup([
            groupName,
            req.session.user.id,
            req.session.user.userId
        ], function(err,data){
            res.redirect('/group/edit/'+data);
        });
    }
});

//群組編輯
router.get('/edit/:group_id', function(req, res, next){
    if(req.session.user){
        groupModel.checkGroupOwner([
            req.params.group_id,
            req.session.user.userId
        ],function(err,data){
            if(data.length!=0){
                var group = {
                    id:req.params.group_id,
                    name:data[0]['name'],
                    description:data[0]['description'],
                    create_date:data[0]['create_date'],
                    members:[],
                    all:[],
                    results:null,
                    roleList:req.session.user.roleList
                };
                
                async.auto({
                    getAllGroupsByTeacherIdDropdown: function(callback){
                        groupModel.getAllGroupsByTeacherIdDropdownByCoocId([req.session.user.userId],function(err,data){
                            callback(null,data);
                        });
                    },
                    getSingleGroupMembersByGroupId: function(callback){
                        groupModel.getSingleGroupMembersByGroupId([req.params.group_id],function(err,data){
                            callback(null,data);
                        });
                    },
                    getUserInfo: function(callback){
                        userModal.getCoocUserInfo(req.session.user.userId, function(err,data){
                            var schoolInfo = data['info']['body'][0]['userList'][0]['user'][0]['schoolList'][0]['school'];
                            callback(null,schoolInfo);
                        });
                    },
                    getSemesterInfo: ['getUserInfo', function(results,callback){
                        var firstSchoolCode = results['getUserInfo'][0]['$']['schoolCode'];
                        userModal.getSemesterInfo(firstSchoolCode, function(err,data){
                            var semesterInfo = data['info']['body'][0]['school'][0]['termList'][0]['term'];
                            callback(null,semesterInfo);
                        });
                    }],
                    getStudentList: ['getSemesterInfo', function(results,callback){
                        var firstTermId = results['getSemesterInfo'][0]['$']['termId'];
                        userModal.getCoocStudentList({
                            userId:req.session.user.userId,
                            termId:firstTermId
                        }, function(err,data){
                            var groupList = data['info']['body'][0]['groupList'][0];
                            if(typeof groupList === 'object'){
                                groupList = groupList['group'];
                                groupList.forEach(function(group){
                                    var studentList = group['$']['studentList'][0];
                                    if(typeof studentList === 'object'){
                                        group['studentList'] = studentList['student'];
                                    }else{
                                        group['studentList'] = [];
                                    }
                                });
                            }else{
                                groupList = [];
                            }
                            callback(null,groupList);
                        });
                    }]
                }, function(err, results){
                    console.log(results);
                    group.results = results;
                    res.render('group_detail',{
                        title:'Group Edit',
                        cooc_header:req.app.locals.cooc_header_login,
                        cooc_footer:req.app.locals.cooc_footer,
                        user:req.session.user,
                        group:group
                    });
                });
            }
        });
    }else{
        res.redirect('/');
    }
});

//刪除群組
router.post('/remove', function(req, res, next){
    if(req.session.user){
        groupModel.removeGroupByGroupId([req.body.group_id,req.session.user.id], function(err,data){
            res.redirect('/group');
        });
    }else{
        res.redirect('/');
    }
});

module.exports = router;