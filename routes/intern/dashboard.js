var express = require('express');
var logModel = require('../../models/intern/Log.js');
var router = express.Router();
//儀表板首頁
router.get('/', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard', {
			title: 'Dashboard',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師儀表板首頁
router.get('/teacher', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/dashboard_teacher', {
			title: 'Dashboard_teacher',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師資料檢閱庫
router.get('/teacher/records', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/records_teacher', {
			title: '資料檢閱庫',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師編輯工具分析
router.get('/teacher/wat', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/wat_teacher', {
			title: '編輯工具分析',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師測驗分析
router.get('/teacher/assessment', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/assessment_teacher', {
			title: '測驗分析',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//我的徽章
router.get('/student/badge', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/intern/badge', {
			title: '我的徽章',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//新增目標類別
router.get('/student/badge/addCategory', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.insertBadgeCategory(req.query.data, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});
//新增目標條件
router.get('/student/badge/addCondition', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.insertBadgeCondition(req.query.data, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});
//更新目標條件
router.get('/student/badge/updateCondition', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.updateBadgeCondition(req.query.data, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});
//移除目標條件
router.get('/student/badge/deleteCondition', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.deleteBadgeCondition(req.query.data, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});
//取得目標條件
router.get('/student/badge/getCondition', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.getBadgeCondition(req.query.data, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});

//取得所有徽章類別與成就條件
router.get('/student/badge/getInfo', function (req, res, next) {
	console.log('接收資料: ', req.query);
	if (req.session.user) {
		logModel.getBadgesInfo(true, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				res.send({
					data: data
				});
			}
		});
	} else {
		res.redirect('/');
	}
});
router.get('/student/custom', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/intern/custom_student', {
			title: '自訂首頁',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師群體學習行為分析
router.get('/teacher/groupanalysis', function (req, res, next) {
	res.render('dashboard/intern/group_analysis', {
		title: '群體學習行為分析',
		cooc_header: req.app.locals.cooc_header_login,
		cooc_footer: req.app.locals.cooc_footer,
		user: req.session.user
	});
});
//教師群體學習行為分析
router.get('/teacher/groupanalysis/search', function (req, res, next) {
	console.log('接收資料: ', req.query.search);
	logModel.getGroupAnalysis(req.query.search.type, req.query.search.data, function (err, data) {
		res.send({
			searchResult: data
		});
	});
});
//教師酷課學習
router.get('/teacher/coocLearning', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/coocLearning_teacher', {
			title: '酷課學習儀表板',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//教師自定首頁
router.get('/teacher/custom', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/custom_teacher', {
			title: '自定首頁',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生儀表板首頁
router.get('/student', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/intern/dashboard_student', {
			title: 'Dashboard_student',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生資料檢閱庫
router.get('/student/records', function (req, res, next) {
	if (req.session.user) {
		logModel.getRecord(true, function (err, data) {
			res.render('dashboard/intern/records_student', {
				title: '資料檢閱庫',
				cooc_header: req.app.locals.cooc_header_login,
				cooc_footer: req.app.locals.cooc_footer,
				user: req.session.user,
				msg: data
			});
		});
	} else {
		res.redirect('/');
	}
});
//取得篩選結果
router.get('/student/records/filter', function (req, res, next) {
	console.log('接收資料: ', req.query.filter);
	if (req.session.user) {
		logModel.getRecordFilterResult(req.query.filter, function (err, data) {
			res.send({
				filterResult: data
			});
		});
	} else {
		res.redirect('/');
	}
});
//學生大學科系探索
router.get('/student/college', function (req, res, next) {
	if (req.session.user) {
		logModel.getAnalysisRecord(true, function (err, data) {
			res.render('dashboard/intern/college_discover', {
				title: '大學科系探索',
				cooc_header: req.app.locals.cooc_header_login,
				cooc_footer: req.app.locals.cooc_footer,
				user: req.session.user,
				msg: data
			});
		});
	} else {
		res.redirect('/');
	}
});
//學生大學科系探索
router.get('/student/college/filter', function (req, res, next) {
	console.log('接收資料: ', req.query.filter);
	if (req.session.user) {
		logModel.getAnalysisFilterResult(req.query.filter, function (err, data) {
			res.send({
				filterResult: data
			});
		});
	} else {
		res.redirect('/');
	}
});
//學生編輯工具分析
router.get('/student/wat', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/wat_student', {
			title: '編輯工具分析',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生酷課學習
router.get('/student/coocLearning', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/coocLearning_student', {
			title: '酷課學習儀表板',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生酷課學習
router.get('/student/coocLearning', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/coocLearning_student', {
			title: '酷課學習儀表板',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生智慧內容推薦
router.get('/student/recommendation', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/recommendation', {
			title: '智慧內容推薦',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
//學生自定首頁
router.get('/student/custom', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/custom_student', {
			title: '自定首頁',
			cooc_header: req.app.locals.cooc_header_login,
			cooc_footer: req.app.locals.cooc_footer,
			user: req.session.user
		});
	} else {
		res.redirect('/');
	}
});
router.get('/news', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/news.ejs', {
			title: 'News',
			cooc_header: req.app.locals.cooc_header,
			cooc_footer: req.app.locals.cooc_footer
		});
	} else {
		res.redirect('/');
	}
});
router.get('/forum', function (req, res, next) {
	if (req.session.user) {
		res.render('dashboard/forum.ejs', {
			title: 'Forum',
			cooc_header: req.app.locals.cooc_header,
			cooc_footer: req.app.locals.cooc_footer
		});
	} else {
		res.redirect('/');
	}
});
module.exports = router;