var express = require('express');
var router = express.Router();
var userModel = require('../models/User');
var sha256 = function(input){
    return require('crypto').createHash('sha256').update(input).digest('hex')
}
var config = require('../config');
var url = require('url');
var auth = require('../util/auth');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

//使用者註冊
router.post('/register',function(req, res, next){
  var user = {
    email:req.body.email,
    password:sha256(req.body.password),
    name:req.body.name,
    register_date:new Date()
  };
  userModel.registerUser(user,function(err,userId){
    console.log('insertId:'+userId);
  });
});

//酷課雲登入
router.get('/login',function(req, res, next){
  var reqURL = url.format({
    protocol: req.protocol,
    host: req.get('host')
  });
  var redirect_url = config.cooc.sso_url+'oauth2/oauth/authorize?client_id='+config.cooc.client_id+'&redirect_uri='+reqURL+'/users/callback&response_type=code&approval_prompt=force';
  res.redirect(redirect_url);
});

//酷課雲登出
router.get('/logout',function(req, res, next){
  req.session.destroy();
  var reqURL = url.format({
    protocol: req.protocol,
    host: req.get('host')
  });
  res.redirect(config.cooc.sso_url+'oauth2/logout.do?client_id='+config.cooc.client_id+'&client_secret='+config.cooc.client_secret+'&redirect_uri='+reqURL+'&series=36B78609_Sji25CC_Sji476C_Sji8F61_Sji9B1EE3C187DE.oauth2');
});

//酷課雲 callback
router.get('/callback',function(req, res, next){
  var reqURL = url.format({
    protocol: req.protocol,
    host: req.get('host')
  });
  var code = req.query.code;
  var getTokenDone = function(err,token){
    if(err) return;
    var accessTokenObj = JSON.parse(token);
    auth.getUserInfo(accessTokenObj.access_token,function(err,userInfo){
      if(err) return;
      console.log(JSON.stringify(userInfo));
      var status = userInfo['info']['header'][0]['status'][0];
      if(status==='OK'){
        var userData = userInfo['info']['body'][0]['info'][0];
        var isTeacher = false;  //2
        var isStudent = false;  //3
        var isParent = false;   //4
        var userSessionData = {
          accessToken:userData['$']['accessToken'],
          userId:userData['userId'][0],
          loginId:userData['loginId'][0],
          name:userData['name'][0],
          photoUrl:userData['photoUrl'][0],
          role_name:'學生',
          roleList:[]
        };
        var roleList = userData['roleList'];
        for(var i=0;i<roleList.length;i++){
          var roleArr = roleList[i]['role'];
          for(var j=0;j<roleArr.length;j++){
            var role = roleArr[j];
            var roleId = role['roleId'][0];
            if(roleId=='2'){
              isTeacher = true;
              userSessionData.role_name = '教師';
            }else if(roleId=='3'){
              isStudent = true;
              userSessionData.role_name = '學生';
            }else if(roleId=='4'){
              isParent = true;
              userSessionData.role_name = '家長';
            }
            userSessionData['roleList'].push({
              roleId:roleId,
              schoolCode:role['schoolCode'][0],
              schoolUserId:role['schoolUserId'][0]
            });
          }
        }
        userSessionData['isStudent'] = isStudent;
        userSessionData['isTeacher'] = isTeacher;
        userSessionData['isParent'] = isParent;

        req.session.user = userSessionData;
        
        userModel.checkCoocUser([userSessionData.userId],function(checkCoocUserErr,checkCoocUserResult){
          var loginRole = 3;
          if(isTeacher){
            loginRole = 2;
          }
          if(checkCoocUserResult.length==0){
            userModel.insertNewCoocUser([userSessionData.loginId,userSessionData.name,userSessionData.userId,userSessionData.isTeacher,userSessionData.isStudent,userSessionData.isParent],function(insertCoocUserError,insertCoocUserResult){
              userSessionData['id'] = insertCoocUserResult.insertId;
              userModel.logUserLoginData([userSessionData.id,userSessionData.loginId,userSessionData.name,req.ip,loginRole],function(logUserLoginError,logUserLoginData){
                console.log('login successfully');
              });
            });
          }else{
            userSessionData['id'] = checkCoocUserResult[0]['id'];
            userModel.logUserLoginData([userSessionData.id,userSessionData.loginId,userSessionData.name,req.ip,loginRole],function(logUserLoginError,logUserLoginData){
              console.log('login successfully');
            });
          }
          
          if(isTeacher){
            res.redirect('/teacher');
          }else{
            res.redirect('/student/activity?p=1&category=assign');
          }

        });
      }  
      
    });
  };
  auth.getAccessToken(code,reqURL+'/users/callback',getTokenDone);
});

//使用者登入檢查
router.post('/login',function(req, res, next){
  var user = [
    req.body.email,
    sha256(req.body.password)
  ];
  userModel.checkUser(user,function(err,results){
    if(results.length>0){ // 有此帳號
			console.log('ok');
		}else{
			console.log('fail');
		}
  });
});

module.exports = router;
