var express = require('express');
var router = express.Router();

var request = require('request');
var config = require('../config');
var async = require('async');

var activityModel = require('../models/Activity');
var materialModel = require('../models/Material');
var groupModel = require('../models/Group');
var analyticModel = require('../models/Analytic');
var userModal = require('../models/User');

//儀表板首頁
router.get('/', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard',{ 
      title: 'Dashboard', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//教師儀表板首頁
router.get('/teacher', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/dashboard_teacher',{ 
      title: 'Dashboard_teacher', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//教師資料檢閱庫
router.get('/teacher/records', function(req, res, next) {
  if(req.session.user){
    var group = {
      results:null,
      roleList:req.session.user.roleList
    };
    async.auto({
      getUserInfo: function(callback){
        userModal.getCoocUserInfo(req.session.user.userId, function(err,data){
            var schoolInfo = data['info']['body'][0]['userList'][0]['user'][0]['schoolList'][0]['school'];
            callback(null,schoolInfo);
        });
      },
      getSemesterInfo: ['getUserInfo', function(results,callback){
        var firstSchoolCode = results['getUserInfo'][0]['$']['schoolCode'];
        userModal.getSemesterInfo(firstSchoolCode, function(err,data){
            var semesterInfo = data['info']['body'][0]['school'][0]['termList'][0]['term'];
            callback(null,semesterInfo);
        });
      }],
      getStudentList: ['getSemesterInfo', function(results,callback){
        var firstTermId = results['getSemesterInfo'][0]['$']['termId'];
        userModal.getCoocStudentList({
            userId:req.session.user.userId,
            termId:firstTermId
        }, function(err,data){
            var groupList = data['info']['body'][0]['groupList'][0];
            if(typeof groupList === 'object'){
                groupList = groupList['group'];
                groupList.forEach(function(group){
                    var studentList = group['$']['studentList'][0];
                    if(typeof studentList === 'object'){
                        group['studentList'] = studentList['student'];
                    }else{
                        group['studentList'] = [];
                    }
                });
            }else{
                groupList = [];
            }
            callback(null,groupList);
        });
      }]
    }, function(err, results){
      group.results = results;
      res.render('dashboard/records_teacher',{ 
        title: '資料檢閱庫', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        group:group
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯工具分析
router.get('/teacher/wat', function(req, res, next) {
  if(req.session.user){
    async.auto({
      getAwtTeacherHomepageData_activity_counts: function(callback){
        analyticModel.getAwtTeacherHomepageData_activity_counts([req.session.user.id], function(err, data){
          callback(null, data);
        });
      },
      getAwtTeacherHomepageData_material_counts: function(callback){
        analyticModel.getAwtTeacherHomepageData_material_counts([req.session.user.id], function(err, data){
          callback(null, data);
        });
      },
      getAwtTeacherHomepageData_group_counts: function(callback){
        analyticModel.getAwtTeacherHomepageData_group_counts([req.session.user.id], function(err, data){
          callback(null, data);
        });
      },
      getAwtTeacherHomepageData_student_counts: function(callback){
        analyticModel.getAwtTeacherHomepageData_student_counts([req.session.user.id], function(err, data){
          callback(null, data);
        });
      }
    }, function(err, results){
      res.render('dashboard/wat_teacher',{ 
        title: '編輯工具分析', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity_counts: results['getAwtTeacherHomepageData_activity_counts'],
        material_counts: results['getAwtTeacherHomepageData_material_counts'],
        group_counts: results['getAwtTeacherHomepageData_group_counts'],
        student_counts: results['getAwtTeacherHomepageData_student_counts']
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師測驗分析
router.get('/teacher/assessment', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/assessment_teacher',{ 
      title: '測驗分析', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//教師群體學習行為分析
router.get('/teacher/groupanalysis', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/group_analysis',{ 
      title: '群體學習行為分析', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//教師酷課學習
router.get('/teacher/coocLearning', function(req, res, next){
  if(req.session.user){
    var group = {
      results:null,
      roleList:req.session.user.roleList
    };
    async.auto({
      getUserInfo: function(callback){
        userModal.getCoocUserInfo(req.session.user.userId, function(err,data){
            var schoolInfo = data['info']['body'][0]['userList'][0]['user'][0]['schoolList'][0]['school'];
            callback(null,schoolInfo);
        });
      },
      getSemesterInfo: ['getUserInfo', function(results,callback){
        var firstSchoolCode = results['getUserInfo'][0]['$']['schoolCode'];
        userModal.getSemesterInfo(firstSchoolCode, function(err,data){
            var semesterInfo = data['info']['body'][0]['school'][0]['termList'][0]['term'];
            callback(null,semesterInfo);
        });
      }],
      getStudentList: ['getSemesterInfo', function(results,callback){
        var firstTermId = results['getSemesterInfo'][0]['$']['termId'];
        userModal.getCoocStudentList({
            userId:req.session.user.userId,
            termId:firstTermId
        }, function(err,data){
            var groupList = data['info']['body'][0]['groupList'][0];
            if(typeof groupList === 'object'){
                groupList = groupList['group'];
                groupList.forEach(function(group){
                    var studentList = group['$']['studentList'][0];
                    if(typeof studentList === 'object'){
                        group['studentList'] = studentList['student'];
                    }else{
                        group['studentList'] = [];
                    }
                });
            }else{
                groupList = [];
            }
            callback(null,groupList);
        });
      }]
    }, function(err, results){
      group.results = results;
      res.render('dashboard/coocLearning_teacher',{ 
        title: '酷課學習儀表板', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        group:group
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師自定首頁
router.get('/teacher/custom', function(req, res, next){
  if(req.session.user){
    res.render('dashboard/custom_teacher',{ 
      title: '自定首頁', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//學生儀表板首頁
router.get('/student', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/dashboard_student',{ 
      title: 'Dashboard_student', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//學生資料檢閱庫
router.get('/student/records', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/records_student',{ 
      title: '資料檢閱庫', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    }); 
  }else{
    res.redirect('/');
  }
});

//學生編輯工具分析
router.get('/student/wat', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/wat_student',{ 
      title: '編輯工具分析', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//學生酷課學習
router.get('/student/coocLearning', function(req, res, next){
  if(req.session.user){
    request({
      url:config.cooc.learning_api+'maps/knowledge',
      json:true
    }, function(err, response, body){
      var maps = JSON.parse(body.replace(/^\uFEFF/, ''));
      res.render('dashboard/coocLearning_student',{ 
        title: '酷課學習儀表板', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        maps:maps
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生測驗分析
router.get('/student/assessment', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/assessment_student',{ 
      title: '測驗分析', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//學生智慧內容推薦
router.get('/student/recommendation', function(req, res, next) {
  if(req.session.user){
    var maps;
    var mapData;
    async.auto({
      getMapStructure: function(callback){
        request({
          url:config.cooc.learning_api+'maps/knowledge',
          json:true
        }, function(err, response, body){
          maps = JSON.parse(body.replace(/^\uFEFF/, ''));
          callback(null, true);
        });
      },
      getDefaultMapData: function(callback){
        request({
          url:config.cooc.learning_api+'d3data/1',
          json:true
        }, function(err, response, body){
          mapData = JSON.parse(body.replace(/^\uFEFF/, ''));
          callback(null, true);
        });
      }
    }, function(err, results){
      res.render('dashboard/recommendation',{ 
        title: '智慧內容推薦', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        maps:maps,
        mapData:mapData
      });
    });
  }else{
    res.redirect('/');
  }
});

//學生大學科系探索
router.get('/student/college', function(req, res, next){
  if(req.session.user){
    analyticModel.getSchoolRecord(true, function(err, data){
      res.render('dashboard/college_discover',{ 
        title: '大學科系探索', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        msg: data
      });
    });
  }else{
    res.redirect('/');
  }
});
//學生大學科系探索 with filter
router.get('/student/college/filter', function(req, res, next){
  if(req.session.user){
    analyticModel.getSchoolFilterRecord(req.query.filter, function(err, data){
      res.send({
				filterResult: data
			});
    });
  }else{
    res.redirect('/');
  }
});

//學生自定首頁
router.get('/student/custom', function(req, res, next){
  if(req.session.user){
    res.render('dashboard/custom_student',{ 
      title: '自定首頁', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

router.get('/news', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/news.ejs',{ 
      title: 'News', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer
    });
  }else{
    res.redirect('/');
  }
});

router.get('/forum', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/forum.ejs',{ 
      title: 'Forum', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer
    });
  }else{
    res.redirect('/');
  }
});

router.get('/forum_content', function(req, res, next) {
  res.render('dashboard/forum_content.ejs',{ 
        title: 'Forum_content', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer
      });
});

//單一活動分析頁面
router.get('/teacher/wat/activity/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetActivityAssingedGroupsDropdown: function(callback){
        groupModel.teacherGetActivityAssingedGroupsDropdown([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityDetails: function(callback){
        activityModel.getActivityDetails([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityMaterials: function(callback){
        materialModel.studentGetMaterialsByUserId([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      teacherGetAllActivityAssignedGroupLearnedStatus: function(callback){
        analyticModel.teacherGetAllActivityAssignedGroupLearnedStatus([
          req.params.activity_id,
          req.params.activity_id
        ], function(err, data){
          callback(null, data);
        });
      },
      teacherGetOpenJoinedStudentStatus:['teacherGetAllActivityAssignedGroupLearnedStatus', function(results, callback){
        var assignedStudentStatus = results['teacherGetAllActivityAssignedGroupLearnedStatus'];
        var assignedStudentIds = [];
        for(var stu_index=0;stu_index<assignedStudentStatus.length;stu_index++){
          assignedStudentIds.push(assignedStudentStatus[stu_index].user_cooc_id);
        }
        analyticModel.teacherGetOpenJoinedStudentStatus({
         activity_id:req.params.activity_id,
         assign_groups:assignedStudentIds 
        }, function(err, data){
          if(data && data.length!=0){
            for(var d=0;d<data.length;d++){
              assignedStudentStatus.push(data[d]);
            }
          }
          callback(null, assignedStudentStatus);
        });
      }],
      getSingleMaterialStudentLearnStatus: ['getActivityMaterials', function(results, callback1){
        var materials = results['getActivityMaterials'];
        if(materials && materials.length!=0){
          async.each(materials, function(material, callback){
            activityModel.getSingleMaterialStudentLearnStatus([material.id], function(err, data){
              material.student = data;
              callback(null, data);
            });
          },function(err){
            callback1(null, materials);
          });
        }else{
          materials.student = [];
          callback1(null, materials);
        }
      }]
    }, function(err, results){
      res.render('dashboard/activity_teacher', {
        title: 'Activity Dashboard',
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['getActivityDetails'],
        pure_materials: results['getActivityMaterials'],
        materials:results['getSingleMaterialStudentLearnStatus'],
        students: results['getActivityJoinedStudents'],
        assign_groups: results['teacherGetActivityAssingedGroupsDropdown'],
        target_group:null,
        student_progress:results['teacherGetOpenJoinedStudentStatus']
      });
    });
  }else{
    res.redirect('/');
  }
});

//單一活動分析頁面 By 單一群組
router.get('/teacher/wat/activity/:activity_id/group/:group_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetActivityAssingedGroupsDropdown: function(callback){
        groupModel.teacherGetActivityAssingedGroupsDropdown([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityDetails: function(callback){
        activityModel.getActivityDetails([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityMaterials: function(callback){
        materialModel.studentGetMaterialsByUserId([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      teacherGetActivityAssignedGroupLearnedStatus: function(callback){
        analyticModel.teacherGetActivityAssignedGroupLearnedStatus([
          req.params.activity_id,
          req.params.activity_id,
          req.params.group_id
        ], function(err, data){
          callback(null, data);
        });
      },
      getSingleMaterialStudentLearnStatus: ['getActivityMaterials', function(results, callback1){
        var materials = results['getActivityMaterials'];
        if(materials && materials.length!=0){
          async.each(materials, function(material, callback){
            activityModel.getSingleMaterialStudentLearnStatus([material.id], function(err, data){
              material.student = data;
              callback(null, data);
            });
          },function(err){
            callback1(null, materials);
          });
        }else{
          materials.student = [];
          callback1(null, materials);
        }
      }]
    }, function(err, results){
      res.render('dashboard/activity_teacher', {
        title: 'Activity Dashboard',
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['getActivityDetails'],
        pure_materials: results['getActivityMaterials'],
        materials:results['getSingleMaterialStudentLearnStatus'],
        students: results['getActivityJoinedStudents'],
        assign_groups: results['teacherGetActivityAssingedGroupsDropdown'],
        target_group:req.params.group_id,
        student_progress: results['teacherGetActivityAssignedGroupLearnedStatus']
      });
    });
  }else{
    res.redirect('/');
  }
});

//適性活動分析頁面
router.get('/teacher/wat/adaptive_activity/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetActivityAssingedGroupsDropdown: function(callback){
        groupModel.teacherGetActivityAssingedGroupsDropdown([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityDetails: function(callback){
        activityModel.getActivityDetails([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      teacherGetAllActivityAssignedGroupLearnedStatus: function(callback){
        analyticModel.teacherGetAllActivityAssignedGroupLearnedStatus([
          req.params.activity_id,
          req.params.activity_id
        ], function(err, data){
          callback(null, data);
        });
      },
      teacherGetOpenJoinedStudentStatus:['teacherGetAllActivityAssignedGroupLearnedStatus', function(results, callback){
        var assignedStudentStatus = results['teacherGetAllActivityAssignedGroupLearnedStatus'];
        var assignedStudentIds = [];
        for(var stu_index=0;stu_index<assignedStudentStatus.length;stu_index++){
          assignedStudentIds.push(assignedStudentStatus[stu_index].user_cooc_id);
        }
        analyticModel.teacherGetOpenJoinedStudentStatus({
         activity_id:req.params.activity_id,
         assign_groups:assignedStudentIds 
        }, function(err, data){
          if(data && data.length!=0){
            for(var d=0;d<data.length;d++){
              assignedStudentStatus.push(data[d]);
            }
          }
          callback(null, assignedStudentStatus);
        });
      }],
      getAdaptiveActivityStudentNodeCounts: function(callback){
        activityModel.getAdaptiveActivityStudentNodeCounts([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getAdaptiveActivityStudentNodeStatus: function(callback){
        activityModel.getAdaptiveActivityStudentNodeStatus([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getAdaptiveActivityHierarchy: function(callback){
        activityModel.getAdaptiveActivityHierarchy([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getAdaptiveActivityStudentPath: ['teacherGetOpenJoinedStudentStatus','getAdaptiveActivityHierarchy', function(results, callback){
        var hierarchy = results['getAdaptiveActivityHierarchy'];
        var students = results['teacherGetOpenJoinedStudentStatus'];
        var pathData = {
          nodes:[{name:"Start"}],
          links:[{source:0, target:1, value:students.length}]
        };
        pathData.nodes = pathData.nodes.concat(hierarchy);
        var pathMatrix = [];
        var pathStudentMatrix = [];
        for(var i=0;i<hierarchy.length;i++){
          pathMatrix[i] = [];
          pathStudentMatrix[i] = [];
          for(var j=0;j<hierarchy.length;j++){
            pathMatrix[i][j] = 0;
            pathStudentMatrix[i][j] = [];
          }
        }
        async.each(students, function(student,cb){
          activityModel.getAdaptiveActivityStudentPath([
            req.params.activity_id,
            student.user_cooc_id
          ], function(err, d){
            for(var t=0;t<d.length;t++){
              if(t!=(d.length-1)){
                pathMatrix[d[t].node_index][d[t+1].node_index]++;
                if(pathStudentMatrix[d[t].node_index][d[t+1].node_index].indexOf(student.user_cooc_id)==-1){
                  pathStudentMatrix[d[t].node_index][d[t+1].node_index].push({
                    user_cooc_id:student.user_cooc_id,
                    user_name:student.user_name
                  });
                }
              }
            }
            cb();
          });
        }, function(err){
          for(var p=0;p<pathMatrix.length;p++){
            var pm = pathMatrix[p];
            var psm = pathStudentMatrix[p];
            for(var pj=0;pj<pm.length;pj++){
              if(pm[pj]!=0 && p!=pj){
                pathData.links.push({
                  source:p,
                  target:pj,
                  value:pm[pj],
                  students:psm[pj]
                });
              }
            }
          }
          callback(null, pathData);
        });
      }]
    }, function(err, results){
      res.render('dashboard/adaptive_activity', {
        title: 'Adaptive Activity Dashboard',
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['getActivityDetails'],
        target_group:null,
        assign_groups: results['teacherGetActivityAssingedGroupsDropdown'],
        student_progress:results['teacherGetOpenJoinedStudentStatus'],
        node_student_counts: results['getAdaptiveActivityStudentNodeCounts'],
        node_student_status: results['getAdaptiveActivityStudentNodeStatus'],
        node_student_path:results['getAdaptiveActivityStudentPath']
      });
    });
  }else{
    res.redirect('/');
  }
});

//適性活動分析頁面 BY 單一群組
router.get('/teacher/wat/adaptive_activity/:activity_id/group/:group_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetActivityAssingedGroupsDropdown: function(callback){
        groupModel.teacherGetActivityAssingedGroupsDropdown([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getActivityDetails: function(callback){
        activityModel.getActivityDetails([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      teacherGetActivityAssignedGroupLearnedStatus: function(callback){
        analyticModel.teacherGetActivityAssignedGroupLearnedStatus([
          req.params.activity_id,
          req.params.activity_id,
          req.params.group_id
        ], function(err, data){
          callback(null, data);
        });
      },
      getAdaptiveActivityStudentNodeCountsByGroup:['teacherGetActivityAssignedGroupLearnedStatus', function(results, callback){
        var assignedStudentStatus = results['teacherGetActivityAssignedGroupLearnedStatus'];
        var assignedStudentIds = [];
        for(var stu_index=0;stu_index<assignedStudentStatus.length;stu_index++){
          assignedStudentIds.push(assignedStudentStatus[stu_index].user_cooc_id);
        }
        activityModel.getAdaptiveActivityStudentNodeCountsByGroup({
          activity_id:req.params.activity_id,
          assign_groups:assignedStudentIds
        }, function(err, data){
          callback(null, data);
        });
      }],
      getAdaptiveActivityStudentNodeStatusByGroup:['teacherGetActivityAssignedGroupLearnedStatus', function(results, callback){
        var assignedStudentStatus = results['teacherGetActivityAssignedGroupLearnedStatus'];
        var assignedStudentIds = [];
        for(var stu_index=0;stu_index<assignedStudentStatus.length;stu_index++){
          assignedStudentIds.push(assignedStudentStatus[stu_index].user_cooc_id);
        }
        activityModel.getAdaptiveActivityStudentNodeStatusByGroup({
          activity_id:req.params.activity_id,
          assign_groups:assignedStudentIds
        }, function(err, data){
          callback(null, data);
        });
      }],
      getAdaptiveActivityHierarchy: function(callback){
        activityModel.getAdaptiveActivityHierarchy([req.params.activity_id], function(err, data){
          callback(null, data);
        });
      },
      getAdaptiveActivityStudentPath: ['teacherGetActivityAssignedGroupLearnedStatus','getAdaptiveActivityHierarchy', function(results, callback){
        var hierarchy = results['getAdaptiveActivityHierarchy'];
        var students = results['teacherGetActivityAssignedGroupLearnedStatus'];
        var pathData = {
          nodes:[{name:"Start"}],
          links:[{source:0, target:1, value:students.length}]
        };
        pathData.nodes = pathData.nodes.concat(hierarchy);
        var pathMatrix = [];
        var pathStudentMatrix = [];
        for(var i=0;i<hierarchy.length;i++){
          pathMatrix[i] = [];
          pathStudentMatrix[i] = [];
          for(var j=0;j<hierarchy.length;j++){
            pathMatrix[i][j] = 0;
            pathStudentMatrix[i][j] = [];
          }
        }
        async.each(students, function(student,cb){
          activityModel.getAdaptiveActivityStudentPath([
            req.params.activity_id,
            student.user_cooc_id
          ], function(err, d){
            for(var t=0;t<d.length;t++){
              if(t!=(d.length-1)){
                pathMatrix[d[t].node_index][d[t+1].node_index]++;
                if(pathStudentMatrix[d[t].node_index][d[t+1].node_index].indexOf(student.user_cooc_id)==-1){
                  pathStudentMatrix[d[t].node_index][d[t+1].node_index].push({
                    user_cooc_id:student.user_cooc_id,
                    user_name:student.user_name
                  });
                }
              }
            }
            cb();
          });
        }, function(err){
          for(var p=0;p<pathMatrix.length;p++){
            var pm = pathMatrix[p];
            var psm = pathStudentMatrix[p];
            for(var pj=0;pj<pm.length;pj++){
              if(pm[pj]!=0 && p!=pj){
                pathData.links.push({
                  source:p,
                  target:pj,
                  value:pm[pj],
                  students:psm[pj]
                });
              }
            }
          }
          callback(null, pathData);
        });
      }]
    }, function(err, results){
      res.render('dashboard/adaptive_activity', {
        title: 'Adaptive Activity Dashboard',
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results['getActivityDetails'],
        target_group:req.params.group_id,
        assign_groups: results['teacherGetActivityAssingedGroupsDropdown'],
        student_progress:results['teacherGetActivityAssignedGroupLearnedStatus'],
        node_student_counts: results['getAdaptiveActivityStudentNodeCountsByGroup'],
        node_student_status: results['getAdaptiveActivityStudentNodeStatusByGroup'],
        node_student_path:results['getAdaptiveActivityStudentPath']
      });
    });
  }else{
    res.redirect('/');
  }
});


//學生徽章
router.get('/student/badge', function(req, res, next) {
  if(req.session.user){
    res.render('dashboard/badge',{ 
      title: '我的徽章', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    }); 
  }else{
    res.redirect('/');
  }
});

module.exports = router;