var express = require('express');
var router = express.Router();

var analyticModel = require('../models/Analytic');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/overview',function(req, res, next){
    analyticModel.getSystemOverview(function(err,results){
        res.json(results[0]);
    });
});

module.exports = router;