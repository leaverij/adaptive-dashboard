var express = require('express');
var router = express.Router();

var activityModel = require('../../models/Activity');
var collectionModel = require('../../models/Collection');
var userModel = require('../../models/User');
var groupModel = require('../../models/Group');
var async = require('async');
var treeIterator = require('tree-iterate');

router.get('/',function(req, res, next){
    res.send('activity api');
});

//教師刪除活動
router.post('/remove/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.teacherRemoveActivity([req.params.activity_id,req.session.user.id], function(err,data){
            res.json({success:true});
        });
    }
});

//教師取得全部活動
router.get('/all', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        activityModel.getActivityByUserId({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            open:null,
            share:null      
        },function(err,data){
            res.json(data);
        });
    }
});

//教師在適性設定中取得活動
router.get('/adaptive/all', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        activityModel.teacherGetAllActivityAdaptive({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            exclude_type:2      
        },function(err,data){
            res.json(data);
        });
    }
});

//教師在適性活動中取得測驗教材
router.get('/adaptive/assessment', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        activityModel.teacherGetAllActivityAdaptiveAssessment({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            exclude_type:2      
        },function(err,data){
            res.json(data);
        });
    }else{
        res.redirect('/');
    }
});

//教師取得已指派活動
router.get('/assign', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        switch(req.query.order[0]['column']){
            case '0':
                sortStr = 'name';
                break;
            case '2':
                sortStr = 'group_name';
                break;
            case '3':
                sortStr = 'assign_date';
                break;
            case '4':
                sortStr = 'start_date';
                break;
            case '5':
                sortStr = 'end_date';
                break;
            default:
        }
        activityModel.teacherGetAssignActivity({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:parseInt(req.query.start),
            end:parseInt(req.query.start)+parseInt(req.query.length),
            sort:sortStr,
            order:req.query.order[0]['dir']
        }, function(err,data){
            res.json(data);
        });
    }
});

//教師指派活動
router.post('/assign', function(req, res, next){
    if(req.session.user){
        var since = '1970-01-01';
        var until = '1970-01-01';
        if(req.body.since){
            since = req.body.since;
        }
        if(req.body.until){
            until = req.body.until;
        }
        async.auto({
            updateActivityAssignStatus: function(callback){
                activityModel.teacherUpdateActivityAssign([req.body.group_id,req.body.activity_id,req.session.user.id],function(err,data){
                    callback(null,data);
                });
            },
            teacherGetGroupMembers: function(callback){
                groupModel.getSingleGroupDetialByGroupId([req.body.group_id,req.session.user.id], function(err,data){
                    callback(null,data);
                });
            },
            assembleActivityGroupData: ['updateActivityAssignStatus','teacherGetGroupMembers', function(results,callback){
                var assignGroupData = [];
                var members = results['teacherGetGroupMembers'];
                for(var i=0;i<members.length;i++){
                    var member = members[i];
                    assignGroupData.push([
                        req.body.activity_id,
                        req.session.user.id,
                        req.body.group_id,
                        member['user_cooc_id'],
                        since,
                        until
                    ]);
                }
                callback(null,assignGroupData)
            }],
            assignActivityGroup:['assembleActivityGroupData', function(results,callback){
                activityModel.teacherAssignActivity([results['assembleActivityGroupData']], function(err,data){
                    callback(null,data);
                });
            }]
        }, function(err, results){
            results['group_id'] = results['teacherGetGroupMembers'][0]['id'];
            results['group_name'] = results['teacherGetGroupMembers'][0]['name'];
            res.json(results);
        });
    }
});

//教師更新活動指派日期
router.post('/assign/date', function(req, res, next){
    if(req.session.user){
        activityModel.teacherUpdateActivityDateAssign([
            req.body.start_date,
            req.body.end_date,
            req.body.activity_id,
            req.body.group_id
        ], function(err,data){
            res.json({success:true});
        });
    }
});

//教師取得公開活動
router.get('/open', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        activityModel.getActivityByUserId({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            open:true,
            share:null
        },function(err,data){
            res.json(data);
        });
    }
});

//教師取得共享(編)活動
router.get('/share', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        activityModel.getActivityByUserId({
            draw:req.query.draw,
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            open:null,
            share:true
        },function(err,data){
            res.json(data);
        });
    }
});

//教師更改活動公開設定
router.post('/public/edit/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.teacherUpdateActivityPublic([req.body.isPublic,parseInt(req.params.activity_id),req.session.user.id],function(err,data){
            res.json([]);
        });
    }
});

//教師加入活動共編成員
router.post('/share/edit/:activity_id', function(req, res, next){
    var result = {
        code:0,
        success:true,
        msg:'success',
        data:null
    };
    if(req.session.user){
        userModel.checkUserByEmail([req.body.user_mail], function(checkUserErr,checkUser){
            if(checkUser.length!=0){
                var user = checkUser[0];
                activityModel.teacherAddActivityShareMember([
                    req.params.activity_id,
                    req.session.user.id,
                    user.id,
                    req.body.user_mail,
                    user.name,
                    req.body.method
                ], function(err,data){  
                    result.data = user;
                    res.json(result);
                });
            }else{
                result.code = 1;
                result.success = false;
                result.msg = '該名使用者不存在!';
                res.json(result);
            }
        });
    }
});

//教師取得活動共編成員
router.get('/share/edit/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.teacherGetActivityShareMember([req.params.activity_id,req.session.user.id], function(err,data){
            res.json(data);
        });
    }
});

//教師刪除活動共編成員
router.post('/share/remove/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.teacherRemoveActivityShareMember([req.params.activity_id,req.session.user.id,req.body.member_mail], function(err,data){
            res.json({success:true});
        });
    }
});

//教師更改活動共編設定
router.post('/share/save/:activity_id', function(req, res, next){
    if(req.session.user){
        var isShare = false;
        if(req.body.isShare=='1'){
            isShare = true;
        }
        activityModel.teacherUpdateActivityShare([isShare,parseInt(req.params.activity_id),req.session.user.id],function(err,data){
            res.json({success:true});
        });
    }
});

//教師儲存適性活動路徑
router.post('/teacher/adaptive/:activity_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            teacherRemoveAdaptiveActivityHierarchy: function(callback){
                activityModel.teacherRemoveAdaptiveActivityHierarchy([req.params.activity_id], function(err, data){
                    callback(null, data);
                });
            },    
            constructTree: ['teacherRemoveAdaptiveActivityHierarchy', function(results, callback){
                var hierarchyJSON = JSON.parse(req.body.hierarchy);
                var hierarchy = [];
                hierarchy.push(hierarchyJSON);
                var options = {
                    objectCallback : false, //params object instead of 3 params
                    ignoreParentalsArray : true,
                    ignoreParents : false
                };
                var hierarchy_obj = [];
                var node_index = 1;
                treeIterator(hierarchy, options, function forEachCallback(node, parent, parents, parentIndices){
                    console.log('NodeID:'+node.id+', NodeName:', node.content_name, ', ParentID:', parent.id, ', ParentName:', parent.content_name);
                    if(node.id){
                        var node_depth = (parents.length!=0)?parents.length:1;
                        hierarchy_obj.push([
                            req.params.activity_id,
                            req.session.user.id,
                            node.id,
                            (parent.id)?parent.id:'',
                            node.content_name,
                            node.content_category,
                            node.content_id,
                            node_index,
                            node_depth
                        ]);
                        node_index++;
                    }
                });
                activityModel.teacherSaveAdaptiveActivityHierarchy([hierarchy_obj], function(err, data){
                    callback(null, data);
                }); 
            }],
            teacherUpdateAdativeActivityHierarchy: function(callback){
                activityModel.teacherUpdateAdativeActivityHierarchy([
                    req.body.hierarchy,
                    req.params.activity_id,
                    req.session.user.id
                ], function(err,data){
                    callback(null, data);
                });
            }
        }, function(error, results){
            res.json({
                success:true
            });
        });
    }else{
        res.redirect('/');
    }
});

//教師取得活動指派群組與狀態
router.get('/teacher/assign/group/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.teacherGetActivityAssignGroups([
            req.session.user.id,
            req.params.activity_id
        ], function(err, data){
            if(err){
                res.json({
                    success:false,
                    data:null
                });
            }else{
                res.json({
                    success:true,
                    data:data
                });
            }
        });
    }else{
        res.redirect('/');
    }
});

//教師進行活動指派群組
router.post('/teacher/assign/group/:activity_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            teacherRemoveActivityAssign: function(callback){
                activityModel.teacherRemoveActivityAssign([
                    req.session.user.id,
                    req.params.activity_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            teacherUpdateActivityAssignGroupCounts: function(callback){
                activityModel.teacherUpdateActivityAssignGroupCounts([
                    JSON.parse(req.body.groups).length,
                    req.params.activity_id,
                    req.session.user.id
                ], function(err, data){
                    callback(null, data);
                });
            },
            teacherUpdateActivityAssign: ['teacherRemoveActivityAssign', function(results, callback){
                var assignGroups = JSON.parse(req.body.groups);
                if(assignGroups.length!=0){
                    var assignGroupData = [];
                    for(var a=0;a<assignGroups.length;a++){
                        var agd = [];
                        var ag = assignGroups[a];
                        agd.push(req.params.activity_id);
                        agd.push(req.session.user.id);
                        agd.push(new Date());
                        agd.push(ag.id);
                        agd.push((ag.start_date)?ag.start_date:null);
                        agd.push((ag.end_date)?ag.end_date:null);
                        assignGroupData.push(agd);
                    }
                    activityModel.teacherInsertActivityAssign([assignGroupData], function(err, data){
                        callback(null, data);
                    });
                }else{
                    callback(null, true);
                }
            }]
        }, function(err, results){
            res.json({
                success:true
            });
        });
    }else{
        res.redirect('/');
    }
});

//學生探索活動
router.post('/student/discover', function(req, res, next){
    if(req.session.user){
        var page = parseInt(req.body.p);
        var start = (page-1)*10;
        activityModel.studentGetDiscoverActivities({
            search:req.body.q,
            start:start,
            end:10,
            sort:'activity.create_date',
            order:'desc',
            teacher_id:null,
            subject_id:parseInt(req.body.subject_id),
            type:parseInt(req.body.type)
          }, function(err, data){
            res.json({
                current:page,
                data:data
            });
          });
    }else{
        res.redirect('/');
    }
});

//學生取得被指派的活動
router.get('/student', function(req, res, next){
    if(req.session.user){
        var category = req.query.category;
        var page = parseInt(req.query.p);
        var start = (page-1)*10;
        switch(category){
            case 'public':
            async.auto({
                getCollections: function(callback){
                    collectionModel.getCollections([req.session.user.userId], function(err,data){
                        callback(null,data);
                    });  
                },
                getActivities: function(callback){
                  activityModel.studentGetJoinActivities({
                    user_cooc_id:req.session.user.userId,
                    search:req.query.q,
                    start:start,
                    end:10,
                    sort:'student_activity_join.join_date',
                    order:'desc',
                    teacher_id:req.query.teacher_id,
                    subject_id:req.query.subject_id,
                    type:req.query.type,
                    progress:parseInt(req.query.progress)
                  }, function(err, data){
                    callback(null, data);
                  })
                }
              }, function(error, results){
                  res.json({
                      current:page,
                      data:results['getActivities'],
                      collections:results['getCollections']
                  });
              });
                break;
            default:
            async.auto({
                getCollections: function(callback){
                    collectionModel.getCollections([req.session.user.userId], function(err,data){
                        callback(null,data);
                    });  
                },
                getBelongGroupIds: function(callback){
                  activityModel.studentGetBelongGroupIds([req.session.user.userId], function(err, data){
                    var ids = [];
                    for(var i=0;i<data.length;i++){
                      ids.push(data[i].group_id);
                    }
                    callback(null, ids);
                  });
                },
                getActivities: ['getBelongGroupIds', function(results, callback){
                  activityModel.studentGetAssignActivities({
                    user_cooc_id:req.session.user.userId,
                    activityIds:results['getBelongGroupIds'],
                    search:req.query.q,
                    start:start,
                    end:10,
                    sort:'activity_assign.assign_date',
                    order:'desc',
                    teacher_id:req.query.teacher_id,
                    subject_id:req.query.subject_id,
                    type:req.query.type,
                    progress:parseInt(req.query.progress)
                  }, function(err, data){
                    callback(null, data);
                  });
                }]
              }, function(error, results){
                res.json({
                    current:page,
                    data:results['getActivities'],
                    collections:results['getCollections']
                });
              });
        }
    }else{
        res.redirect('/');
    }
});

//學生解鎖活動
router.post('/student/unlock/:activity_id', function(req, res, next){
    if(req.session.user){
        activityModel.studentUnlockActivity([
            req.params.activity_id,
            req.body.access_code
        ], function(err, data){
            if(data!=0){
                async.auto({
                    getActivityCreator: function(callback){
                        activityModel.getActivityCreator([req.params.activity_id], function(err, data){
                          callback(null, data);
                        });
                      },
                      activityJoin: ['getActivityCreator', function(results, callback){
                        activityModel.studentInsertActivityJoin([
                          req.params.activity_id,
                          results['getActivityCreator'].creator_id,
                          results['getActivityCreator'].creator_cooc_id,
                          req.session.user.id,
                          req.session.user.userId,
                          new Date(),
                          req.params.activity_id,
                          req.session.user.userId
                        ], function(err, data){
                          callback(null, data);
                        });
                      }]
                }, function(error, results){
                    res.json({
                        success:true
                    });
                });
            }else{
                res.json({
                    success:false
                });
            }
        });
    }else{
        res.redirect('/');
    }
});

//學生加入探索活動的學習
router.get('/student/discover/learn/:activity_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            getActivityCreator: function(callback){
                activityModel.getActivityCreator([req.params.activity_id], function(err, data){
                  callback(null, data);
                });
            },
            activityJoin: ['getActivityCreator', function(results, callback){
                activityModel.studentInsertActivityJoin([
                  req.params.activity_id,
                  results['getActivityCreator'].creator_id,
                  results['getActivityCreator'].creator_cooc_id,
                  req.session.user.id,
                  req.session.user.userId,
                  new Date(),
                  req.params.activity_id,
                  req.session.user.userId
                ], function(err, data){
                  callback(null, data);
                });
            }]
        }, function(error, results){
            res.redirect('/student/activity/public/'+req.params.activity_id);
        });
    }
});

module.exports = router;