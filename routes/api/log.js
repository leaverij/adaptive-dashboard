var express = require('express');
var router = express.Router();
var activityModal = require('../../models/Activity');
var logModel = require('../../models/Log');
var materialModel = require('../../models/Material');
var statementModal = require('../../models/Statement');
var config = require('../../config');
var async = require('async');
var moment = require('moment');

var zip = require('../../util/archiver_zip');

router.get('/', function(req, res, next){
    res.send('log api is ready.');
});

//學生閱讀 PDF 頁面
router.post('/pdf/read', function(req, res, next){
    if(req.session.user){
        async.auto({
            logPDFReadResult: function(callback){
                logModel.studentReadSinglePDFPage([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.page,
                    req.body.time_spent,
                    req.body.start_date,
                    req.body.end_date,
                    req.session.user.userId,
                    req.body.time_spent
                ], function(err, data){
                    callback(null, data);
                });
            },
            getPDFMaterialCompletion: function(callback){
                materialModel.getPDFMaterialCompletion([
                    req.body.activity_id,
                    req.body.material_id,
                    req.session.user.userId,
                    req.body.pass_val
                ], function(err, data){
                    callback(null, data);
                });
            },
            updatePDFMaterialCompletionStatus:['getPDFMaterialCompletion', function(results, callback){
                var completedPageCounts = results['getPDFMaterialCompletion'];
                if(completedPageCounts==Number(req.body.total_pages)){ // 100% complete
                    activityModal.studentUpdateActivityCompletion([
                        req.body.activity_completion,
                        req.body.time_spent,
                        req.body.activity_id,
                        req.session.user.userId
                    ], function(err1, data1){
                        materialModel.updatePDFMaterialCompletedStatus([
                            req.body.time_spent,
                            req.body.activity_id,
                            req.body.material_id,
                            req.session.user.userId
                        ], function(err, data){
                            callback(null, data);
                        });
                    });
                }else{
                    materialModel.updatePDFMaterialCompletingStatus([
                        Math.round(completedPageCounts/req.body.total_pages*100),
                        req.body.time_spent,
                        req.body.activity_id,
                        req.body.material_id,
                        req.session.user.userId
                    ], function(err, data){
                        callback(null, data);
                    });
                }
            }]
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生閱讀 PDF 頁面(適性)
router.post('/pdf/adaptive/read', function(req, res, next){
    if(req.session.user){
        async.auto({
            logPDFReadResult: function(callback){
                logModel.studentReadAdaptiveSinglePDFPage([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.material_uuid,
                    req.body.page,
                    req.body.time_spent,
                    req.body.start_date,
                    req.body.end_date,
                    req.session.user.userId,
                    req.body.time_spent
                ], function(err, data){
                    callback(null, data);
                });
            },
            getAdaptivePDFMaterialCompletion: function(callback){
                materialModel.getAdaptivePDFMaterialCompletion([
                    req.body.activity_id,
                    req.body.material_id,
                    req.session.user.userId,
                    req.body.material_uuid,
                    req.body.pass_val
                ], function(err, data){
                    callback(null, data);
                });
            },
            updatePDFMaterialCompletionStatus:['getAdaptivePDFMaterialCompletion', function(results, callback){
                var completedPageCounts = results['getAdaptivePDFMaterialCompletion'];
                if(completedPageCounts==Number(req.body.total_pages)){ // 100% complete
                    materialModel.updateAdaptivePDFMaterialCompletedStatus([
                        req.body.time_spent,
                        req.body.activity_id,
                        req.body.material_id,
                        req.session.user.userId,
                        req.body.material_uuid
                    ], function(err, data){
                        callback(null, data);
                    });
                }else{
                    materialModel.updateAdaptivePDFMaterialCompletingStatus([
                        Math.round(completedPageCounts/req.body.total_pages*100),
                        req.body.time_spent,
                        req.body.activity_id,
                        req.body.material_id,
                        req.session.user.userId,
                        req.body.material_uuid
                    ], function(err, data){
                        callback(null, data);
                    });
                }
            }]
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生觀看影片片段
router.post('/video/watch', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentUpdateVideoWatched: function(callback){
                logModel.studentUpdateVideoWatched([
                    Math.round(req.body.total_duration/req.body.criteria*100),
                    Math.round(parseFloat(req.body.duration)),
                    parseInt(req.body.activity_id),
                    parseInt(req.body.material_id),
                    req.body.user_cooc_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            logWatchVideoClip: function(callback){
                logModel.watchVideoClip([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.body.user_cooc_id,
                    new Date(req.body.start_date),
                    new Date(req.body.end_date),
                    req.body.duration,
                    '2',
                    config.xapi.recipe.acrossx.video.action.watch.verb,
                    req.body.start_point,
                    req.body.end_point
                ], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生觀看影片片段(適性)
router.post('/video/adaptive/watch', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentUpdateVideoWatched: function(callback){
                logModel.studentUpdateAdaptiveVideoWatched([
                    Math.round(req.body.total_duration/req.body.criteria*100),
                    Math.round(parseFloat(req.body.duration)),
                    parseInt(req.body.activity_id),
                    parseInt(req.body.material_id),
                    req.body.user_cooc_id,
                    req.body.material_uuid
                ], function(err, data){
                    callback(null, data);
                });
            },
            logWatchVideoClip: function(callback){
                logModel.watchVideoClip([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.body.user_cooc_id,
                    new Date(req.body.start_date),
                    new Date(req.body.end_date),
                    req.body.duration,
                    '2',
                    config.xapi.recipe.acrossx.video.action.watch.verb,
                    req.body.start_point,
                    req.body.end_point
                ], function(err,data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生跳過影片片段
router.post('/video/skip', function(req, res, next){
    if(req.session.user){
        logModel.watchVideoClip([
            req.body.activity_id,
            req.body.material_id,
            req.body.creator_id,
            req.body.creator_cooc_id,
            req.body.user_cooc_id,
            new Date(req.body.start_date),
            new Date(req.body.end_date),
            req.body.duration,
            '2',
            config.xapi.recipe.acrossx.video.action.skip.verb,
            req.body.start_point,
            req.body.end_point
        ], function(err,data){
            res.json({success:true});
        });
    }
});

//學生完成影片
router.post('/video/complete/:activity_id/:material_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentCompleteWeb: function(callback){
                logModel.studentCompleteVideo([
                    req.body.time_spent,
                    req.params.activity_id,
                    req.params.material_id,
                    req.session.user.userId
                ], function(err, data){
                    callback(null, data);
                });
            },
            studentUpdateActivityCompletion: function(callback){
                activityModal.studentUpdateActivityCompletion([
                    req.body.activity_completion,
                    req.body.time_spent,
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err, data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.json({
                success: false,
                msg: 'unauthorized'
            });
        });
    }
});

//學生完成影片(適性)
router.post('/video/adaptive/complete/:activity_id/:material_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentCompleteAdaptiveVideo: function(callback){
                logModel.studentCompleteAdaptiveVideo([
                    req.body.time_spent,
                    req.params.activity_id,
                    req.params.material_id,
                    req.session.user.userId,
                    req.body.material_uuid
                ], function(err, data){
                    callback(null, data);
                });
            }
        }, function(err, results){
            res.json({
                success: false,
                msg: 'unauthorized'
            });
        });
    }
});

//學生瀏覽網頁(適性)
router.post('/web/adaptive/read', function(req, res, next){
    if(req.session.user){
        async.auto({
            updateWebRead: function(callback){
                logModel.studentUpdateAdaptiveWebRead([
                    Number(req.body.completion_rate),
                    Number(req.body.total_time_spent),
                    (Number(req.body.completion_rate)==100)?true:false,
                    parseInt(req.body.activity_id),
                    parseInt(req.body.material_id),
                    req.session.user.userId,
                    req.body.material_uuid
                ], function(err, data){
                    callback(data);
                });
            },
            logViewWeb: function(callback){
                logModel.viewWeb([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.body.user_cooc_id,
                    new Date(req.body.start_date),
                    new Date(req.body.end_date),
                    req.body.time_spent,
                    '1',
                    config.xapi.recipe.adb.action.read.verb
                ], function(err,data){
                    callback(data);
                });
            }
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生瀏覽網頁
router.post('/web/read', function(req, res, next){
    if(req.session.user){
        async.auto({
            updateWebRead: function(callback){
                logModel.studentUpdateWebRead([
                    req.body.completion_rate,
                    req.body.total_time_spent,
                    (req.body.completion_rate==100)?true:false,
                    req.body.activity_id,
                    req.body.material_id,
                    req.session.user.userId
                ], function(err, data){
                    callback(data);
                });
            },
            logViewWeb: function(callback){
                logModel.viewWeb([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.body.user_cooc_id,
                    new Date(req.body.start_date),
                    new Date(req.body.end_date),
                    req.body.time_spent,
                    '1',
                    config.xapi.recipe.adb.action.read.verb
                ], function(err,data){
                    callback(data);
                });
            }
        }, function(err, results){
            res.json({success:true});
        });
    }
});

//學生完成網頁
router.post('/web/complete/:activity_id/:material_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentCompleteWeb: function(callback){
                logModel.studentCompleteWeb([
                    req.params.activity_id,
                    req.params.material_id,
                    req.session.user.userId
                ], function(err, data){
                    callback(data);
                });
            },
            studentUpdateActivityCompletion: function(callback){
                activityModal.studentUpdateActivityCompletion([
                    req.body.activity_completion,
                    req.body.time_spent,
                    req.params.activity_id,
                    req.session.user.userId
                ], function(err, data){
                    callback(data);
                });
            }
        }, function(err, results){
            res.json({
                success: false,
                msg: 'unauthorized'
            });
        });
    }else{
        
    }
});

//學生完成網頁(適性)
router.post('/adaptive/web/complete/:activity_id/:material_id', function(req, res, next){
    if(req.session.user){
        async.auto({
            studentCompleteWeb: function(callback){
                logModel.studentCompleteAdaptiveWeb([
                    req.params.activity_id,
                    req.params.material_id,
                    req.session.user.userId,
                    req.body.material_uuid
                ], function(err, data){
                    callback(data);
                });
            }
        }, function(err, results){
            res.json({
                success: true,
                msg: ''
            });
        });
    }else{
        res.json({
            success: false,
            msg: 'unauthorized'
        });
    }
});

//學生在觀看影片教材時筆記
router.post('/material/video/note', function(req, res, next){
    if(req.session.user){
        var note_date = new Date();
        async.auto({
            noteVideoMaterial: function(callback){
                logModel.noteVideoMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    req.body.note,
                    req.body.type,
                    req.body.point,
                    note_date
                ], function(err,data){
                    callback(null,data);
                });
            },
            logNoteVideoMaterial: function(callback){
                logModel.logNoteVideoMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    note_date,
                    req.body.type,
                    config.xapi.recipe.adb.action.note.verb,
                    req.body.point,
                    req.body.note
                ], function(err,data){
                    callback(null,data);
                });
            }
        }, function(err, results){
            res.json({
                success:true,
                data:{
                    id:results['noteVideoMaterial'],
                    note_date:note_date
                }
            });
        });
    }
});

//學生在瀏覽網頁教材時筆記
router.post('/material/web/note', function(req, res, next){
    if(req.session.user){
        var note_date = new Date();
        async.auto({
            noteWebMaterial: function(callback){
                logModel.noteWebMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    req.body.note,
                    req.body.type,
                    note_date
                ], function(err,data){
                    callback(null,data);
                });
            },
            logNoteWebMaterial: function(callback){
                logModel.logNoteWebMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    note_date,
                    req.body.type,
                    config.xapi.recipe.adb.action.note.verb,
                    req.body.note
                ], function(err,data){
                    callback(null,data);
                });
            }
        }, function(err, results){
            res.json({
                success:true,
                data:{
                    id:results['noteWebMaterial'],
                    note_date:note_date
                }
            });
        });
    }
});

//學生在瀏覽PDF教材時筆記
router.post('/material/pdf/note', function(req, res, next){
    if(req.session.user){
        var note_date = new Date();
        async.auto({
            notePDFMaterial: function(callback){
                logModel.notePDFMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    req.body.note,
                    req.body.type,
                    note_date
                ], function(err,data){
                    callback(null,data);
                });
            },
            logNotePDFMaterial: function(callback){
                logModel.logNotePDFMaterial([
                    req.body.activity_id,
                    req.body.material_id,
                    req.body.creator_id,
                    req.body.creator_cooc_id,
                    req.session.user.userId,
                    note_date,
                    req.body.type,
                    config.xapi.recipe.adb.action.note.verb,
                    req.body.note
                ], function(err,data){
                    callback(null,data);
                });
            }
        }, function(err, results){
            res.json({
                success:true,
                data:{
                    id:results['notePDFMaterial'],
                    note_date:note_date
                }
            });
        });
    }
});

//學生刪除筆記
router.post('/material/note/remove/:material_id/:note_id', function(req, res, next){
    if(req.session.user){
        logModel.deleteMaterialNote([
            req.params.note_id,
            req.params.material_id,
            req.session.user.userId
        ], function(err,data){
            res.json({
                success:true
            });
        });
    }
});

//學生更新筆記
router.post('/material/note/update/:material_id/:note_id', function(req, res, next){
    if(req.session.user){
        logModel.updateMaterialNote([
            req.body.note,
            req.params.note_id,
            req.params.material_id,
            req.session.user.userId
        ], function(err,data){
            res.json({
                success:true
            });
        });
    }
});

//取得最近10筆 Statements
router.get('/xapi/list/:user_id', function(req, res, next){
    statementModal.getLastTenStatementsByUserId({
        user_id:req.params.user_id,
        skip:parseInt(req.query.skip),
        limit:parseInt(req.query.limit)
    }, function(err, data){
        res.json({
            success:true,
            data:data
        });
    });
});

//取得最近30天每天記錄數量
router.get('/xapi/counts/thirty/:user_id', function(req, res, next){
    statementModal.getLastThirtyDaysStatementCountsPerDay({
        user_id:req.params.user_id
    }, function(err, data){
        var dayCounts = [];
        for(var d=0;d<data.length;d++){
            var dayData = data[d];
            dayCounts.push([
                moment(dayData.date, 'YYYY-MM-DD').valueOf(),
                dayData.count
            ]);
        }
        res.json({
            success:true,
            data:dayCounts
        });
    });
});

module.exports = router;