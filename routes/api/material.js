var express = require('express');
var router = express.Router();
var materialModel = require('../../models/Material');
var async = require('async');

router.get('/',function(req, res, next){
    res.send('material api');
});

//教師儲存教材權重
router.post('/weight/:material_id', function(req, res, next){
    if(req.session.user){
        materialModel.teacherUpdateMaterialWeight([req.body.weight,req.params.material_id], function(err,data){
            res.json({success:true});
        });
    }
});

//教師平均權重
router.post('/weight/avg', function(req, res, next){
    if(req.session.user){
        async.each(JSON.parse(req.body.materials),function(row, callback){
            materialModel.teacherUpdateMaterialWeight([row.weight,row.material_id],function(err,data){
                callback();
            });
        },function(err){
            res.json({success:true});
        });
    }
});

//教師取得所有教材
router.get('/teacher', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        materialModel.teacherGetAllMaterials({
            creator_id:req.session.user.id,
            search:req.query.search.value,
            order:'asc',
            start:req.query.start,
            end:req.query.start+req.query.length,
            sort:sortStr,
            order:req.query.order[0]['dir'],
            draw:req.query.draw
        }, function(err, data){
            res.json(data);
        });
    }
});

//適性活動直接新增網頁教材
router.post('/teacher/adaptive/web', function(req, res, next){
    if(req.session.user){
        var insertData = [
            req.body.activity_id,
            req.body.name,
            req.body.description,
            req.body.type,
            req.body.url,
            req.session.user.id,
            req.body.goal,
            req.body.bloom,
            req.body.embedded,
            req.body.upload_name,
            0,
            0,
            0,
            1,
            req.body.pass_method,
            req.body.pass_val
        ];
        materialModel.createWebMaterial(insertData, function(err, data){
            res.json({
                success: true,
                data:{
                    id:data,
                    name:req.body.name
                }
            });
        });
    }
});

//適性活動直接新增影片教材
router.post('/teacher/adaptive/video', function(req, res, next){
    if(req.session.user){
        var insertData = [
            req.body.activity_id,
            req.body.name,
            req.body.description,
            req.body.type,
            req.body.upload_name,
            req.body.url,
            req.session.user.id,
            req.body.goal,
            req.body.bloom,
            req.body.pass_method,
            req.body.pass_val,
            0,
            req.body.duration,
            0,
            0,
            1
        ];
        materialModel.createVideoMaterial(insertData, function(err, data){
            res.json({
                success: true,
                data:{
                    id:data,
                    name:req.body.name
                }
            });
        });
    }
});

//適性活動直接新增PDF教材
router.post('/teacher/adaptive/pdf', function(req, res, next){
    if(req.session.user){
        var insertData = [
            req.body.activity_id,
            req.body.name,
            req.body.description,
            req.body.type,
            req.body.upload_name,
            req.body.url,
            req.session.user.id,
            req.body.goal,
            req.body.bloom,
            req.body.pass_method,
            req.body.pass_val,
            0,
            0,
            1,
            1,
            req.body.page
        ];
        materialModel.createPDFMaterial(insertData, function(err,data){
            res.json({
                success: true,
                data:{
                    id:data,
                    name:req.body.name
                }
            });
        });
    }
});

//適性活動直接新增測驗教材
router.post('/teacher/adaptive/assessment', function(req, res, next){
    if(req.session.user){
        var bodyData = req.body;
        var quizs = [];
        for(var bd in bodyData){
          if(bd.indexOf('no')!=-1){
            var quizId = bodyData[bd];
            var quizScore = parseInt(bodyData['score'+quizId]);
            var quizNo = parseInt(bodyData['num'+quizId]);
            quizs.push({
              id:quizId,
              no:quizNo,
              score:quizScore
            });
          }
        }
        var insertData = [
            req.body.activity_id,
            req.body.name,
            req.body.description,
            req.body.type,
            req.session.user.id,
            req.body.goal,
            req.body.bloom,
            req.body.pass_method,
            req.body.pass_val,
            0,
            1,
            JSON.stringify(quizs),
            req.body.total_score
        ];
        materialModel.createAssessmentMaterial(insertData, function(err,data){
            res.json({
                success: true,
                data:{
                    id:data,
                    name:req.body.name
                }
            });
        });
    }
});

//適性活動直接新增文章教材
router.post('/teacher/adaptive/article', function(req, res, next){
    if(req.session.user){
        var insertData = [
            req.body.activity_id,
            req.body.name,
            req.body.description,
            req.body.type,
            req.session.user.id,
            req.body.goal,
            req.body.bloom,
            req.body.pass_method,
            req.body.pass_val,
            0,
            1,
            0,
            0
        ];
        materialModel.createArticleMaterial(insertData, function(err, data){
            res.json({
                success: true,
                data:{
                    id:data,
                    name:req.body.name
                }
            });
        });
    }
});

module.exports = router;