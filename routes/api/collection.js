var express = require('express');
var router = express.Router();

var collectionModel = require('../../models/Collection');
var async = require('async');

//學生新增收藏
router.post('/', function(req, res, next){
    var returnData = {
        success:true,
        data:null
    };
    var createDate = new Date();
    async.auto({
        getCollectionByName: function(callback){
            collectionModel.getCollectionByName([req.body.col_name,req.session.user.userId], function(err,data){
                callback(null,data);
            });
        },
        createCollection: ['getCollectionByName', function(results,callback){
            var getCollection = results['getCollectionByName'];
            if(getCollection.length!=0){
                returnData.data = getCollection[0];
                callback(null, getCollection);
            }else{
                collectionModel.createCollection([
                    req.body.col_name,
                    req.body.col_description,
                    req.session.user.id,
                    req.session.user.userId,
                    createDate
                ], function(err,data){
                    returnData.data = {
                        id:data,
                        activity_id:req.body.activity_id,
                        name:req.body.col_name,
                        description:req.body.col_description,
                        create_date:createDate,
                        creator_id:req.session.user.id
                    };
                    callback(null,returnData);
                });
            }
        }]
    }, function(err, results){        
        res.json(returnData);
    }); 
});

//學生更新收藏
router.post('/activity/metadata', function(req, res, next){
    if(req.session.user){
        async.auto({
            getCollectionByName: function(callback){
                collectionModel.getCollectionByName([req.body.edit_col_name,req.session.user.userId], function(err,data){
                    callback(null, data);
                });
            },
            updateCollection:['getCollectionByName', function(results,callback){
                if(results['getCollectionByName'].length==0){
                    collectionModel.updateCollection([
                        req.body.edit_col_name,
                        req.body.edit_col_description,
                        req.body.edit_col_id,
                        req.session.user.userId
                    ], function(err,data){
                        res.json({
                            success:true,
                            id:req.body.edit_col_id,
                            name:req.body.edit_col_name,
                            description:req.body.edit_col_description
                        });
                    });
                }else{
                    res.json({
                        success:false,
                        msg:'已存在相同名稱的收藏!'
                    });
                }
            }]
        }, function(err, results){

        });
    }
});

//學生刪除收藏
router.post('/remove', function(req, res, next){
    if(req.session.user){
        collectionModel.removeCollection([req.body.col_id,req.session.user.userId], function(err,data){
            res.json({
                success:true,
                id:req.body.col_id
            });
        });
    }
});

//學生更新活動收藏(加入或移除活動)
router.post('/activity', function(req, res, next){
    var returnData = {
        success:true
    };
    if(req.session.user){
        if(req.body.is_collect==='1'){
            collectionModel.removeActivityFromCollection([req.body.collection_id,req.body.activity_id,req.session.user.userId], function(err,data){
                if(err){
                    returnData.success = false;
                }
                res.json(returnData);
            });
        }else{
            collectionModel.addActivityToCollection([req.body.collection_id,req.body.activity_id,req.session.user.userId,req.body.creator_id], function(err,data){
                if(err){
                    returnData.success = false;
                }
                res.json(returnData);
            });
        }
    }
});

module.exports = router;