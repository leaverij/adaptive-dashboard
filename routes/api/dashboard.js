var express = require('express');
var router = express.Router();
var analyticModel = require('../../models/Analytic');
var activityModel = require('../../models/Activity');
var materialModel = require('../../models/Material');
var groupModel = require('../../models/Group');
var async = require('async');

router.get('/',function(req, res, next){
    res.send('dashboard api');
});

//AWT教師首頁資料:取得活動類型
router.get('/awt/activity_type_counts', function(req, res, next){
    if(req.session.user){
        analyticModel.getAwtTeacherHomepageData_activity_types([req.session.user.id], function(err, data){
            res.json({
                success:true,
                data:data
            });
        });
    }else{
        res.json({
            success:false,
            msg:'unauthorized'
        });
    }
});

//AWT教師首頁資料:取得教材類型
router.get('/awt/material_type_counts', function(req, res, next){
    if(req.session.user){
        analyticModel.getAwtTeacherHomepageData_material_types([req.session.user.id], function(err, data){
            res.json({
                success:true,
                data:data
            });
        });
    }else{
        res.json({
            success:false,
            msg:'unauthorized'
        });
    }
});

//教師取得教材所有學生學習紀錄(適性活動)
router.post('/awt/adaptive/material/progress', function(req, res, next){
    if(req.body.group_id){
        async.auto({
            getGroupMembers: function(callback){
                groupModel.getSingleGroupMembersByGroupId([
                    req.body.group_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            getAdaptiveSingleMaterialStudentLearnStatusByGroup:['getGroupMembers', function(results, callback){
                var groupMembers = results['getGroupMembers'];
                var assignedStudentIds = [];
                for(var stu_index=0;stu_index<groupMembers.length;stu_index++){
                  assignedStudentIds.push(groupMembers[stu_index].user_cooc_id);
                }
                activityModel.getAdaptiveSingleMaterialStudentLearnStatusByGroup({
                    activity_id:req.body.activity_id,
                    material_id:req.body.material_id,
                    material_uuid:req.body.material_uuid,
                    assign_groups:assignedStudentIds
                }, function(err, data){
                    callback(null, data);
                });
            }]
        }, function(err, results){
            res.json({
                success:true,
                data:results['getAdaptiveSingleMaterialStudentLearnStatusByGroup']
            });
        });
    }else{
        activityModel.getAdaptiveSingleMaterialStudentLearnStatus([
            req.body.activity_id,
            req.body.material_id,
            req.body.material_uuid
        ], function(error, data){
            res.json({
                success:true,
                data:data
            });
        });
    }
});

//教師取得活動所有學生學習紀錄(適性活動)
router.post('/awt/adaptive/activity/progress', function(req, res, next){
    if(req.body.group_id){
        async.auto({
            getActivityMaterials: function(callback){
                materialModel.studentGetMaterialsByUserId([
                    req.body.content_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            getGroupMembers: function(callback){
                groupModel.getSingleGroupMembersByGroupId([
                    req.body.group_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            getSingleMaterialStudentLearnStatus: ['getActivityMaterials', 'getGroupMembers', function(results, callback1){
                var materials = results['getActivityMaterials'];
                if(materials && materials.length!=0){
                    var groupMembers = results['getGroupMembers'];
                    var assignedStudentIds = [];
                    for(var stu_index=0;stu_index<groupMembers.length;stu_index++){
                      assignedStudentIds.push(groupMembers[stu_index].user_cooc_id);
                    }
                    async.each(materials, function(material, callback){
                        activityModel.getSingleMaterialStudentLearnStatusFromAdaptiveActivityByGroup({
                            material_id:material.id,
                            material_uuid:req.body.material_uuid,
                            activity_id:req.body.activity_id,
                            assign_groups:assignedStudentIds
                        }, function(err, data){
                            material.student = data;
                            callback(null, data);
                        });
                    },function(err){
                        callback1(null, materials);
                    });
                }else{
                    materials.student = [];
                    callback1(null, materials);
                }
            }]
        }, function(err, results){
            res.json({
                success:true,
                data:{
                    materials: results['getActivityMaterials'],
                    material_learn: results['getSingleMaterialStudentLearnStatus']
                }
            });
        });
    }else{
        async.auto({
            getActivityMaterials: function(callback){
                materialModel.studentGetMaterialsByUserId([
                    req.body.content_id
                ], function(err, data){
                    callback(null, data);
                });
            },
            getSingleMaterialStudentLearnStatus: ['getActivityMaterials', function(results, callback1){
                var materials = results['getActivityMaterials'];
                if(materials && materials.length!=0){
                    async.each(materials, function(material, callback){
                        activityModel.getSingleMaterialStudentLearnStatusFromAdaptiveActivity([
                            material.id,
                            req.body.material_uuid,
                            req.body.activity_id
                        ], function(err, data){
                            material.student = data;
                            callback(null, data);
                        });
                    },function(err){
                        callback1(null, materials);
                    });
                }else{
                    materials.student = [];
                    callback1(null, materials);
                }
            }]
        }, function(err, results){
            res.json({
                success:true,
                data:{
                    materials: results['getActivityMaterials'],
                    material_learn: results['getSingleMaterialStudentLearnStatus']
                }
            });
        });
    }
});

router.get('/json', function(req, res, next){
    res.json({
        "links":[
            {"source":0, "target":1, "value":25},
            {"source":1, "target":2, "value":3},
            {"source":1, "target":3, "value":8},
            {"source":2, "target":4, "value":2},
            {"source":2, "target":5, "value":1}
        ],
        "nodes":[
            {"id":"start","name":"Start"},
            {"id":"employee","name":"Employee"},
            {"id":"facebook","name":"Facebook測試履歷"},
            {"id":"transformer","name":"便型金剛"},
            {"id":"activity","name":"一班活動"},
            {"id":"check","name":"check 一下"}
        ]
    });
});

module.exports = router;