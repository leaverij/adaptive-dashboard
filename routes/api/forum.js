var express = require('express');
var router = express.Router();

var forumModel = require('../../models/Forum');
var async = require('async');

router.get('/', function(req, res, next){
    res.send('forum api');
});

//取得問題
router.post('/activity/:activity_id/questions', function(req, res, next){
    if(req.session.user){
        forumModel.getQuestions({
            activity_id:req.params.activity_id,
            search:req.body.q,
            sort:'post_date',
            order:'desc',
            start:req.body.start,
            end:req.body.end,
            draw:req.body.draw
        }, function(err,data){
            res.json(data);
        });
    }
});

//回應問題
router.post('/activity/:activity_id/questions/:question_id/responses', function(req, res, next){
    if(req.session.user){
        async.auto({
            updateQuestionResponseCounts: function(callback){
                forumModel.updateQuestionResponseCounts([
                    req.params.question_id,
                    req.params.activity_id
                ], function(err,data){
                    callback(null,true);
                });
            },
            responseQuestion: function(callback){
                forumModel.responseQuestion([
                    req.params.question_id,
                    req.params.activity_id,
                    req.body.response,
                    req.session.user.id,
                    req.session.user.userId
                ], function(err,data){
                    callback(null,data);
                });
            }
        }, function(err, results){
            res.redirect('/forum/teacher/activity/'+req.params.activity_id+'/questions/'+req.params.question_id);
        });
    }
});

//追蹤問題
router.post('/activity/:activity_id/questions/:question_id/trace', function(req, res, next){
    if(req.session.user){
        forumModel.traceQuestion([
            req.params.activity_id,
            req.params.question_id,
            req.session.user.userId,
            req.body.creator_cooc_id
        ], function(err,data){
            res.json({
                success:true
            });
        });
    }
});

//取消追蹤問題
router.post('/activity/:activity_id/questions/:question_id/canceltrace', function(req, res, next){
    if(req.session.user){
        forumModel.cancelTraceQuestion([req.params.question_id,req.session.user.userId], function(err,data){
            res.json({
                success:true
            });
        });
    }
});

module.exports = router;