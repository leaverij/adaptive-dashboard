var express = require('express');
var router = express.Router();

var groupModel = require('../../models/Group');
var userModel = require('../../models/User');

router.get('/',function(req, res, next){
    res.send('group api');
});

//教師取得全部群組
router.get('/all', function(req, res, next){
    if(req.session.user){
        var sortStr = 'create_date';
        if(req.query.order[0]['column']=='0'){
            sortStr = 'name';
        }
        groupModel.getAllGroupsByTeacherId({
            draw:parseInt(req.query.draw),
            creator_id:req.session.user.id,
            search:req.query.search.value,
            start:parseInt(req.query.start),
            end:parseInt(req.query.start)+parseInt(req.query.length),
            sort:sortStr,
            order:req.query.order[0]['dir']     
        },function(err,data){
            res.json(data);
        });
    }
});

//教師取得全部群組(下拉選單)
router.get('/all/d', function(req, res, next){
    if(req.session.user){
        groupModel.getAllGroupsByTeacherIdDropdown([req.session.user.id], function(err,data){
            res.json(data);
        });
    }
});

//教師取得酷課雲學年學期資訊
router.get('/semester/:school_code', function(req, res, next){
    if(req.session.user){
        userModel.getSemesterInfo(req.params.school_code, function(err,data){
            res.json(data);
        });
    }
});

//教師取得酷課雲班級學生資訊
router.get('/class/:term_id', function(req, res, next){
    if(req.session.user){
        userModel.getCoocStudentList({
            termId:req.params.term_id,
            userId:req.session.user.userId
        }, function(err,data){
            res.json(data);
        });
    }
});

//教師加入酷課雲成員
router.post('/member/add/:group_id', function(req, res, next){
    if(req.session.user){
        groupModel.addCoocClassMember([JSON.parse(req.body.members)],function(addMemberErr,addMemberData){
            groupModel.getSingleGroupMembersByGroupId([req.params.group_id], function(err, data){
                res.json(data);
            });
        });
    }
});

//教師取得單一群組成員
router.get('/member/get/:id', function(req, res, next){
    if(req.session.user){
        groupModel.getSingleGroupMembersByGroupId([req.params.id],function(err,data){
            res.json(data);
        });
    }
});

//教師移除酷課雲成員
router.post('/member/remove/:group_id', function(req, res, next){
    var result = {
        success:true,
        msg:''
    };
    if(req.session.user){
        groupModel.checkGroupOwner([
            req.params.group_id,
            req.session.user.userId
        ], function(checkErr,checkResult){
            if(checkResult.length!=0){
                groupModel.removeCoocClassMember([
                    req.params.group_id,
                    req.body.user_cooc_id
                ], function(err,data){
                    res.json(result);
                });
            }else{
                result.success = false;
                result.msg = 'do not own this group';
                res.json(result);
            }
        }); 
    }
});

module.exports = router;