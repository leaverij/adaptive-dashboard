var express = require('express');
var router = express.Router();

var analyticModel = require('../models/Analytic');
var formidable = require('formidable');
var util = require('util');
var path = require('path');
var fs = require('fs-extra');
var md5 = require('md5');

var config = require('../config');

/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.session && req.session.user){
    var role_name = req.session.user.role_name;
    if(role_name==='教師'){
      res.redirect('/teacher');
      return;
    }else{
      res.redirect('/student/activity?p=1');
      return;
    }
  }
  analyticModel.getSystemOverview(function(err,results){
      if(req.session.user){
        res.render('index', { 
          title: 'Adaptive', 
          overview:results,
          cooc_header:req.app.locals.cooc_header_login,
          cooc_footer:req.app.locals.cooc_footer,
          user:req.session.user
        });
      }else{
        res.render('index', { 
          title: 'Adaptive', 
          overview:results,
          cooc_header:req.app.locals.cooc_header,
          cooc_footer:req.app.locals.cooc_footer,
          user:null
        });
      }
  });
});

//上傳檔案測試
router.post('/upload/test', function(req, res, next){
  var form = new formidable.IncomingForm();
  //解析上傳的檔案
  form.parse(req, function(err, fields, files){
    res.writeHead(200, {'content-type': 'text/plain'});
    res.write('收到上傳檔案:\n\n');
    res.end(util.inspect({fields: fields, files: files}));
  });
  return;
});

//上傳活動圖片
router.post('/upload/thumbnail/:activity_id',function(req, res, next){
  var uploadForm = new formidable.IncomingForm();
  uploadForm.encoding = 'UTF-8'; //UTF8編碼
  uploadForm.uploadDir = config.fileupload.tempDir;
  uploadForm.maxFieldsSize = config.fileupload.maxSize;
  uploadForm.hash = 'md5'; //checksums，md5或sha1
  uploadForm.multiples = false; //是否允許多個檔案上傳
  uploadForm.keepExtensions = true; //儲存至uploadDir時保留原始副檔名
  uploadForm.type = 'multipart'; //multipart或urlencoded
  uploadForm.maxFieldsSize = 2*1024*1024; //限制除了檔案欄位外的欄位的大小總合(bytes)，預設是2MB
  uploadForm.maxFields = 1000; //欄位數量限制，預設是1000，0為無限制
  uploadForm.parse(req,function(err, fields, files){
    var targetDir = config.fileupload.targetDir+path.sep+req.params.activity_id+path.sep+'thumbnail';
    var dirExist = fs.existsSync(targetDir);
    var filePath = files.activity_img.path;
    var fileExt = filePath.substring(filePath.lastIndexOf('.'));
    if(('.jpg.jpeg.png.gif').indexOf(fileExt.toLowerCase())===-1){
      res.json({success:false, message:'請上傳jpg/jpeg/png/gif檔案'});
    }
    var fileName = md5(files.activity_img.name)+'_'+new Date().getTime()+fileExt;
    var accessPath = config.fileupload.accessDir+path.sep+req.params.activity_id+path.sep+'thumbnail'+path.sep+fileName;
    var targetFile = path.join(targetDir,fileName);
    if(!dirExist){
      fs.ensureDir(targetDir, function(){
        fs.rename(filePath, targetFile, function (err) {
          if(err){
            res.json({success:false, message:'操作失败'});
          }else{
            res.json({success:true, url:accessPath});
          } 
        });
      });
    }else{
      fs.rename(filePath, targetFile, function (err) {
        if(err){
          res.json({success:false, message:'操作失败'});
        }else{
          res.json({success:true, url:accessPath});
        }
      });
    }
  });
  /*
  var io = req.app.get('socketio');
  uploadForm.on('progress',function(bytesReceived, bytesExpected){
    var percent = Math.floor(bytesReceived / bytesExpected * 100);
    var progress = {
        name: 'progress',
        percent: percent
    };
    // emit event : progress
    io.emit('progress', progress); //notify all client, no session here
  }).on('error',function(err){
    console.log('upload file error:'+err);
  }).on('end',function(){
    console.log('upload file end');
    res.end('success');
  });
  */
});

router.post('/upload/activity/:file_type/:activity_id', function(req, res, next){
  var fileType = req.params.file_type;
  var uploadForm = new formidable.IncomingForm();
  uploadForm.encoding = 'UTF-8'; //UTF8編碼
  uploadForm.uploadDir = config.fileupload.tempDir;
  uploadForm.maxFieldsSize = config.fileupload.maxSize;
  uploadForm.hash = 'md5'; //checksums，md5或sha1
  uploadForm.multiples = true; //是否允許多個檔案上傳
  uploadForm.keepExtensions = true; //儲存至uploadDir時保留原始副檔名
  uploadForm.type = 'multipart'; //multipart或urlencoded
  uploadForm.maxFieldsSize = 2000*1024*1024; //限制除了檔案欄位外的欄位的大小總合(bytes)，預設是2MB
  uploadForm.maxFields = 0; //欄位數量限制，預設是1000，0為無限制
  uploadForm.on('file', function(field, file) {
    console.log('onfile');
    var targetDir = config.fileupload.targetDir+path.sep+req.params.activity_id+path.sep+'material';
    var dirExist = fs.existsSync(targetDir);
    var filePath = file.path;
    var fileExt = filePath.substring(filePath.lastIndexOf('.'));
    if((fileExt.toLowerCase()).indexOf(fileType)===-1){
      res.json({success:false, message:'請上傳'+fileType+'檔案'});
    }
    var fileName = md5(file.name)+'_'+new Date().getTime()+fileExt;
    var accessPath = config.fileupload.accessDir+path.sep+req.params.activity_id+path.sep+'material'+path.sep+fileName;
    var targetFile = path.join(targetDir,fileName);
    var hostUrl = req.protocol+'://'+req.hostname+':'+req.socket.localPort;
    if(!dirExist){
      fs.ensureDir(targetDir, function(){
        fs.rename(filePath, targetFile, function (err) {
          if(err){
            res.json({success:false, message:'操作失败'});
          }else{
            res.json({
              success:true, 
              name:file.name,
              encode_name:fileName,
              url:hostUrl+'/uploadm/activity/'+req.params.activity_id+'/material/'+fileName
            });
          } 
        });
      });
    }else{
      fs.rename(filePath, targetFile, function (err) {
        if(err){
          res.json({success:false, message:'操作失败'});
        }else{
          res.json({
            success:true, 
            name:file.name,
            encode_name:fileName,
            url:hostUrl+'/uploadm/activity/'+req.params.activity_id+'/material/'+fileName
          });
        }
      });
    }
  });
  uploadForm.parse(req);
});

module.exports = router;
