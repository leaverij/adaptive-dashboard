var express = require('express');
var router = express.Router();
var activityModel = require('../models/Activity');
var materialModel = require('../models/Material');
var groupModel = require('../models/Group');
var userModel = require('../models/User');
var logModel = require('../models/Log');
var randomstring = require("randomstring");
var async = require('async');
var buffer = require('buffer');
var request = require('request');
var cheerio = require('cheerio');
var config = require('../config');
const uuidv4 = require('uuid/v4');

//教師首頁
router.get('/', function(req, res, next) {
  if(req.session.user){
    var returnData = {
      title: 'Teacher', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user,
      activities:null,
      groups:null
    };
    async.auto({
      getActivities: function(callback){
        activityModel.getActivityByUserId({
          creator_id:req.session.user.id,
          search:null,
          start:0,
          end:10,
          sort:'create_date',
          order:'desc',
          open:null,
          share:null
        },function(err,data){
          returnData.activities = data;
          callback(null, data);
        });
      },
      getAllGroups: function(callback){
        groupModel.teacherGetAllGroupsByCoocId([
          req.session.user.userId
        ], function(err,data){
          returnData.groups = data;
          callback(null, data);
        });
      }
    }, function(err, results){
      res.render('teacher', returnData);
    });
  }else{
    res.redirect('/');
  }
});

//教師建立新活動
router.post('/activity/create', function(req, res, next){
  if(req.session.user){
    var hierarchy = {
      id:uuidv4(),
      title:"first node"
    };
    if(req.body.activity_type=='3'){
      hierarchy = {
        'id':uuidv4(),
        'title':'',
        'name':'',
        'children':[
          {
            'id':'2',
            'name':'',
            'title':'',
            'children':[
              {
                'id':'3',
                'name':'',
                'title':''
              }
            ]
          }
        ]
      }
    }
    activityModel.createNewActivity([
      req.body.activity_name,
      parseInt(req.body.activity_type),
      req.session.user.id,
      randomstring.generate(5),
      JSON.stringify(hierarchy)
    ],function(err,data){
      switch(req.body.activity_type){
        case '2':
          res.redirect('/teacher/activity/adaptive/edit/'+data);
          break;
        case '3':
          res.redirect('/teacher/activity/assessment/edit/'+data);
          break;
        default:
          res.redirect('/teacher/activity/edit/'+data);
      }
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯一般活動
router.get('/activity/edit/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      },
      getAllGroups: function(callback){
        groupModel.teacherGetAllGroups([
          req.session.user.id
        ], function(err,data){
          callback(null, data);
        });
      }
    }, function(err, results){
      res.render('teacher/edit', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        groups:results['getAllGroups']
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯適性活動
router.get('/activity/adaptive/edit/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      },
      getAllGroups: function(callback){
        groupModel.teacherGetAllGroups([
          req.session.user.id
        ], function(err,data){
          callback(null, data);
        });
      }
    }, function(err, results){
      res.render('teacher/edit_adaptive', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        groups:results['getAllGroups']
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯前後測活動
router.get('/activity/assessment/edit/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      },
      getAllGroups: function(callback){
        groupModel.teacherGetAllGroups([
          req.session.user.id
        ], function(err,data){
          callback(null, data);
        });
      }
    }, function(err, results){
      res.render('teacher/edit_assessment', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        groups:results['getAllGroups']
      });
    });
  }else{
    res.redirect('/');
  }
});

//教師預覽活動
router.get('/activity/preview/:activity_id/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetActivityDetails: function(callback){
        activityModel.teacherGetActivityDetails([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null, data);
        });
      },
      getMaterialNote: function(callback){
        logModel.getMaterialNotes([
          req.params.material_id,
          req.session.user.userId
        ], function(err,data){
          callback(null,data);
        });
      },
      teacherGetMaterials: ['teacherGetActivityDetails', function(results,callback){
        materialModel.studentGetMaterialsByUserId(req.params.activity_id, function(err, data){
          results['teacherGetActivityDetails']['materials'] = data;
          callback(null, results['teacherGetActivityDetails']);
        })
      }]
    }, function(err, results){
      var materials = results['teacherGetActivityDetails']['materials'];
      var renderPage = 'student/activity';
      for(var i=0;i<materials.length;i++){
        var materialType = materials[i]['type'];
        if(materials[i].id==req.params.material_id){
          switch(materialType){
            case 1: //link
              renderPage = 'student/play_web';
              break;
            case 2: //video
              renderPage = 'student/play_video';
              break;
            case 3: //pdf
              renderPage = 'student/play_pdf';
              break;
            case 4: //assessment
              renderPage = 'student/play_assessment';
              break;
            case 5: //article
              renderPage = 'student/play_article';
              break;
          }
          break;
        }
      }
      if(renderPage.indexOf('assessment')!=-1){
        activityModel.studentGetAssessmentRecords([
          req.params.activity_id,
          req.params.material_id,
          req.session.user.userId
        ], function(erra, dataa){
          res.render(renderPage,{ 
            title: 'Play Activity',
            cooc_header:req.app.locals.cooc_header_login,
            cooc_footer:req.app.locals.cooc_footer, 
            user:req.session.user,
            activity:results['teacherGetActivityDetails'],
            material_id:req.params.material_id,
            host:req.protocol+'://'+req.hostname,
            teacher:true,
            notes:results['studentGetMaterialNotes'],
            assessment_results:dataa,
            lrs:config.xapi.lrs
          });
        });
      }else{
        res.render(renderPage,{ 
          title: 'Preview Activity', 
          cooc_header:req.app.locals.cooc_header_login,
          cooc_footer:req.app.locals.cooc_footer,
          user:req.session.user,
          activity:results['teacherGetActivityDetails'],
          material_id:req.params.material_id,
          host:req.protocol+'://'+req.hostname,
          teacher:true,
          notes:results['getMaterialNote']
        });
      }
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯活動:建立網頁
router.get('/activity/edit/:activity_id/web/create', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_web', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:null
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師編輯活動:編輯網頁
router.get('/activity/edit/:activity_id/web/edit/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_web', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:req.params.material_id
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存新活動:網頁
router.post('/activity/edit/:activity_id/web/add', function(req, res, next){
  if(req.session.user){
    var insertData = [
      req.params.activity_id,
      req.body.web_name,
      req.body.web_description,
      '1',
      req.body.web_url,
      req.session.user.id,
      req.body.web_goal,
      req.body.bloom,
      req.body.embedded,
      req.body.upload_name,
      req.body.weight,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order,
      req.body.pass_method,
      req.body.pass_val
    ];
    async.auto({
      createWebMaterial: function(callback){
        materialModel.createWebMaterial(insertData, function(err,data){
          callback(null, true);
        });
      },
      getActivityByUserId: ['createWebMaterial', function(results, callback){
        activityModel.getActivityDetailsByUserId([
          req.params.activity_id,
          req.session.user.id
        ],function(err,data){
          callback(null, data);
        });
      }]
    }, function(err, results){
      var activity_type = results['getActivityByUserId'][0]['type'];
      if(activity_type=='1'){
        res.redirect('/teacher/activity/edit/'+req.params.activity_id);
      }else if(activity_type=='2'){
        res.redirect('/teacher/activity/adaptive/edit/'+req.params.activity_id);
      }else{
        res.redirect('/teacher/activity/assessment/edit/'+req.params.activity_id);
      }
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存編輯活動:網頁
router.post('/activity/edit/:activity_id/web/edit', function(req, res, next){
  if(req.session.user){
    var updateData = [
      req.body.web_name,
      req.body.web_description,
      req.body.web_url,
      req.body.web_goal,
      req.body.web_bloom,
      req.body.embedded,
      req.body.upload_name,
      req.body.weight,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order,
      req.body.pass_method,
      req.body.pass_val,
      req.body.material_id,
      req.params.activity_id,
      req.session.user.id
    ];
    materialModel.updateWebMaterial(updateData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯活動:建立影片
router.get('/activity/edit/:activity_id/video/create', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_video', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:null
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師編輯活動:編輯影片
router.get('/activity/edit/:activity_id/video/edit/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_video', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:req.params.material_id
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存新活動:影片
router.post('/activity/edit/:activity_id/video/add', function(req, res, next){
  if(req.session.user){
    var insertData = [
      req.params.activity_id,
      req.body.video_name,
      req.body.video_description,
      '2',
      req.body.upload_name,
      req.body.url,
      req.session.user.id,
      req.body.video_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.video_duration,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order
    ];
    materialModel.createVideoMaterial(insertData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存編輯活動:影片
router.post('/activity/edit/:activity_id/video/edit', function(req, res, next){
  if(req.session.user){
    var updateData = [
      req.body.video_name,
      req.body.video_description,
      req.body.upload_name,
      req.body.url,
      req.body.video_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.video_duration,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order,
      req.body.material_id,
      req.params.activity_id,
      req.session.user.id
    ];
    materialModel.updateVideoMaterial(updateData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯活動:建立PDF
router.get('/activity/edit/:activity_id/pdf/create', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_pdf', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:null
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師編輯活動:編輯PDF
router.get('/activity/edit/:activity_id/pdf/edit/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_pdf', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:req.params.material_id
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存新活動:PDF
router.post('/activity/edit/:activity_id/pdf/add', function(req, res, next){
  if(req.session.user){
    var insertData = [
      req.params.activity_id,
      req.body.pdf_name,
      req.body.pdf_description,
      '3',
      req.body.upload_name,
      req.body.url,
      req.session.user.id,
      req.body.pdf_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order,
      req.body.activity_page
    ];
    materialModel.createPDFMaterial(insertData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存編輯活動:PDF
router.post('/activity/edit/:activity_id/pdf/edit', function(req, res, next){
  if(req.session.user){
    var updateData = [
      req.body.pdf_name,
      req.body.pdf_description,
      req.body.upload_name,
      req.body.url,
      req.body.pdf_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.activity_order,
      req.body.activity_page,
      req.body.material_id,
      req.params.activity_id,
      req.session.user.id
    ];
    materialModel.updatePDFMaterial(updateData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯活動:建立測驗
router.get('/activity/edit/:activity_id/assessment/create', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_assessment', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:null
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師編輯活動:編輯測驗
router.get('/activity/edit/:activity_id/assessment/edit/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_assessment', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:req.params.material_id
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存新活動:測驗
router.post('/activity/edit/:activity_id/assessment/add', function(req, res, next){
  if(req.session.user){
    var bodyData = req.body;
    var quizs = [];
    for(var bd in bodyData){
      if(bd.indexOf('no')!=-1){
        var quizId = bodyData[bd];
        var quizScore = parseInt(bodyData['score'+quizId]);
        var quizNo = parseInt(bodyData['num'+quizId]);
        quizs.push({
          id:quizId,
          no:quizNo,
          score:quizScore
        });
      }
    }
    var insertData = [
      req.params.activity_id,
      req.body.assessment_name,
      req.body.assessment_description,
      4,
      req.session.user.id,
      req.body.assessment_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.activity_order,
      JSON.stringify(quizs),
      req.body.total_score
    ];
    materialModel.createAssessmentMaterial(insertData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存編輯活動:測驗
router.post('/activity/edit/:activity_id/assessment/edit', function(req, res, next){
  if(req.session.user){
    var bodyData = req.body;
    var quizs = [];
    for(var bd in bodyData){
      if(bd.indexOf('no')!=-1){
        var quizId = bodyData[bd];
        var quizScore = parseInt(bodyData['score'+quizId]);
        var quizNo = parseInt(bodyData['num'+quizId]);
        quizs.push({
          id:quizId,
          no:quizNo,
          score:quizScore
        });
      }
    }
    var updateData = [
      req.body.assessment_name,
      req.body.assessment_description,
      req.body.assessment_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.activity_order,
      JSON.stringify(quizs),
      req.body.total_score,
      req.body.material_id,
      req.params.activity_id,
      req.session.user.id
    ];
    materialModel.updateAssessmentMaterial(updateData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

//教師編輯活動:建立文章
router.get('/activity/edit/:activity_id/article/create', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_article', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:null
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師編輯活動:編輯文章
router.get('/activity/edit/:activity_id/article/edit/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      getActivityDetailsByUserId: function(callback){
        activityModel.getActivityDetailsByUserId([req.params.activity_id,req.session.user.id],function(err,data){
          callback(null,data);
        });
      },
      getCoocUserInfo: function(callback){
        userModel.getCoocUserInfo([req.session.user.userId], function(err,data){
          callback(null,data);
        });
      },
      getMaterials: function(callback){
        materialModel.getMaterialsByUserId([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      }
    }, function(err, results){
      res.render('teacher/material_article', {
        title: 'Activity Edit', 
        cooc_header:req.app.locals.cooc_header_login,
        cooc_footer:req.app.locals.cooc_footer,
        user:req.session.user,
        activity:results,
        material_id:req.params.material_id
      });
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存新活動:文章
router.post('/activity/edit/:activity_id/article/add', function(req, res, next){
  if(req.session.user){
    var insertData = [
      req.params.activity_id,
      req.body.article_name,
      req.body.article_description,
      '5',
      req.session.user.id,
      req.body.article_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.activity_order,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val
    ];
    materialModel.createArticleMaterial(insertData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});
//教師儲存編輯活動:文章
router.post('/activity/edit/:activity_id/article/edit', function(req, res, next){
  if(req.session.user){
    var updateData = [
      req.body.article_name,
      req.body.article_description,
      req.body.article_goal,
      req.body.bloom,
      req.body.pass_method,
      req.body.pass_val,
      req.body.weight,
      req.body.activity_order,
      req.body.pass_method,
      req.body.pass_val,
      req.body.required_time_spent_method,
      req.body.required_time_spent_val,
      req.body.material_id,
      req.params.activity_id,
      req.session.user.id
    ];
    materialModel.updateArticleMaterial(updateData, function(err,data){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

//教師儲存活動基本資料
router.post('/activity/metadata', function(req, res, next){
  if(req.session.user){
    async.auto({
      saveActivityImage: function(callback){
        //var img = new Buffer(req.body.activity_data,'base64');
        callback(null,{});
      },
      teacherUpdateActivityMetadata: function(callback){
        activityModel.teacherUpdateActivityMetadata([
          req.body.activity_name,
          req.body.activity_description,
          req.body.activity_subject_id,
          req.body.activity_subject_name,
          req.body.activity_data,
          parseInt(req.body.activity_id),
          parseInt(req.session.user.id)
        ], function(err,data){
          callback(null, data);
        });
      }
    }, function(err, results){
      res.json({success:true});
    });
  }else{
    res.redirect('/');
  }
});

//檢查網頁是否允許被嵌入
router.post('/url', function(req, res, body){
  var targetUrl = req.body.url;
  request(targetUrl, function(err, response, body){
    var isBlocked = false;
    var title = '';
    // If the page was found...
    if (!err && response.statusCode == 200) {
      var $ = cheerio.load(body);
      title = $("title").text();
      // Grab the headers
      var headers = response.headers;
      // Grab the x-frame-options header if it exists
      var xFrameOptions = headers['x-frame-options'] || '';
      // Normalize the header to lowercase
      xFrameOptions = xFrameOptions.toLowerCase();
      // Check if it's set to a blocking option
      if (
        xFrameOptions.indexOf('sameorigin')!=-1 ||
        xFrameOptions.indexOf('deny')!=-1
      ) {
        isBlocked = true;
      }
    }
    res.json({
      block:isBlocked,
      title:title
    });
  });
});

//教師調整教材順序
router.post('/activity/:activity_id/material/reorder', function(req, res, next){
  if(req.session.user){
    async.each(JSON.parse(req.body.updateRow),function(row, callback){
      materialModel.teacherUpdateMaterialOrder([row.activity_order,row.material_id],function(err,data){
        callback();
      });
    },function(err){
      res.json({success:true});
    });
  }else{
    res.redirect('/');
  }
});

//教師複製一般活動
router.post('/activity/copy/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetCopyActivity:function(callback){
        activityModel.teacherGetCopyActivity([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      },
      teacherGetCopyActivityMaterial:function(callback){
        activityModel.teacherGetCopyActivityMaterial([req.params.activity_id], function(err,data){
          callback(null,data);
        });
      },
      teacherCopyActivity:['teacherGetCopyActivity', function(results,callback){
        var activity = results['teacherGetCopyActivity'];
        activityModel.teacherCopyActivity([
          req.body.activity_name,
          activity.type,
          activity.description,
          activity.goal,
          activity.creator_id,
          activity.subject_id,
          activity.subject_name,
          activity.grade,
          activity.school,
          activity.semester,
          activity.is_share,
          activity.is_public,
          activity.is_assign,
          randomstring.generate(5),
          activity.img_url,
          activity.img_data,
          activity.creator_cooc_id,
          activity.hierarchy
        ], function(err,data){
          callback(null,data);
        });
      }],
      teacherCopyActivityMaterial:['teacherCopyActivity','teacherGetCopyActivityMaterial', function(results,callback){
        var copyActivityId = results['teacherCopyActivity'];
        var materials = results['teacherGetCopyActivityMaterial'];
        var cm = [];
        for(var i=0;i<materials.length;i++){
          var copyMaterials = [];
          var material = materials[i];
          copyMaterials.push(copyActivityId);
          copyMaterials.push(material.name);
          copyMaterials.push(material.upload_name);
          copyMaterials.push(material.goal);
          copyMaterials.push(material.bloom);
          copyMaterials.push(material.description);
          copyMaterials.push(material.type);
          copyMaterials.push(material.time_limit);
          copyMaterials.push(material.url);
          copyMaterials.push(material.total_score);
          copyMaterials.push(material.weight);
          copyMaterials.push(material.activity_order);
          copyMaterials.push(material.is_delete);
          copyMaterials.push(material.creator_id);
          copyMaterials.push(material.create_date);
          copyMaterials.push(material.update_date);
          copyMaterials.push(material.embedded);
          copyMaterials.push(material.pass_method);
          copyMaterials.push(material.pass_val);
          cm.push(copyMaterials);
        }
        activityModel.teacherCopyActivityMaterial([cm], function(err,data){
          callback(null,data);
        });
      }]
    }, function(err, results){
      res.redirect('/teacher');
    });
  }else{
    res.redirect('/');
  }
});

//教師複製適性活動
/*
router.post('/activity/adaptive/copy/:activity_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherGetCopyActivity:function(callback){
        activityModel.teacherGetCopyActivity([req.params.activity_id,req.session.user.id], function(err,data){
          callback(null,data);
        });
      },
      teacherCopyActivity:['teacherGetCopyActivity', function(results,callback){
        var activity = results['teacherGetCopyActivity'];
        activityModel.teacherCopyActivity([
          req.body.activity_name,
          activity.type,
          activity.description,
          activity.goal,
          activity.creator_id,
          activity.subject_id,
          activity.subject_name,
          activity.grade,
          activity.school,
          activity.semester,
          activity.is_share,
          activity.is_public,
          activity.is_assign,
          randomstring.generate(5),
          activity.img_url,
          activity.img_data,
          activity.creator_cooc_id,
          activity.hierarchy
        ], function(err,data){
          callback(null,data);
        });
      }]
    }, function(err, results){
      res.redirect('/teacher');
    });
  }
});
*/

//教師:我的教材頁面
router.get('/material', function(req, res, next){
  if(req.session.user){
    res.render('teacher/material', {
      title: 'My Material', 
      cooc_header:req.app.locals.cooc_header_login,
      cooc_footer:req.app.locals.cooc_footer,
      user:req.session.user
    });
  }else{
    res.redirect('/');
  }
});

//教師刪除教材
router.post('/material/remove/:activity_id/:material_id', function(req, res, next){
  if(req.session.user){
    async.auto({
      teacherRemoveMaterial: function(callback){
        materialModel.teacherRemoveMaterial([
          req.params.activity_id,
          req.params.material_id
        ], function(err, data){
          callback(null, data);
        });
      },
      getMaterialsByUserId: function(callback){
        materialModel.getMaterialsByUserId([
          req.params.activity_id,
          req.session.user.id
        ], function(err, data){
          callback(null, data);
        });
      },
      updateMaterialOrder: ['getMaterialsByUserId', function(results, callback){
        var updateRow = [];
        var materials = results['getMaterialsByUserId'];
        for(var m=0;m<materials.length;m++){
          updateRow.push({
            activity_order:(m+1),
            material_id:materials[m].id
          });
        }
        async.each(updateRow, function(row, callback1){
          materialModel.teacherUpdateMaterialOrder([
            row.activity_order,
            row.material_id
          ],function(err1, data1){
            callback1(null, data1);
          });
        },function(err2){
          callback(null, true);
        });
      }]
    }, function(err, results){
      res.redirect('/teacher/activity/edit/'+req.params.activity_id);
    });
  }else{
    res.redirect('/');
  }
});

module.exports = router;