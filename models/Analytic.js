var db = require('../mysql');
var async = require('async');

//取得平台使用概況
module.exports.getSystemOverview = function(done){
	async.auto({
		getUserCounts: function(callback){
			db.get().query('select count(account.id) as users from account', null, function(error, results, fields){
				callback(null, results[0]['users']);
			});
		},
		getActivityCounts: function(callback){
			db.get().query('select count(activity.id) as activities from activity', null, function(error, results, fields){
				callback(null, results[0]['activities']);
			});
		},
		getMaterialCounts: function(callback){
			db.get().query('select count(material.id) as materials from material', null, function(error, results, fields){
				callback(null, results[0]['materials']);
			});
		}
	}, function(err, results){
		var overview = {
			users:results['getUserCounts'],
            activities:results['getActivityCounts'],
            materials:results['getMaterialCounts']
		};
		done(null, overview);
	});
};

//教師取得活動指派單一群組活動學習狀態
module.exports.teacherGetActivityAssignedGroupLearnedStatus = function(data, done){
    var queryStr = 'select '+
                      'gm.user_name,gm.user_cooc_id,saj.time_spent,'+
                      'DATE_FORMAT(saj.join_date, "%Y/%m/%d %H:%i:%s") as join_date_format,'+
                      'IFNULL(saj.completion_rate,0) as completion_rate,'+
                      'SEC_TO_TIME(IFNULL(saj.time_spent,0)) as time_spent_format '+
                   'from group_member gm '+
                   'left join activity_assign aa on aa.group_id=gm.group_id '+
                   'left join student_activity_join saj on gm.user_cooc_id=saj.user_cooc_id and saj.activity_id=? '+
                   'where aa.activity_id=? and aa.group_id=?'+
                   'order by saj.completion_rate asc,saj.time_spent asc ';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得活動指派全部群組活動學習狀態
module.exports.teacherGetAllActivityAssignedGroupLearnedStatus = function(data, done){
    var queryStr = 'select '+
                      'gm.user_name,gm.user_cooc_id,saj.time_spent,'+
                      'DATE_FORMAT(saj.join_date, "%Y/%m/%d %H:%i:%s") as join_date_format,'+
                      'IFNULL(saj.completion_rate,0) as completion_rate,'+
                      'SEC_TO_TIME(IFNULL(saj.time_spent,0)) as time_spent_format '+
                   'from group_member gm '+
                   'left join activity_assign aa on aa.group_id=gm.group_id '+
                   'left join student_activity_join saj on gm.user_cooc_id=saj.user_cooc_id and saj.activity_id=? '+
                   'where aa.activity_id=? '+
                   'group by gm.user_cooc_id '+
                   'order by saj.completion_rate asc,saj.time_spent asc ';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得公開加入的學生
module.exports.teacherGetOpenJoinedStudentStatus = function(data, done){
    var queryStr = 'select '+
                      'a.name,saj.user_cooc_id,saj.time_spent,'+
                      'DATE_FORMAT(saj.join_date, "%Y/%m/%d %H:%i:%s") as join_date_format,'+
                      'IFNULL(saj.completion_rate,0) as completion_rate,'+
                      'SEC_TO_TIME(IFNULL(saj.time_spent,0)) as time_spent_format '+
                   'from student_activity_join saj '+
                   'left join account a on a.cooc_id = saj.user_cooc_id '+
                   'where saj.activity_id='+data.activity_id+' and saj.user_cooc_id ';
    if(data.assign_groups.length!=0){
        var notInStr = '('
        for(var i=0;i<data.assign_groups.length;i++){
            if(i!=(data.assign_groups.length-1)){
                notInStr += ('\''+data.assign_groups[i]+'\',');
            }else{
                notInStr += ('\''+data.assign_groups[i]+'\'');
            }
        }
        notInStr += ')';
        queryStr += 'not in '+notInStr;
    }
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//AWT教師首頁資料:取得活動數量
module.exports.getAwtTeacherHomepageData_activity_counts = function(data, done){
    db.get().query('select count(*) as counts from activity where creator_id=? and is_delete=false', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results[0]['counts']);
    });
}
//AWT教師首頁資料:取得教材數量
module.exports.getAwtTeacherHomepageData_material_counts = function(data, done){
    db.get().query('select count(*) as counts from material where creator_id=? and is_delete=false', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results[0]['counts']);
    });
}
//AWT教師首頁資料:取得群組數量
module.exports.getAwtTeacherHomepageData_group_counts = function(data, done){
    db.get().query('select count(*) as counts from teach_group where creator_id=? and is_delete=false', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results[0]['counts']);
    });
}
//AWT教師首頁資料:取得學生數量
module.exports.getAwtTeacherHomepageData_student_counts = function(data, done){
    db.get().query('select count(distinct user_cooc_id) as counts from group_member gm left join teach_group tg on gm.group_id=tg.id where tg.creator_id=?', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results[0]['counts']);
    });
}
//AWT教師首頁資料:取得活動類型
module.exports.getAwtTeacherHomepageData_activity_types = function(data, done){
    db.get().query('select type,count(*) as counts from activity where creator_id=? group by type', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results);
    });
}
//AWT教師首頁資料:取得教材類型
module.exports.getAwtTeacherHomepageData_material_types = function(data, done){
    db.get().query('select type,count(*) as counts from material where creator_id=? group by type', data, function(error, results, fields){
        if(error) return done(error);
        done(null, results);
    });
}

//取得大學科系探索校系資料
module.exports.getSchoolRecord = function (data, done) {
	db.get().query('SELECT a.group_name, b.school_name FROM `academy_category` AS a, `academy_school` AS b WHERE a.name = b.academy_category', function (error, results, fields) {
		if (error) done(error);
		var newResult = {
			group: []
			, school: []
		};
		for (var i = 0; i < results.length; i++) {
			var result = results[i];
			var group = result['group_name'];
			var school = result['school_name'];
			var department = result['department_name'];
			if (newResult['group'].indexOf(group) === -1) {
				newResult['group'].push(group);
			}
			if (newResult['school'].indexOf(school) === -1) {
				newResult['school'].push(school);
			}
		}
		done(null, newResult);
	});
};
//取得大學科系探索校系資料篩選的結果
module.exports.getSchoolFilterRecord = function (filter, done) {
	var filterType = filter.type;
	var filterValue = filter.value;
	var sqlStr = '';
	if (filterType === '0') {
		var sqlStr = 'SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category';
		if (filterValue.group !== '') {
			sqlStr += ' AND a.group_name=' + '"' + filterValue.group + '"';
		}
		if (filterValue.college !== '') {
			sqlStr += ' AND b.school_name=' + '"' + filterValue.college + '"';
		}
		if (filterValue.department !== '') {
			sqlStr += ' AND b.department_name LIKE' + '"%' + filterValue.department + '%"';
		}
		db.get().query(sqlStr, function (error, results, fields) {
			if (error) done(error);
			done(null, results);
		});
	}
	else if (filterType === '1') {
		//依照選擇的學科去篩選
		var sqlStr = 'SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category';
		db.get().query(sqlStr, function (error, results, fields) {
			if (error) done(error);
			//計算學科占比
			results.map(function (item, index) {
				//分子權重加成
				var subjectsWeightAddition = 0;
				//分母權重相加
				var subjectsWeightTotal = 0;
				for (var i = 0; i < filterValue.length; i++) {
					var subject = filterValue[i];
					subjectsWeightAddition += item[subject];
				}
				subjectsWeightTotal = item.chinese + item.english + item.math_a + item.math_b + item.history + item.geography + item.citizen + item.physics + item.chemistry + item.biology;
				//新增屬性
				item['scale'] = parseFloat(subjectsWeightAddition / subjectsWeightTotal).toFixed(2);
			});
			done(null, results);
		});
	}
	else if (filterType === '2') {
		//依照選擇的學科去篩選
		var sqlStr = 'SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category';
		db.get().query(sqlStr, function (error, results, fields) {
			if (error) done(error);
			//計算學科Pr值占比
			results.map(function (item, index) {
				//分子權重加成
				var subjectsWeightAddition = 0;
				//分母權重相加
				var subjectsWeightTotal = 0;
				var prTotal = 0;
				for (var i = 0; i < filterValue.length; i++) {
					var subject = filterValue[i].name;
					var prValue = filterValue[i].value / 100;
					if (item[subject] !== 0) {
						subjectsWeightAddition += item[subject] * prValue;
						prTotal += Math.pow(prValue, 2);
					}
				}
				prTotal = Math.sqrt(prTotal);
				subjectsWeightTotal = prTotal + (Math.sqrt((Math.pow(item.chinese, 2) + Math.pow(item.english, 2) + Math.pow(item.math_a, 2) + Math.pow(item.math_b, 2) + Math.pow(item.history, 2) + Math.pow(item.geography, 2) + Math.pow(item.citizen, 2) + Math.pow(item.physics, 2) + Math.pow(item.chemistry, 2) + Math.pow(item.biology, 2))));
				//新增屬性
				item['similarity'] = parseFloat(subjectsWeightAddition / subjectsWeightTotal).toFixed(7);
			});
			done(null, results);
		});
	}
	else if (filterType === '3') {
		//依照選擇的學科去篩選
		var sqlStr = 'SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology, c.' + filterValue['sexValue'] + ' FROM academy_category AS a, academy_school AS b, academy_interest AS c WHERE a.name = b.academy_category AND c.academy_category = b.academy_category';
		db.get().query(sqlStr, function (error, results, fields) {
			if (error) done(error);
			var newResults = [];
			//計算學科Pr值占比
			results.map(function (item, index) {
				var interests = trimAll(item[filterValue['sexValue']], 'g').split('/');
				//IR,AI,BR(DB)
				for (var i = 0; i < interests.length; i++) {
					//IR
					var interest = interests[i];
					//IA,YG(user)
					for (var j = 0; j < filterValue['codesValue'].length; j++) {
						//IA
						var codeValue = filterValue['codesValue'][j];
						//長度要相同
						if (codeValue.length === interest.length) {
							var count = 0;
							//I,A
							for (var k = 0; k < codeValue.length; k++) {
								//I==I,A==A
								if (interest.indexOf(codeValue.charAt(k)) !== -1) {
									count++;
								}
							}
							//比對成功
							if (count === codeValue.length) {
								newResults.push(item);
							}
						}
					}
				}
			});
			done(null, newResults);
		});
	}
};