var db = require('../mysql');
var async = require('async');

//教師建立新活動
module.exports.createNewActivity = function(data,done){
    db.get().query('insert into activity (name,type,creator_id,access_code,hierarchy) values (?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//教師取得活動
module.exports.getActivityByUserId = function(data,done){
    var queryCountsStr = 'select count(*) as recordsTotal from activity where creator_id=? and is_delete=false';
    var queryStr = 'select '+
                     'activity.*,'+
                     'DATE_FORMAT(activity.create_date, "%Y/%m/%d") as create_date,'+
                     'teach_group.id as group_id,'+
                     'teach_group.name as group_name from activity '+
                   'left join teach_group on activity.assign_group=teach_group.id '+
                   'where activity.creator_id=? and activity.is_delete=false';
    var queryCountsData = [data.creator_id];
    var queryData = [data.creator_id];
    if(data.search){
        queryCountsStr += ' and activity.name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and activity.name like ?';
        queryData.push('%'+data.search+'%');
    }
    if(data.open){
        queryStr += ' and is_public=?';
        queryData.push(data.open);
    }
    if(data.share){
        queryStr += ' and is_share=?';
        queryData.push(data.share);
    }
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);

    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        data:[],
        draw:data.draw
    };
    db.get().query(queryCountsStr , queryCountsData, function(countsError, countsResults, countsFields){
        returnData.recordsTotal = countsResults[0]['recordsTotal'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            if(error) return done(error);
            returnData.recordsFiltered = returnData.recordsTotal;
            returnData.data = results;
            done(null,returnData);
        });
    });
};

//教師在適性設定中取得活動
module.exports.teacherGetAllActivityAdaptive = function(data,done){
    var queryCountsStr = 'select count(*) as counts from activity where creator_id=? and is_delete=false';
    var queryStr = 'select '+
                      'id,name,'+
                      'DATE_FORMAT(activity.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format '+
                      'from activity '+
                      'where creator_id=? and is_delete=false';
    var queryCountsData = [data.creator_id];
    var queryData = [data.creator_id];
    if(data.search){
        queryCountsStr += ' and name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and name like ?';
        queryData.push('%'+data.search+'%');
    }
    if(data.exclude_type){
        queryCountsStr += ' and type <> ?';
        queryCountsData.push(data.exclude_type);
        queryStr += ' and type <> ?';
        queryData.push(data.exclude_type);
    }
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        data:[],
        draw:data.draw
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, activities, fields){
            returnData.recordsFiltered = activities.length;
            returnData.data = activities;

            async.each(activities, function(activity,callback){
                db.get().query(
                    'select id,name,type from material where activity_id=? and is_delete=false', 
                    [activity.id], function(mError, mResults, mFields){
                        activity.materials = mResults;
                        activity.category = 'activity';
                        callback();
                });
            }, function(err){
                done(null, returnData);
            });

        });
    });
};

//教師在適性設定中取得活動-測驗教材
module.exports.teacherGetAllActivityAdaptiveAssessment = function(data,done){
    var queryCountsStr = 'select count(*) as counts from activity where creator_id=? and is_delete=false';
    var queryStr = 'select '+
                      'id,name,'+
                      'DATE_FORMAT(activity.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format '+
                      'from activity '+
                      'where creator_id=? and is_delete=false';
    var queryCountsData = [data.creator_id];
    var queryData = [data.creator_id];
    if(data.search){
        queryCountsStr += ' and name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and name like ?';
        queryData.push('%'+data.search+'%');
    }
    if(data.exclude_type){
        queryCountsStr += ' and type <> ?';
        queryCountsData.push(data.exclude_type);
        queryStr += ' and type <> ?';
        queryData.push(data.exclude_type);
    }
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        data:[],
        draw:data.draw
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, activities, fields){
            returnData.recordsFiltered = activities.length;
            returnData.data = activities;

            async.each(activities, function(activity,callback){
                db.get().query(
                    'select id,name,type from material where activity_id=? and is_delete=false and type=4', 
                    [activity.id], function(mError, mResults, mFields){
                    activity.materials = mResults;
                    activity.category = 'activity';
                    callback();
                });
            }, function(err){
                done(null, returnData);
            });

        });
    });
};

//教師取得已指派的活動
module.exports.teacherGetAssignActivity = function(data,done){
    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        data:[],
        draw:Number(data.draw)
    };
    var queryCountsStr = 'select count(*) as counts from activity_assign '+
                           'left join activity on activity_assign.activity_id=activity.id and activity_assign.creator_id=activity.creator_id '+
                           'where activity_assign.creator_id=? and activity.is_delete=false';
    var queryStr = 'select '+
                   'activity_assign.activity_id,'+
                   'activity.name,'+
                   'activity.type,'+
                   'DATE_FORMAT(activity_assign.start_date, "%Y/%m/%d")as start_date_format,'+
                   'DATE_FORMAT(activity_assign.end_date, "%Y/%m/%d") as end_date_format,'+
                   'DATE_FORMAT(activity_assign.assign_date, "%Y/%m/%d") as assign_date_format,'+
                   'activity_assign.group_id,'+
                   'teach_group.name as group_name '+
                   'from activity_assign '+
                   'left join activity on activity_assign.activity_id=activity.id and activity_assign.creator_id=activity.creator_id '+
                   'left join teach_group on activity_assign.group_id=teach_group.id '+
                   'where activity_assign.creator_id=? and activity.is_delete=false';
    
    var queryCountsData = [data.creator_id];
    var queryData = [data.creator_id];
    if(data.search){
        queryCountsStr += ' and activity.name like ?';
        queryStr += ' and activity.name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryData.push('%'+data.search+'%');
    }
    var querySort = 'activity_assign.'+data.sort;
    if(data.sort==='name' || data.sort==='create_date'){
        querySort = ('activity.'+data.sort);
    }else if(data.sort==='group_name'){
        querySort = 'teach_group.name';
    }else{
        querySort = 'activity_assign.'+data.sort;
    }
    queryStr += (' order by '+querySort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            if(error) done(error);
            returnData.recordsFiltered = returnData.recordsTotal;
            returnData.data = results;
            done(null,returnData);
        });
    });
};

//教師指派活動給群組
module.exports.teacherAssignActivity = function(data,done){
    db.get().query('insert into activity_assign (activity_id,creator_id,group_id,user_cooc_id,start_date,end_date) values ?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};
//教師更新活動指派群組狀態
module.exports.teacherUpdateActivityAssign = function(data,done){
    db.get().query('update activity set is_assign=true,assign_group=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//取得單一活動細節 By UserID
module.exports.getActivityDetailsByUserId = function(data,done){
    db.get().query('select * from activity where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//取得單一活動細節
module.exports.getActivityDetails = function(data,done){
    var queryStr = 'select '
                        +'activity.id as activity_id,'
                        +'activity.name as activity_name,'
                        +'activity.type as activity_type,'
                        +'activity.description as activity_description,'
                        +'activity.subject_name as activity_subject_name,'
                        +'activity.semester as activity_semester,'
                        +'activity.grade as activity_grade,'
                        +'activity.school as activity_school,'
                        +'activity.img_data as img_data,'
                        +'activity.hierarchy as activity_hierarchy,'
                        +'account.name as creator_name,'
                        +'account.cooc_id as creator_cooc_id,'
                        +'account.id as creator_id,'
                        +'account.cooc_id as creator_cooc_id from activity '
                    +'left join account on activity.creator_id=account.id '
                    +'where activity.id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results[0]);
    });
};

//學生取得已收藏的活動
module.exports.studentGetCollectActivity = function(data,done){
    var returnData = [];
    var queryCollectionStr = 'select * from collection where creator_cooc_id=? and is_delete=false order by create_date asc'
    db.get().query(queryCollectionStr, data, function(error1, results1, fields1){
        if(results1.length!=0){
            var queryCollectionActivityStr = 'select '
                                                +'activity.img_data as activity_img,'
                                                +'activity.is_public as activity_public,'
                                                +'activity.name as activity_name,'
                                                +'account.name as creator_name,'
                                                +'collection_activity.activity_id,'
                                                +'collection_activity.creator_id,'
                                                +'IFNULL(student_activity_join.completion_rate,0) as completion_rate '
                                                +'from collection_activity '
                                                +'left join account on collection_activity.creator_id=account.id '
                                                +'left join activity on collection_activity.activity_id=activity.id '
                                                +'left join student_activity_join on collection_activity.activity_id=student_activity_join.activity_id and student_activity_join.user_cooc_id=? '
                                                +'where collection_activity.collection_id=? and collection_activity.user_cooc_id=? and activity.is_delete=false';
            returnData = results1;
            async.each(returnData, function(collection,callback){
                db.get().query(queryCollectionActivityStr, [data[0],collection.id,data[0]], function(e2, r2, f2){
                    collection.activities = r2;
                    callback();
                });
            }, function(err){
                done(null, returnData);
            });
        }else{
            done(null, returnData);
        }
    });
};

//教師更改活動公開狀態
module.exports.teacherUpdateActivityPublic = function(data,done){
    db.get().query('update activity set is_public=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師加入活動共編成員
module.exports.teacherAddActivityShareMember = function(data,done){
    db.get().query('insert into activity_share (activity_id,creator_id,member_id,member_mail,member_name,member_share) values (?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師取得活動共編成員
module.exports.teacherGetActivityShareMember = function(data,done){
    db.get().query('select * from activity_share where activity_id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//教師刪除活動共編成員
module.exports.teacherRemoveActivityShareMember = function(data,done){
    db.get().query('delete from activity_share where activity_id=? and creator_id=? and member_mail=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師更改活動共編狀態
module.exports.teacherUpdateActivityShare = function(data,done){
    db.get().query('update activity set is_share=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師刪除活動
module.exports.teacherRemoveActivity = function(data,done){
    db.get().query('update activity set is_delete=true,delete_date=now() where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師更新活動內容
module.exports.teacherUpdateActivityMetadata = function(data,done){
    db.get().query('update activity set name=?,description=?,subject_id=?,subject_name=?,img_data=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師更新活動指派日期
module.exports.teacherUpdateActivityDateAssign = function(data,done){
    db.get().query('update activity_assign set start_date=?,end_date=? where activity_id=? and group_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//學生取得活動細節
module.exports.studentGetJoinedActivityDetails = function(data,done){
    var queryStr = 'select '
                    +'activity.*,'
                    +'account.name as creator_name,'
                    +'student_activity_join.creator_id,'
                    +'student_activity_join.creator_cooc_id as creator_cooc_id,'
                    +'IFNULL(student_activity_join.completion_rate,0) as completion_rate,'
                    +'student_activity_join.join_date,'
                    +'student_activity_join.last_access_date,'
                    +'student_activity_join.access_learn_date '
                +'from student_activity_join '
                +'left join activity on student_activity_join.activity_id=activity.id '
                +'left join account on student_activity_join.creator_id=account.id '
                +'where student_activity_join.activity_id=? and student_activity_join.user_cooc_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results[0]);
    });
};

//教師取得活動細節(預覽)
module.exports.teacherGetActivityDetails = function(data,done){
    var queryStr = 'select '
                        +'activity.*,'
                        +'account.name as creator_name,'
                        +'account.cooc_id as creator_cooc_id,'
                        +'account.id as creator_id,'
                        +'account.cooc_id as creator_cooc_id from activity '
                    +'left join account on activity.creator_id=account.id '
                    +'where activity.id=? and activity.creator_id=? ';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results[0]);
    });
};

module.exports.teacherGetCopyActivity = function(data,done){
    db.get().query('select * from activity where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0])
    });
};
module.exports.teacherGetCopyActivityMaterial = function(data,done){
    db.get().query('select * from material where activity_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師複製活動
module.exports.teacherCopyActivity = function(data,done){
    var queryStr = 'insert into activity ('
                     + 'name,type,description,goal,creator_id,subject_id,subject_name,grade,school,semester,is_share,is_public,is_assign,access_code,img_url,img_data,creator_cooc_id,hierarchy'
                   +') values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};
module.exports.teacherCopyActivityMaterial = function(data,done){
    var queryStr = 'insert into material ('
                      + 'activity_id,name,upload_name,goal,bloom,description,type,time_limit,url,'
                      + 'total_score,weight,activity_order,is_delete,creator_id,create_date,update_date,embedded,pass_method,pass_val'
                  +') values ?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師更新適性活動設定
module.exports.teacherUpdateAdativeActivityHierarchy = function(data,done){
    db.get().query('update activity set hierarchy=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師取得活動指派狀態
module.exports.teacherGetActivityAssignGroups = function(data,done){
    var queryStr = 'select * from activity_assign where creator_id=? and activity_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師刪除活動指派
module.exports.teacherRemoveActivityAssign = function(data,done){
    db.get().query('delete from activity_assign where creator_id=? and activity_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師指派活動
module.exports.teacherInsertActivityAssign = function(data,done){
    var queryStr = 'insert into activity_assign ('
                        + 'activity_id,creator_id,assign_date,group_id,start_date,end_date'
                    +') values ?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師更新活動指派群組數量
module.exports.teacherUpdateActivityAssignGroupCounts = function(data,done){
    db.get().query('update activity set assign_group=? where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
}

//學生取得所屬群組ID
module.exports.studentGetBelongGroupIds = function(data, done){
    var queryStr = 'select group_member.group_id from group_member '+
                   'left join teach_group on group_member.group_id=teach_group.id '+
                   'where group_member.user_cooc_id=? and teach_group.is_delete=false '+
                   'group by group_member.group_id';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//學生取得探索活動(所有公開活動)
module.exports.studentGetDiscoverActivities = function(data, done){
    var queryCountsStr = 'select count(*) as counts '+
                            'from activity '+
                            'left join account on activity.creator_id=account.id '+
                            'where is_delete=false and is_public<>0';
    var queryStr = 'select '+
                        'activity.*,'+
                        'DATE_FORMAT(activity.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format,'+
                        'account.name as creator_name,'+
                        'account.id as creator_id,'+
                        'account.cooc_id as creator_cooc_id '+
                   'from activity '+
                   'left join account on activity.creator_id=account.id '+
                   'where activity.is_delete=false and is_public<>0';
    var queryCountsData = [];
    var queryData = [];
    if(data.type!=null && data.type!=-1){ //搜尋活動類型
        queryCountsStr += ' and activity.type=?';
        queryStr += ' and activity.type=?';
        queryCountsData.push(data.type);
        queryData.push(data.type);
    }
    if(data.subject_id && data.subject_id!=-1){ //搜尋活動科目
        queryCountsStr += ' and activity.subject_id=?';
        queryStr += ' and activity.subject_id=?';
        queryCountsData.push(data.subject_id);
        queryData.push(data.subject_id);
    }
    if(data.teacher_id){ //搜尋活動所屬教師
        queryCountsStr += ' and account.cooc_id=?';
        queryStr += ' and account.cooc_id=?';
        queryCountsData.push(data.teacher_id);
        queryData.push(data.teacher_id);
    }
    if(data.search){ //搜尋活動名稱
        queryCountsStr += ' and activity.name like ?';
        queryStr += ' and activity.name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryData.push('%'+data.search+'%');
    }
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        recordsTotal:0,
        data:[]
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, activities, fields){
            returnData.data = activities;
            done(null, returnData);
        });
    });
};

//學生取得被指派的活動
module.exports.studentGetAssignActivities = function(data, done){
    var returnData = {
        recordsTotal:0,
        data:[]
    };
    if(data.activityIds.length==0){
        done(null, returnData);
        return;
    }
    var queryCountsStr = 'select count(*) as counts from (select count(*) as counts '+
                    'from activity_assign '+
                    'left join activity on activity_assign.activity_id=activity.id '+
                    'left join account on activity_assign.creator_id=account.id '+
                    'left join student_activity_join on activity_assign.activity_id = student_activity_join.activity_id and (student_activity_join.user_cooc_id=?) '+
                    'where activity_assign.group_id in (?) and activity.is_delete=false';
    var queryStr = 'select '+
                        'activity.*,'+
                        'account.name as creator_name,'+
                        'activity_assign.creator_id,'+
                        'account.cooc_id as creator_cooc_id,'+
                        'DATE_FORMAT(activity_assign.assign_date, "%Y/%m/%d %H:%i:%s") as assign_date_format,'+
                        'IFNULL(student_activity_join.completion_rate,0) as completion_rate,'+
                        'DATE_FORMAT(student_activity_join.join_date, "%Y/%m/%d %H:%i:%s") as join_date_format,'+
                        'DATE_FORMAT(student_activity_join.last_access_date, "%Y/%m/%d %H:%i:%s") as last_access_date_format,'+
                        'DATE_FORMAT(student_activity_join.access_learn_date, "%Y/%m/%d %H:%i:%s") as access_learn_date_format,'+
                        'group_concat(collection_activity.collection_id) as collection '+
                    'from activity_assign '+
                    'left join activity on activity_assign.activity_id=activity.id '+
                    'left join account on activity_assign.creator_id=account.id '+
                    'left join student_activity_join on activity_assign.activity_id = student_activity_join.activity_id and (student_activity_join.user_cooc_id=?) '+
                    'left join collection_activity on activity_assign.activity_id = collection_activity.activity_id and (collection_activity.user_cooc_id=?) '+
                    'where activity_assign.group_id in (?) and activity.is_delete=false ';
    var queryCountsData = [data.user_cooc_id,data.activityIds];
    var queryData = [data.user_cooc_id,data.user_cooc_id,data.activityIds];
    if(data.type!=null && data.type!=-1){ //搜尋活動類型
        queryCountsStr += ' and activity.type=?';
        queryStr += ' and activity.type=?';
        queryCountsData.push(data.type);
        queryData.push(data.type);
    }
    if(data.subject_id && data.subject_id!=-1){ //搜尋活動科目
        queryCountsStr += ' and activity.subject_id=?';
        queryStr += ' and activity.subject_id=?';
        queryCountsData.push(data.subject_id);
        queryData.push(data.subject_id);
    }
    if(data.teacher_id && data.teacher_id!=-1){ //搜尋活動所屬教師
        queryCountsStr += ' and account.cooc_id=?';
        queryStr += ' and account.cooc_id=?';
        queryCountsData.push(data.teacher_id);
        queryData.push(data.teacher_id);
    }
    if(data.search){ //搜尋活動名稱
        queryCountsStr += ' and activity.name like ?';
        queryStr += ' and activity.name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryData.push('%'+data.search+'%');
    }
    if(data.progress && data.progress!=-1){ //搜尋進度
        if(data.progress==0){
            queryCountsStr += ' and completion_rate<>100';
            queryStr += ' and completion_rate<>100';
        }else{
            queryCountsStr += ' and completion_rate=100';
            queryStr += ' and completion_rate=100';
        }
    }
    queryCountsStr += (' group by activity_assign.activity_id) as x');
    queryStr += (' group by activity_assign.activity_id');
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, activities, fields){
            returnData.data = activities;
            done(null, returnData);
        });
    });
};

//學生取得活動細節
module.exports.studentGetActivityDetails = function(data, done){
    var queryStr = 'select '+
                    'activity.*,'+
                    'account.name as creator_name,'+
                    'account.id as creator_id,'+
                    'account.cooc_id as creator_cooc_id '+
                    'from activity '+
                    'left join account on activity.creator_id=account.id '+
                    'where activity.id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]);
    });
};

//學生取得參加的活動
module.exports.studentGetJoinActivities = function(data, done){
    var queryCountsStr = 'select count(*) as counts from (select student_activity_join.activity_id '+
                   'from student_activity_join '+
                   'left join activity on student_activity_join.activity_id=activity.id '+
                   'left join account on student_activity_join.creator_id=account.id '+
                   'where student_activity_join.user_cooc_id=? and activity.is_delete=false and activity.is_public<>0';
    var queryStr = 'select '+
                        'activity.*,'+
                        'DATE_FORMAT(activity.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format,'+
                        'student_activity_join.creator_id,'+
                        'account.name as creator_name,'+
                        'account.cooc_id as creator_cooc_id,'+
                        'IFNULL(student_activity_join.completion_rate,0) as completion_rate,'+
                        'group_concat(collection_activity.collection_id) as collection '+
                   'from student_activity_join '+
                   'left join activity on student_activity_join.activity_id=activity.id '+
                   'left join account on student_activity_join.creator_id=account.id '+
                   'left join collection_activity on collection_activity.activity_id=student_activity_join.activity_id and collection_activity.user_cooc_id=?'+
                   'where student_activity_join.user_cooc_id=? and activity.is_delete=false and activity.is_public<>0';
    var queryCountsData = [data.user_cooc_id];
    var queryData = [data.user_cooc_id,data.user_cooc_id];
    if(data.type!=null && data.type!=-1){ //搜尋活動類型
        queryCountsStr += ' and activity.type=?';
        queryStr += ' and activity.type=?';
        queryCountsData.push(data.type);
        queryData.push(data.type);
    }
    if(data.subject_id && data.subject_id!=-1){ //搜尋活動科目
        queryCountsStr += ' and activity.subject_id=?';
        queryStr += ' and activity.subject_id=?';
        queryCountsData.push(data.subject_id);
        queryData.push(data.subject_id);
    }
    if(data.teacher_id){ //搜尋活動所屬教師
        queryCountsStr += ' and account.cooc_id=?';
        queryStr += ' and account.cooc_id=?';
        queryCountsData.push(data.teacher_id);
        queryData.push(data.teacher_id);
    }
    if(data.search){ //搜尋活動名稱
        queryCountsStr += ' and activity.name like ?';
        queryStr += ' and activity.name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryData.push('%'+data.search+'%');
    }
    if(data.progress && data.progress!=-1){ //搜尋進度
        if(data.progress==0){
            queryCountsStr += ' and completion_rate<>100';
            queryStr += ' and completion_rate<>100';
        }else{
            queryCountsStr += ' and completion_rate=100';
            queryStr += ' and completion_rate=100';
        }
    }
    queryCountsStr += (') as x');
    queryStr += (' group by student_activity_join.activity_id');
    queryStr += (' order by '+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        recordsTotal:0,
        data:[]
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, activities, fields){
            returnData.data = activities;
            done(null, returnData);
        });
    });
};

//學生開始學習
module.exports.studentInsertActivityJoin = function(data, done){
    //如果 student_activity_join 沒有資料才 insert
    var insertStr = 'insert into student_activity_join '+
    '(activity_id,creator_id,creator_cooc_id,user_id,user_cooc_id,join_date) '+
    'select * from (select ?, ?, ?, ?, ?, ?) AS tmp '+
    'where not exists ('+
    'select activity_id from student_activity_join where activity_id = ? and user_cooc_id=?'+
    ') limit 1';
    db.get().query(insertStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//取得活動建立者ID
module.exports.getActivityCreator = function(data, done){
    db.get().query('select account.id,account.cooc_id from activity left join account on activity.creator_id=account.id where activity.id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, {
            creator_id:results[0]['id'],
            creator_cooc_id:results[0]['cooc_id']
        });
    });
};

//檢查學生是否 Join 活動
module.exports.checkStudentActivityJoined = function(data, done){
    var queryStr = 'select count(*) as counts from student_activity_join '+
    'where activity_id=? and user_cooc_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//檢查學生是否被指派該活動
module.exports.checkStudentActivityAssigned = function(data, done){
    if(data[2].length==0){
        done(null, 0);
        return;
    }
    var queryStr = 'select count(*) as counts from (select count(*) as counts from activity_assign '+
    'left join group_member on activity_assign.group_id=group_member.group_id '+
    'where activity_assign.activity_id=? and activity_assign.creator_id=? and activity_assign.group_id in (?) and group_member.user_cooc_id=? '+
    'group by activity_assign.group_id) as x';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//學生解鎖活動
module.exports.studentUnlockActivity = function(data, done){
    db.get().query('select count(*) as counts from activity where id=? and access_code=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//學生繳交測驗
module.exports.studentSubmitAssessment = function(data, done){
    db.get().query('insert into student_assessment_result(activity_id,material_id,score,time_spent,submit_date,user_id,user_cooc_id) values(?,?,?,?,now(),?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//學生繳交測驗(適性)
module.exports.studentSubmitAdaptiveAssessment = function(data, done){
    db.get().query('insert into student_assessment_result(activity_id,material_id,score,time_spent,submit_date,user_id,user_cooc_id,material_uuid) values(?,?,?,?,now(),?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//學生取得測驗作答紀錄
module.exports.studentGetAssessmentRecords = function(data, done){
    var queryStr = 'select *,'+
                           'DATE_FORMAT(submit_date, "%Y/%m/%d %H:%i:%s") as submit_date_format,'+
                           'SEC_TO_TIME(time_spent) as time_spent_format '+
                   'from student_assessment_result where activity_id=? and material_id=? and user_cooc_id=? and material_uuid IS NULL order by submit_date asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//學生取得測驗作答紀錄(適性)
module.exports.studentGetAdaptiveAssessmentRecords = function(data, done){
    var queryStr = 'select *,'+
                           'DATE_FORMAT(submit_date, "%Y/%m/%d %H:%i:%s") as submit_date_format,'+
                           'SEC_TO_TIME(time_spent) as time_spent_format '+
                   'from student_assessment_result where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=? order by submit_date asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得活動完成資訊
module.exports.getActivityCompletionStatus = function(data, done){
    var queryStr = 
        'select '+
            'count(*) as user_counts,'+
            'count(completion_rate>=100) as completion_counts,'+
            'SEC_TO_TIME(avg(time_spent)) as avg_time_spent,'+
            'concat(round(( count(completion_rate>=100)/count(*) * 100 ),0),\'%\') as completion_rate '+
        'from student_activity_join '+
        'where student_activity_join.activity_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};

//教師取得教材所有學生學習紀錄
module.exports.getSingleMaterialStudentLearnStatus = function(data, done){
    queryStr = 'select '+
                    'm.id,'+
                    'm.name,'+
                    'sml.user_cooc_id,'+
                    'sml.is_complete,'+
                    'sml.completion_rate,'+
                    'sml.last_access_date,'+
                    'sml.completion_date,'+
                    'SEC_TO_TIME(IFNULL(sml.time_spent,0)) as time_spent '+
               'from material m '+
               'left join student_material_learn sml on m.id=sml.material_id '+
               'where m.id=? order by m.activity_order asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};

//教師取得教材所有學生學習紀錄(適性活動內的活動)
module.exports.getSingleMaterialStudentLearnStatusFromAdaptiveActivity = function(data, done){
    queryStr = 'select '+
                    'm.id,'+
                    'm.name,'+
                    'sml.user_cooc_id,'+
                    'sml.is_complete,'+
                    'sml.completion_rate,'+
                    'sml.last_access_date,'+
                    'sml.completion_date,'+
                    'SEC_TO_TIME(IFNULL(sml.time_spent,0)) as time_spent '+
               'from material m '+
               'left join student_adaptive_material_learn sml on m.id=sml.material_id '+
               'where m.id=? and sml.material_uuid=? and sml.activity_id=? order by m.activity_order asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};

//教師取得教材所有學生學習紀錄(適性活動內的活動) BY GROUP
module.exports.getSingleMaterialStudentLearnStatusFromAdaptiveActivityByGroup = function(data, done){
    queryStr = 'select '+
                    'm.id,'+
                    'm.name,'+
                    'sml.user_cooc_id,'+
                    'sml.is_complete,'+
                    'sml.completion_rate,'+
                    'sml.last_access_date,'+
                    'sml.completion_date,'+
                    'SEC_TO_TIME(IFNULL(sml.time_spent,0)) as time_spent '+
               'from material m '+
               'left join student_adaptive_material_learn sml on m.id=sml.material_id '+
               'where m.id='+data.material_id+' and sml.material_uuid=\''+data.material_uuid+'\''+
               ' and sml.activity_id='+data.activity_id+' and sml.user_cooc_id ';
    if(data.assign_groups.length!=0){
        var inStr = '('
        for(var i=0;i<data.assign_groups.length;i++){
            if(i!=(data.assign_groups.length-1)){
                inStr += ('\''+data.assign_groups[i]+'\',');
            }else{
                inStr += ('\''+data.assign_groups[i]+'\'');
            }
        }
        inStr += ') ';
        queryStr += 'in '+inStr;
    }
    queryStr += ' order by m.activity_order asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};

//教師取得活動細節，包含教材id
module.exports.getActivityDetailsAndMaterials = function(data, done){
    var queryStr = 'select *,'+
                        'GROUP_CONCAT(material.id) as materials from activity '+
                        'left join material on activity.id=material.activity_id '+
                        'where activity.id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]);
    });
};

//取得活動與教材細節
module.exports.getActivityDetialWithMaterials = function(data, done){
    var queryStr = 'select '+
                        'a.id as activity_id,'+
                        'a.name as activity_name,'+
                        'a.type as activity_type,'+
                        'a.description as activity_description,'+
                        'a.goal as activity_goal,'+
                        'a.img_data as activity_img,'+
                        'm.* '+
                   'from activity a '+
                   'left join material m on a.id=m.activity_id '+
                   'where a.id=? '+
                   'order by m.activity_order asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//學生更新活動完成度
module.exports.studentUpdateActivityCompletion = function(data, done){
    db.get().query('update student_activity_join set completion_rate=?,time_spent=time_spent+? where activity_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, data);
    });
};

//取得活動學生
module.exports.getActivityJoinedStudents = function(data, done){
    var queryStr = 'select student_activity_join.user_cooc_id as cooc_id,account.name as name from student_activity_join left join account on student_activity_join.user_cooc_id=account.cooc_id where student_activity_join.activity_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師刪除適性活動結構
module.exports.teacherRemoveAdaptiveActivityHierarchy = function(data, done){
    var queryStr = 'delete from adaptive_activity_hierarchy where activity_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師儲存適性活動結構
module.exports.teacherSaveAdaptiveActivityHierarchy = function(data, done){
    var queryStr = 'insert into adaptive_activity_hierarchy (activity_id,creator_uuid,node_uuid,parent_uuid,node_name,node_category,node_content_id,node_index,node_depth) values ?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//檢查學生適性活動狀態
module.exports.checkStudentAdaptiveActivityProgress = function(data, done){
    var queryStr = 'select count(*) as progress_counts from student_adaptive_node where user_cooc_id=? and activity_id=? and node_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['progress_counts']);
    });
};

//記錄學生進行節點
module.exports.recordStudentAdaptiveActivityProgress = function(data, done){
    var queryStr = 'insert into student_adaptive_node (user_cooc_id,activity_id,node_id,enter_date) values (?,?,?,now())';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//取得適性活動學生所在節點數量
module.exports.getAdaptiveActivityStudentNodeCounts = function(data, done){
    var queryStr = 'select '+
                     'san.node_id,'+
                     'count(san.user_cooc_id) as node_counts '+
                   'from student_adaptive_node san '+
                   'left join account a on san.user_cooc_id=a.cooc_id '+
                   'where (user_cooc_id, enter_date) '+
                   'in (select user_cooc_id, max(enter_date) from student_adaptive_node group by user_cooc_id) '+
                   'and san.activity_id=? '+
                   'group by san.node_id';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//取得適性活動學生所在節點數量 BY Group
module.exports.getAdaptiveActivityStudentNodeCountsByGroup = function(data, done){
    var queryStr = 'select '+
                     'san.node_id,'+
                     'count(san.user_cooc_id) as node_counts '+
                   'from student_adaptive_node san '+
                   'left join account a on san.user_cooc_id=a.cooc_id '+
                   'where (user_cooc_id, enter_date) '+
                   'in (select user_cooc_id, max(enter_date) from student_adaptive_node group by user_cooc_id) '+
                   'and san.activity_id='+data.activity_id+' and san.user_cooc_id ';
    if(data.assign_groups.length!=0){
        var notInStr = '('
        for(var i=0;i<data.assign_groups.length;i++){
            if(i!=(data.assign_groups.length-1)){
                notInStr += ('\''+data.assign_groups[i]+'\',');
            }else{
                notInStr += ('\''+data.assign_groups[i]+'\'');
            }
        }
        notInStr += ') ';
        queryStr += 'in '+notInStr;
    }
    queryStr += 'group by san.node_id';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//取得適性活動學生所在節點狀態
module.exports.getAdaptiveActivityStudentNodeStatus = function(data, done){
    var queryStr = 'select '+
                     'san.node_id,'+
                     'san.user_cooc_id,'+
                     'a.name,'+
                     'aah.node_name,'+
                     'DATE_FORMAT(san.enter_date, "%Y/%m/%d %H:%i:%s") as enter_date_format '+
                   'from student_adaptive_node san '+
                   'left join account a on san.user_cooc_id=a.cooc_id '+
                   'left join adaptive_activity_hierarchy aah on san.node_id=aah.node_uuid '+
                   'where (user_cooc_id, enter_date) '+
                   'in (select user_cooc_id, max(enter_date) from student_adaptive_node group by user_cooc_id) '+
                   'and san.activity_id=? '+
                   'group by san.node_id';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//取得適性活動學生所在節點狀態 BY Group
module.exports.getAdaptiveActivityStudentNodeStatusByGroup = function(data, done){
    var queryStr = 'select '+
                     'san.node_id,'+
                     'san.user_cooc_id,'+
                     'a.name,'+
                     'aah.node_name,'+
                     'DATE_FORMAT(san.enter_date, "%Y/%m/%d %H:%i:%s") as enter_date_format '+
                   'from student_adaptive_node san '+
                   'left join account a on san.user_cooc_id=a.cooc_id '+
                   'left join adaptive_activity_hierarchy aah on san.node_id=aah.node_uuid '+
                   'where (user_cooc_id, enter_date) '+
                   'in (select user_cooc_id, max(enter_date) from student_adaptive_node group by user_cooc_id) '+
                   'and san.activity_id='+data.activity_id+' and san.user_cooc_id ';
    if(data.assign_groups.length!=0){
        var notInStr = '('
        for(var i=0;i<data.assign_groups.length;i++){
            if(i!=(data.assign_groups.length-1)){
                notInStr += ('\''+data.assign_groups[i]+'\',');
            }else{
                notInStr += ('\''+data.assign_groups[i]+'\'');
            }
        }
        notInStr += ') ';
        queryStr += 'in '+notInStr;
    }
    queryStr += 'group by san.node_id';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//取得適性活動節點資訊
module.exports.getAdaptiveActivityHierarchy = function(data, done){
    var queryStr = 'select * from adaptive_activity_hierarchy where activity_id=? order by node_index asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        var nodes = [];
        for(var r=0;r<results.length;r++){
            nodes.push({
                "name":results[r].node_name
            });
        }
        done(null, nodes);
    });
};

//取得適性活動每位學生的路徑
module.exports.getAdaptiveActivityStudentPath = function(data, done){
    var queryStr = 'select aah.node_index from student_adaptive_node san '+
                   'left join adaptive_activity_hierarchy aah on san.node_id=aah.node_uuid '+
                   'where san.activity_id=? and san.user_cooc_id=? '
                   'order by san.enter_date asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得教材所有學生學習紀錄(適性活動)
module.exports.getAdaptiveSingleMaterialStudentLearnStatus = function(data, done){
    var queryStr = 'select '+
                    'account.name,'+
                    'saml.last_access_date,'+
                    'saml.is_complete,'+
                    'saml.completion_date,'+
                    'saml.completion_rate,'+
                    'SEC_TO_TIME(IFNULL(saml.time_spent,0)) as time_spent '+
               'from student_adaptive_material_learn saml '+
               'left join account on account.cooc_id=saml.user_cooc_id '+
               'left join material m on saml.material_id=m.id '+
               'where saml.activity_id=? and saml.material_id=? and saml.material_uuid=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};

//教師取得教材所有學生學習紀錄(適性活動) BY GROUP
module.exports.getAdaptiveSingleMaterialStudentLearnStatusByGroup = function(data, done){
    var queryStr = 'select '+
                    'account.name,'+
                    'saml.last_access_date,'+
                    'saml.is_complete,'+
                    'saml.completion_date,'+
                    'saml.completion_rate,'+
                    'SEC_TO_TIME(IFNULL(saml.time_spent,0)) as time_spent '+
               'from student_adaptive_material_learn saml '+
               'left join account on account.cooc_id=saml.user_cooc_id '+
               'left join material m on saml.material_id=m.id '+
               'where saml.activity_id='+data.activity_id+
               ' and saml.material_id='+data.material_id+
               ' and saml.material_uuid=\''+data.material_uuid+'\''+
               ' and saml.user_cooc_id ';
    if(data.assign_groups.length!=0){
        var inStr = '('
        for(var i=0;i<data.assign_groups.length;i++){
            if(i!=(data.assign_groups.length-1)){
                inStr += ('\''+data.assign_groups[i]+'\',');
            }else{
                inStr += ('\''+data.assign_groups[i]+'\'');
            }
        }
        inStr += ') ';
        queryStr += 'in '+inStr;
    }
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(null);
        done(null, results);
    });
};