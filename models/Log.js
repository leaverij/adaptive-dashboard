var db = require('../mysql');

//學生點擊教材
module.exports.accessMaterial = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,type,action) values (?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生閱讀 PDF 每一個頁面
module.exports.studentReadSinglePDFPage = function(data, done){
    var queryStr = 'INSERT INTO student_pdf_result (activity_id,material_id,page,time_spent,start_date,last_access_date,user_cooc_id,access_counts) '
    +'VALUES(?,?,?,?,?,?,?,1) ON DUPLICATE KEY UPDATE last_access_date=VALUES(last_access_date),time_spent=time_spent+?,access_counts=access_counts+1'; 
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生閱讀 PDF 每一個頁面(適性)
module.exports.studentReadAdaptiveSinglePDFPage = function(data, done){
    var queryStr = 'INSERT INTO student_adaptive_pdf_result (activity_id,material_id,material_uuid,page,time_spent,start_date,last_access_date,user_cooc_id,access_counts) '
    +'VALUES(?,?,?,?,?,?,?,?,1) ON DUPLICATE KEY UPDATE last_access_date=VALUES(last_access_date),time_spent=time_spent+?,access_counts=access_counts+1'; 
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生閱讀PDF頁面
module.exports.readPDFPage = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,time_spent,type,action,page) values (?,?,?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生觀看影片片段
module.exports.watchVideoClip = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,duration,type,action,start_point,end_point) values (?,?,?,?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生瀏覽網頁
module.exports.viewWeb = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,time_spent,type,action) values (?,?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//更新學生瀏覽網頁
module.exports.studentUpdateWebRead = function(data, done){
    db.get().query('update student_material_learn set completion_rate=?,time_spent=?,is_complete=? where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//更新學生瀏覽網頁(適性)
module.exports.studentUpdateAdaptiveWebRead = function(data, done){
    db.get().query('update student_adaptive_material_learn set completion_rate=?,time_spent=?,is_complete=? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生完成網頁
module.exports.studentCompleteWeb = function(data, done){
    db.get().query('update student_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=30 where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生完成網頁(適性)
module.exports.studentCompleteAdaptiveWeb = function(data, done){
    db.get().query('update student_adaptive_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=30 where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生完成影片
module.exports.studentCompleteVideo = function(data, done){
    db.get().query('update student_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=? where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生完成影片(適性)
module.exports.studentCompleteAdaptiveVideo = function(data, done){
    db.get().query('update student_adaptive_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生筆記:影片教材
module.exports.noteVideoMaterial = function(data,done){
    db.get().query('insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,point,complete_date) values (?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生筆記Log:影片教材
module.exports.logNoteVideoMaterial = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,page,note) values (?,?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//學生筆記:網頁教材
module.exports.noteWebMaterial = function(data,done){
    db.get().query('insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,complete_date) values (?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生筆記Log:網頁教材
module.exports.logNoteWebMaterial = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,note) values (?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//學生筆記:PDF教材
module.exports.notePDFMaterial = function(data,done){
    db.get().query('insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,complete_date) values (?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//學生筆記Log:PDF教材
module.exports.logNotePDFMaterial = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,note) values (?,?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//取得筆記
module.exports.getMaterialNotes = function(data,done){
    var queryStr = 'select *,'+
                          'DATE_FORMAT(complete_date, "%Y/%m/%d %H:%i:%s") as note_date,'+
                          'SEC_TO_TIME(point) as point_format from student_learn_note '+
                   'where material_id=? and user_cooc_id=? and is_delete=false and material_uuid IS NULL order by complete_date desc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//取得筆記(適性)
module.exports.getAdaptiveMaterialNotes = function(data,done){
    var queryStr = 'select *,'+
                          'DATE_FORMAT(complete_date, "%Y/%m/%d %H:%i:%s") as note_date,'+
                          'SEC_TO_TIME(point) as point_format from student_learn_note '+
                   'where material_id=? and user_cooc_id=? and is_delete=false and material_uuid=? order by complete_date desc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//刪除筆記
module.exports.deleteMaterialNote = function(data,done){
    db.get().query('update student_learn_note set is_delete=true,delete_date=now() where id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//更新筆記
module.exports.updateMaterialNote = function(data,done){
    db.get().query('update student_learn_note set note=?,update_date=now() where id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//進入討論問題
module.exports.accessForumQuestion = function(data,done){
    db.get().query('insert into student_learn_log (activity_id,creator_id,creator_cooc_id,user_cooc_id,start_date,type,action,category) values (?,?,?,?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//更新學生測驗教材狀態
module.exports.studentUpdateAssessmentStatus = function(data, done){
    db.get().query('update student_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=? where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
//更新學生測驗教材狀態(適性)
module.exports.studentUpdateAdaptiveAssessmentStatus = function(data, done){
    db.get().query('update student_adaptive_material_learn set completion_rate=100,is_complete=true,completion_date=now(),time_spent=? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//更新學生影片觀看時間
module.exports.studentUpdateVideoWatched = function(data, done){
    db.get().query('update student_material_learn set completion_rate=?,time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//更新學生影片觀看時間(適性)
module.exports.studentUpdateAdaptiveVideoWatched = function(data, done){
    db.get().query('update student_adaptive_material_learn set completion_rate=?,time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};