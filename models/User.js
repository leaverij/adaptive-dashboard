var db = require('../mysql');
var request = require('request');
var config = require('../config');
var sha1 = function(input){
    return require('crypto').createHash('sha1').update(input).digest('hex')
}
var parseString = require('xml2js').parseString;

//註冊使用者
module.exports.registerUser = function(user,done){
    db.get().query('insert into account set ?', user, function (error, results, fields) {
        if (error) return done(error);
        done(null,results.insertId); 
    });
};

//檢查是否是已註冊的使用者
module.exports.checkUser = function(user,done){
    db.get().query('select * from account where email=? and password=?', user, function (error, results, fields) {
        if (error) return done(error);
        done(null,results); 
    });
};

//檢查Email是否是系統使用者
module.exports.checkUserByEmail = function(user,done){
    db.get().query('select * from account where email=?', user, function (error, results, fields) {
        if (error) return done(error);
        done(null,results);
    });
};

//寫入使用者登入系統紀錄
module.exports.logUserLoginData = function(user,done){
    db.get().query('insert into login_history (user_id,user_mail,user_name,login_ip,login_role) values (?,?,?,?,?)', user, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//檢查酷課雲使用者帳號
module.exports.checkCoocUser = function(user,done){
    db.get().query('select * from account where cooc_id=?', user, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//新增酷課雲使用者
module.exports.insertNewCoocUser = function(user,done){
    db.get().query('insert into account (email,name,cooc_id,teacher_flag,student_flag,parent_flag) values (?,?,?,?,?,?)', user, function(error, result){
        if(error) done(error);
        done(null,result);
    });
};

function computeSign(client_id,client_secret,queryParams){
    var code = client_id;
    for(queryKey in queryParams){
        code += queryKey;
        code += queryParams[queryKey];
    }
    code += client_secret;
    return sha1(code);
}
//取得酷課雲使用者基本資訊
module.exports.getCoocUserInfo = function(data,done){
    var queryParams = {
        userId:data
    };
    var sign = computeSign(config.cooc.client_id,config.cooc.client_secret,queryParams);
    var getUserInfoPath = config.cooc.sso_school+(config.cooc.get_user_path+'?userId='+queryParams.userId+'&appKey='+config.cooc.client_id+'&sign='+sign);
    request(getUserInfoPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            parseString(body, function (parseError, jsonResult) {
                if(parseError) done(parseError);
                done(null,jsonResult);
            });          
        }else{
            done(error,body);
        }
    });
};

//取得酷課雲學年學期基本資訊
module.exports.getSemesterInfo = function(data,done){
    var queryParams = {
        schoolCode:data
    };
    var sign = computeSign(config.cooc.client_id,config.cooc.client_secret,queryParams);
    var getSmsInfoPath = config.cooc.sso_school+(config.cooc.get_sms_path+'?schoolCode='+data+'&appKey='+config.cooc.client_id+'&sign='+sign);
    console.log('getSmsInfoPath:'+getSmsInfoPath);
    request(getSmsInfoPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            parseString(body, function (parseError, jsonResult) {
                if(parseError) done(parseError);
                done(null,jsonResult);
            });          
        }else{
            done(error,body);
        }
    });
};

//酷課雲教師取得學生名單
module.exports.getCoocStudentList = function(data,done){
    var queryParams = {
        termId:data.termId,
        userId:data.userId
    };
    var sign = computeSign(config.cooc.client_id,config.cooc.client_secret,queryParams);
    var getStudentListPath = config.cooc.sso_school+(config.cooc.get_student_list+'?userId='+data.userId+'&termId='+data.termId+'&appKey='+config.cooc.client_id+'&sign='+sign);
    console.log('getStudentListPath:'+getStudentListPath);
    request(getStudentListPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            parseString(body, function (parseError, jsonResult) {
                if(parseError) done(parseError);
                done(null,jsonResult);
            });          
        }else{
            done(err,body);
        }
    });
};

