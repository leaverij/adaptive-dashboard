var db = require('../mysql');

//取得教材
module.exports.getMaterialsByUserId = function(data,done){
    db.get().query('select * from material where activity_id=? and creator_id=? and is_delete=false order by activity_order asc', data, function(error ,results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//學生取得教材
module.exports.studentGetMaterialsByUserId = function(data,done){
    db.get().query('select * from material where activity_id=? and is_delete=false order by activity_order asc', data, function(error ,results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//學生取得單一教材(含activity_id)
module.exports.studentGetSingleMaterial = function(data,done){
    db.get().query('select * from material where activity_id=? and id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,results[0]);
    });
};

//學生取得適性單一教材
module.exports.studentGetTargetAdaptiveMaterial = function(data,done){
    var queryStr = 'select m.*, '+
                          'IFNULL(sml.completion_rate,0) as completion_rate,'+
                          'IFNULL(sml.is_complete,false) as is_complete,'+
                          'DATE_FORMAT(sml.start_date, "%Y/%m/%d %H:%i:%s") as start_date_format,'+
                          'DATE_FORMAT(sml.last_access_date, "%Y/%m/%d %H:%i:%s") as last_access_date_format,'+
                          'sml.is_complete,'+
                          'sml.access_counts,'+
                          'SEC_TO_TIME(IFNULL(sml.time_spent,0)) as time_spent_format, '+
                          'IFNULL(sml.time_spent, 0) as time_spent '+
                   'from material m '+
                   'left join student_adaptive_material_learn sml on m.id=sml.material_id and sml.activity_id=? and sml.user_cooc_id=? '+
                   'where m.id=?';
    db.get().query(queryStr, data, function(error ,results, fields){
        if(error) done(error);
        done(null,results[0]);
    });
};

//新增網頁教材
module.exports.createWebMaterial = function(data,done){
    db.get().query('insert into material (activity_id,name,description,type,url,creator_id,goal,bloom,create_date,embedded,upload_name,weight,required_time_spent_method,required_time_spent_val,activity_order,pass_method,pass_val) values (?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};
//更新網頁教材
module.exports.updateWebMaterial = function(data,done){
    db.get().query('update material set name=?,description=?,url=?,goal=?,update_date=now(),bloom=?,embedded=?,upload_name=?,weight=?,required_time_spent_method=?,required_time_spent_val=?,activity_order=?,pass_method=?,pass_val=? where id=? and activity_id=? and creator_id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//新增影片教材
module.exports.createVideoMaterial = function(data,done){
    db.get().query('insert into material (activity_id,name,description,type,upload_name,url,creator_id,goal,bloom,create_date,pass_method,pass_val,weight,duration,required_time_spent_method,required_time_spent_val,activity_order) values (?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};
//更新影片教材
module.exports.updateVideoMaterial = function(data,done){
    db.get().query('update material set name=?,description=?,upload_name=?,url=?,goal=?,update_date=now(),bloom=?,pass_method=?,pass_val=?,weight=?,duration=?,required_time_spent_method=?,required_time_spent_val=?,activity_order=? where id=? and activity_id=? and creator_id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true); 
    });
};

//新增PDF教材
module.exports.createPDFMaterial = function(data,done){
    db.get().query('insert into material (activity_id,name,description,type,upload_name,url,creator_id,goal,bloom,create_date,pass_method,pass_val,weight,required_time_spent_method,required_time_spent_val,activity_order,pages) values (?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};
//更新PDF教材
module.exports.updatePDFMaterial = function(data,done){
    db.get().query('update material set name=?,description=?,upload_name=?,url=?,goal=?,update_date=now(),bloom=?,pass_method=?,pass_val=?,weight=?,required_time_spent_method=?,required_time_spent_val=?,activity_order=?,pages=? where id=? and activity_id=? and creator_id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//新增測驗教材
module.exports.createAssessmentMaterial = function(data,done){
    db.get().query('insert into material (activity_id,name,description,type,creator_id,goal,bloom,create_date,pass_method,pass_val,weight,activity_order,quizs,total_score) values (?,?,?,?,?,?,?,now(),?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};
//更新測驗教材
module.exports.updateAssessmentMaterial = function(data,done){
    db.get().query('update material set name=?,description=?,goal=?,update_date=now(),bloom=?,pass_method=?,pass_val=?,weight=?,activity_order=?,quizs=?,total_score=? where id=? and activity_id=? and creator_id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//新增文章教材
module.exports.createArticleMaterial = function(data,done){
    db.get().query('insert into material (activity_id,name,description,type,creator_id,goal,bloom,create_date,pass_method,pass_val,weight,activity_order,required_time_spent_method,required_time_spent_val) values (?,?,?,?,?,?,?,now(),?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};
//更新文章教材
module.exports.updateArticleMaterial = function(data,done){
    db.get().query('update material set name=?,description=?,goal=?,update_date=now(),bloom=?,pass_method=?,pass_val=?,weight=?,activity_order=?,pass_method=?,pass_val=?,required_time_spent_method=?,required_time_spent_val=? where id=? and activity_id=? and creator_id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};
//儲存文章
module.exports.saveArticle = function(data,done){
    db.get().query('insert into student_article (activity_id,material_id,user_id,user_cooc_id,article,time_spent) values (?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};
//儲存文章(適性)
module.exports.saveArticle = function(data,done){
    db.get().query('insert into student_article (activity_id,material_id,user_id,user_cooc_id,article,time_spent,material_uuid) values (?,?,?,?,?,?,?)', data, function(error ,results, fields){
        if(error) done(error);
        done(null,true);
    });
};
//學生更新文章學習狀態
module.exports.studentUpdateArticleStatus = function(data, done){
    db.get().query('update student_material_learn set is_complete=true,completion_rate=100,time_spent=? where activity_id=? and material_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
//學生更新文章學習狀態(適性)
module.exports.studentUpdateAdaptiveArticleStatus = function(data, done){
    db.get().query('update student_adaptive_material_learn set is_complete=true,completion_rate=100,time_spent=? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//取得最新文章
module.exports.getLatestArticleByUserId = function(data,done){
    db.get().query('select article from student_article where activity_id=? and material_id=? and user_cooc_id=? order by save_date desc limit 1', data, function(error ,results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師更新教材順序
module.exports.teacherUpdateMaterialOrder = function(data,done){
    db.get().query('update material set activity_order=? where id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師更新教材權重
module.exports.teacherUpdateMaterialWeight = function(data,done){
    db.get().query('update material set weight=? where id=?', data, function(error ,results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//教師取得所有自己的教材
module.exports.teacherGetAllMaterials = function(data,done){
    var queryCountsData = [data.creator_id];
    var queryData = [data.creator_id];
    var queryCountsStr = 'select count(*) as counts from material where creator_id=? and is_delete=false';
    var queryStr = 'select *,DATE_FORMAT(create_date, "%Y/%m/%d %H:%i:%s") as create_date_format from material where creator_id=? and is_delete=false';
    if(data.search){
        queryCountsStr += ' and name like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and name like ?';
        queryData.push('%'+data.search+'%');
    }
    if(data.type){
        queryCountsStr += ' and type=?';
        queryStr += ' and type=?';
        queryCountsData.push(data.type);
        queryData.push(data.type);
    }
    queryStr += ' order by '+data.sort+' '+data.order;
    queryStr += ' limit '+data.start+','+data.end;
    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        data:[],
        draw:data.draw
    }
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResult, countsField){
        returnData.recordsTotal = countsResult[0]['counts'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            returnData.recordsFiltered = results.length;
            returnData.data = results;
            done(null ,returnData);
        });
    });
};

//學生學習教材
module.exports.studentLearnMaterial = function(data, done){
    var queryStr = 'INSERT INTO student_material_learn (activity_id,user_id,user_cooc_id,start_date,material_id,last_access_date,access_counts) '
    +'VALUES(?,?,?,?,?,?,1) ON DUPLICATE KEY UPDATE last_access_date=VALUES(last_access_date),access_counts=access_counts+1'; 
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生學習適性教材
module.exports.studentLearnAdaptiveMaterial = function(data, done){
    var queryStr = 'INSERT INTO student_adaptive_material_learn (activity_id,user_cooc_id,start_date,material_id,material_uuid,last_access_date,access_counts) '
    +'VALUES(?,?,?,?,?,?,1) ON DUPLICATE KEY UPDATE last_access_date=VALUES(last_access_date),access_counts=access_counts+1'; 
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//學生取得含有進度的教材
module.exports.studentGetMaterialDetailWithProgress = function(data, done){
    var queryStr = 'select '+
                        'm.*,'+
                        'IFNULL(sml.completion_rate,0) as completion_rate,'+
                        'IFNULL(sml.is_complete,false) as is_complete,'+
                        'DATE_FORMAT(sml.start_date, "%Y/%m/%d %H:%i:%s") as start_date_format,'+
                        'DATE_FORMAT(sml.last_access_date, "%Y/%m/%d %H:%i:%s") as last_access_date_format,'+
                        'sml.is_complete,'+
                        'sml.access_counts,'+
                        'SEC_TO_TIME(IFNULL(sml.time_spent,0)) as time_spent_format, '+
                        'IFNULL(sml.time_spent, 0) as time_spent '+
                        'from material m '+
                        'left join student_material_learn sml on m.activity_id=sml.activity_id and m.id=sml.material_id and sml.user_cooc_id=? '+
                        'where m.activity_id=? '+
                        'order by m.activity_order asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//更新PDF教材完成度
module.exports.updatePDFMaterialCompletingStatus = function(data, done){
    var queryStr = 'update student_material_learn set completion_rate=?,time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
//更新PDF教材完成度(適性)
module.exports.updateAdaptivePDFMaterialCompletingStatus = function(data, done){
    var queryStr = 'update student_adaptive_material_learn set completion_rate=?,time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
//更新PDF教材完成度:100%
module.exports.updatePDFMaterialCompletedStatus = function(data, done){
    var queryStr = 'update student_material_learn set completion_rate=100,is_complete=true,completion_date=IF(completion_date IS NULL, now(), completion_date),time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
//更新PDF教材完成度:100%(適性)
module.exports.updateAdaptivePDFMaterialCompletedStatus = function(data, done){
    var queryStr = 'update student_adaptive_material_learn set completion_rate=100,is_complete=true,completion_date=IF(completion_date IS NULL, now(), completion_date),time_spent=time_spent+? where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//取得 PDF 完成度
module.exports.getPDFMaterialCompletion = function(data, done){
    var queryStr = 'select count(*) as counts from student_pdf_result where activity_id=? and material_id=? and user_cooc_id=? and time_spent>=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//取得 PDF 完成度(適性)
module.exports.getAdaptivePDFMaterialCompletion = function(data, done){
    var queryStr = 'select count(*) as counts from student_adaptive_pdf_result where activity_id=? and material_id=? and user_cooc_id=? and material_uuid=? and time_spent>=?';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//學生取得適性學習活動進度
module.exports.studentGetAdaptiveMaterialProgress = function(data, done){
    var queryStr = 'select * '+
    'from student_adaptive_material_learn where activity_id=? and user_cooc_id=? '+
    'order by completion_date asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師刪除教材
module.exports.teacherRemoveMaterial = function(data, done){
    db.get().query('update material set is_delete=true where activity_id=? and id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};
