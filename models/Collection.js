var db = require('../mysql');

//建立新的收藏
module.exports.createCollection = function(data,done){
    db.get().query('insert into collection (name,description,creator_id,creator_cooc_id,create_date) values (?,?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//刪除收藏
module.exports.removeCollection = function(data,done){
    db.get().query('update collection set is_delete=true where id=? and creator_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//編輯收藏
module.exports.updateCollection = function(data,done){
    db.get().query('update collection set name=?,description=? where id=? and creator_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//取得所有收藏
module.exports.getCollections = function(data,done){
    db.get().query('select * from collection where creator_cooc_id=? and is_delete=false', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//取得收藏 By 收藏名稱
module.exports.getCollectionByName = function(data,done){
    db.get().query('select * from collection where name=? and creator_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//加入活動收藏
module.exports.addActivityToCollection = function(data,done){
    db.get().query('insert into collection_activity (collection_id,activity_id,user_cooc_id,creator_id) values (?,?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//移除活動收藏
module.exports.removeActivityFromCollection = function(data,done){
    db.get().query('delete from collection_activity where collection_id=? and activity_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};