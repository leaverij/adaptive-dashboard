var db = require('../mysql');

//教師取得活動指派的群組
module.exports.getAllGroupsByTeacherId = function(data,done){
    var queryData = [data.creator_id];
    var queryStr = 'select teach_group.id,teach_group.name,teach_group.description,DATE_FORMAT(teach_group.create_date, "%Y/%m/%d %H:%i:%s") as create_date,count(group_member.group_id) as members from teach_group' 
                  +' left join group_member on teach_group.id=group_member.group_id where teach_group.creator_id=? and teach_group.is_delete=false';
    if(data.search){
        queryStr += ' and group.name like ?';
        queryData.push('%'+data.search+'%');
    }
    queryStr += ' group by teach_group.id order by ? ? limit ?,?'
    queryData.push(data.sort);
    queryData.push(data.order);
    queryData.push(data.start);
    queryData.push(data.end);

    var returnData = {
        recordsTotal:0,
        recordsFiltered:0,
        draw:1,
        data:[],
        draw:data.draw
    };
    db.get().query('select count(*) as recordsTotal from teach_group where creator_id=?',[data.creator_id],function(error, countsResults, fields){
        returnData.recordsTotal = countsResults[0]['recordsTotal'];
        db.get().query(queryStr,queryData,function(error, results, fields){
            if(error) done(error);
            returnData.recordsFiltered = results.length;
            returnData.data = results;
            done(null,returnData);
        });
    });
};

//檢查教師是否擁有該群組 By CoocID
module.exports.checkGroupOwner = function(data,done){
    db.get().query('select * from teach_group where id=? and creator_cooc_id=?',data,function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得所有群組(下拉選單)
module.exports.getAllGroupsByTeacherIdDropdown = function(data,done){
    var queryStr = 'select teach_group.id,teach_group.name,DATE_FORMAT(teach_group.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format,count(teach_group.id) as group_count'
                  +' from teach_group left join group_member on teach_group.id=group_member.group_id'
                  +' where teach_group.creator_id=? and teach_group.is_delete=false'
                  +' group by teach_group.id'
                  +' order by teach_group.create_date desc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//教師取得所有群組(下拉選單)By CoocID
module.exports.getAllGroupsByTeacherIdDropdownByCoocId = function(data,done){
    var queryStr = 'select teach_group.id,teach_group.name,DATE_FORMAT(teach_group.create_date, "%Y/%m/%d %H:%i:%s") as create_date_format,count(teach_group.id) as group_count'
                  +' from teach_group left join group_member on teach_group.id=group_member.group_id'
                  +' where teach_group.creator_cooc_id=? and teach_group.is_delete=false'
                  +' group by teach_group.id'
                  +' order by teach_group.create_date desc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得所有群組(下拉選單)By CoocID
module.exports.teacherGetActivityAssingedGroupsDropdown = function(data,done){
    var queryStr = 'select tg.id,tg.name '
                  +' from teach_group tg '
                  +' left join activity_assign aa on tg.id=aa.group_id'
                  +' where tg.is_delete=false and aa.activity_id=?'
                  +' order by aa.assign_date asc';
    db.get().query(queryStr, data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得所有群組
module.exports.teacherGetAllGroups = function(data,done){
    db.get().query('select * from teach_group where creator_id=? and is_delete=false', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};
//教師取得所有群組 By CoocID
module.exports.teacherGetAllGroupsByCoocId = function(data, done){
    db.get().query('select * from teach_group where creator_cooc_id=? and is_delete=false', data, function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//教師取得單一群組詳細資料
module.exports.getSingleGroupDetialByGroupId = function(data,done){
    var queryStr = 'select *,DATE_FORMAT(group_member.add_date, "%Y/%m/%d %H:%i:%s") as add_date_format'
                  +' from teach_group left join group_member on teach_group.id=group_member.group_id'
                  +' where teach_group.id=? and teach_group.creator_id=?';
    db.get().query(queryStr,data,function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//教師取得單一群組成員
module.exports.getSingleGroupMembersByGroupId = function(data,done){
    db.get().query('select *,DATE_FORMAT(add_date, "%Y/%m/%d %H:%i:%s") as add_date_format from group_member where group_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,results);
    });
};

//教師建立群組
module.exports.createNewGroup = function(data,done){
    db.get().query('insert into teach_group (name,creator_id,creator_cooc_id) values (?,?,?)', data, function(error, results, fields){
        if(error) done(error);
        done(null,results.insertId);
    });
};

//教師加入酷課雲班級成員
module.exports.addCoocClassMember = function(data,done){
    db.get().query('insert into group_member (group_id,user_mail,user_name,user_cooc_id,user_cooc_seat) values ?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師移除酷課雲成員
module.exports.removeCoocClassMember = function(data,done){
    db.get().query('delete from group_member where group_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//教師刪除群組
module.exports.removeGroupByGroupId = function(data,done){
    db.get().query('update teach_group set is_delete=true where id=? and creator_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};