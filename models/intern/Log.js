var db = require("../../mysql");
var mongodb = require("../../mongo");
//取得學生紀錄
module.exports.getRecord = function(data, done) {
  var collection = mongodb.get().collection("tincan");
  var records = [];
  collection
    .find(
      {
        "object.definition.name.zh-TW": {
          $exists: true
        }
      },
      {
        "actor.account.name": 1,
        "verb.display.zh-TW": 1,
        "verb.id": 1,
        "object.definition.name.zh-TW": 1,
        timestamp: 1
      }
    )
    .sort({
      timestamp: -1
    })
    .toArray(function(error, items) {
      if (error) done(error);
      //轉成自訂格式
      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        records.push({
          actor: item["actor"]["account"]["name"],
          verb: item["verb"]["display"]["zh-TW"],
          object: item["object"]["definition"]["name"]["zh-TW"],
          timestamp: item["timestamp"],
          verbId: item["verb"]["id"].substring(
            item["verb"]["id"].lastIndexOf("/") + 1
          )
        });
      }
      //每個項目中的不同紀錄
      var unique = {
        actor: [],
        verb: [],
        object: [],
        time: []
      };
      //檢查每個項目的所有紀錄中每個不同的紀錄
      for (var i = 0; i < records.length; i++) {
        var actor = records[i]["actor"];
        var verb = records[i]["verb"];
        var object = records[i]["object"];
        var timestamp = records[i]["timestamp"];
        var verbId = records[i]["verbId"];
        if (typeof verb === "undefined") {
          verb = verbId;
        }
        if (unique["actor"].indexOf(actor) === -1) {
          unique["actor"].push(actor);
        }
        if (unique["verb"].indexOf(verb) === -1) {
          unique["verb"].push(verb);
        }
        if (unique["object"].indexOf(object) === -1) {
          unique["object"].push(object);
        }
        var date = new Date(timestamp);
        var newMonth = date.getMonth() + 1;
        var newYear = date.getFullYear();
        var newDate = date.getDate();
        //加入年
        var isYearHave = false;
        for (var j = 0; j < unique["time"].length; j++) {
          var oldyear = unique["time"][j]["year"];
          if (oldyear === newYear) {
            isYearHave = true;
          }
        }
        if (!isYearHave) {
          unique["time"].push({
            year: newYear,
            month: []
          });
        }
        //加入月
        var isMonthHave = false;
        var yearIndex = 0;
        for (var j = 0; j < unique["time"].length; j++) {
          var oldyear = unique["time"][j]["year"];
          if (oldyear === newYear) {
            yearIndex = j;
            for (var k = 0; k < unique["time"][j]["month"].length; k++) {
              var oldMonth = unique["time"][j]["month"][k]["month"];
              if (oldMonth === newMonth) {
                isMonthHave = true;
                //加入日
                if (
                  unique["time"][j]["month"][k]["date"].indexOf(newDate) === -1
                ) {
                  unique["time"][j]["month"][k]["date"].push(newDate);
                }
              }
            }
          }
        }
        if (!isMonthHave) {
          unique["time"][yearIndex]["month"].push({
            month: newMonth,
            date: [newDate]
          });
        }
      }
      done(null, unique);
    });
};
//取得學生篩選後的紀錄
//每個篩選項目的處理方法
var filterFun = {
  selectedDate: function(record, filterValue) {
    var timestamp = record["timestamp"];
    var day = new Date(timestamp);
    var month = day.getMonth();
    var year = day.getFullYear();
    var date = day.getDate();
    //指定的
    var specifyDay = new Date(filterValue);
    var specifyMonth = specifyDay.getMonth();
    var specifyYear = specifyDay.getFullYear();
    if (specifyYear === year && specifyMonth === month) {
      return date;
    } else {
      return false;
    }
  },
  specifyDate: function(record, filterValue) {
    var timestamp = record["timestamp"];
    var time = new Date(timestamp);
    var date = time.getDate();
    var month = time.getMonth() + 1;
    var year = time.getFullYear();
    var nowTime = new Date(filterValue);
    var nowDate = nowTime.getDate();
    var nowMonth = nowTime.getMonth() + 1;
    var nowYear = nowTime.getFullYear();
    console.log(date, month, year);
    console.log(nowDate, nowMonth, nowYear);
    if (year === nowYear && month === nowMonth && date === nowDate) {
      return true;
    } else {
      return false;
    }
  },
  specifyMonth: function(record, filterValue) {
    var timestamp = record["timestamp"];
    var date = new Date(timestamp);
    var month = (date.getMonth() + 1).toString();
    var year = date.getFullYear().toString();
    var nowDate = new Date();
    var nowYear = nowDate.getFullYear().toString();
    if (year === nowYear && month === filterValue) {
      return true;
    } else {
      return false;
    }
  },
  specifyYear: function(record, filterValue) {
    var timestamp = record["timestamp"];
    var date = new Date(timestamp);
    var year = date.getFullYear();
    var specifyDate = new Date(filterValue);
    var specifyYear = specifyDate.getFullYear();
    if (year === specifyYear) {
      return true;
    } else {
      return false;
    }
  },
  today: function(record) {
    var timestamp = record["timestamp"];
    var now = new Date();
    var nowDate = now.toLocaleDateString();
    var date = new Date(timestamp);
    var localDate = date.toLocaleDateString();
    if (nowDate === localDate) {
      return true;
    } else {
      return false;
    }
  },
  month: function(record) {
    var timestamp = record["timestamp"];
    var now = new Date();
    var nowMonth = now.getMonth() + 1;
    var nowYear = now.getFullYear();
    var date = new Date(timestamp);
    var dateMonth = date.getMonth() + 1;
    var dateYear = date.getFullYear();
    if (nowYear === dateYear && nowMonth === dateMonth) {
      return true;
    } else {
      return false;
    }
  },
  year: function(record) {
    var timestamp = record["timestamp"];
    var now = new Date();
    var nowYear = now.getFullYear();
    var date = new Date(timestamp);
    var dateYear = date.getFullYear();
    if (nowYear === dateYear) {
      return true;
    } else {
      return false;
    }
  },
  actor: function(record, filterValue) {
    var actor = record["actor"];
    if (actor.indexOf(filterValue) !== -1) {
      return true;
    } else {
      return false;
    }
  },
  verb: function(record, filterValue) {
    var verb = record["verb"];
    if (verb.indexOf(filterValue) !== -1) {
      return true;
    } else {
      return false;
    }
  },
  object: function(record, filterValue) {
    var object = record["object"];
    if (object.indexOf(filterValue) !== -1) {
      return true;
    } else {
      return false;
    }
  }
};
//清除所有空格
var trimAll = function(str, is_global) {
  var result;
  result = str.replace(/(^\s+)|(\s+$)/g, "");
  if (is_global.toLowerCase() == "g") {
    result = result.replace(/\s/g, "");
  }
  return result;
};
//產生結果字串
var getResultStr = function(filter, records) {
  var filterProperty = filter["property"];
  var filterValue = filter["value"];
  var resultStr = "";
  if (filterProperty !== "selectedDate") {
    for (var i = 0; i < records.length; i++) {
      var record = records[i];
      if (typeof record["verb"] === "undefined") {
        record["verb"] = record["verbId"];
      }
      var isRight = filterFun[filterProperty](record, filterValue);
      if (isRight) {
        //時間轉換
        var date = new Date(record["timestamp"]);
        resultStr +=
          "<div data-vtdate=" +
          '"' +
          date.toLocaleString() +
          '"' +
          "><div class=item-actor>" +
          record["actor"] +
          "</div><div class=item-verb>" +
          record["verb"] +
          "</div><div class=item-object>" +
          record["object"] +
          "</div></div>";
      }
    }
  } else {
    var resultArr = [];
    for (var i = 0; i < records.length; i++) {
      var record = records[i];
      var date = filterFun[filterProperty](record, filterValue);
      if (date && resultArr.indexOf(date) < 0) {
        resultArr.push(date);
      }
    }
    resultStr = resultArr;
  }
  return resultStr;
};
module.exports.getRecordFilterResult = function(data, done) {
  var collection = mongodb.get().collection("tincan");
  var records = [];
  collection
    .find(
      {
        "object.definition.name.zh-TW": {
          $exists: true
        }
      },
      {
        "actor.account.name": 1,
        "verb.display.zh-TW": 1,
        "verb.id": 1,
        "object.definition.name.zh-TW": 1,
        timestamp: 1
      }
    )
    .sort({
      timestamp: -1
    })
    .toArray(function(error, items) {
      if (error) done(error);
      //轉成自訂格式
      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        records.push({
          actor: item["actor"]["account"]["name"],
          verb: item["verb"]["display"]["zh-TW"],
          object: item["object"]["definition"]["name"]["zh-TW"],
          timestamp: item["timestamp"],
          verbId: item["verb"]["id"].substring(
            item["verb"]["id"].lastIndexOf("/") + 1
          )
        });
      }
      //做篩選
      var filterResultStr = getResultStr(data, records);
      done(null, filterResultStr);
    });
};
module.exports.getAnalysisRecord = function(data, done) {
  db
    .get()
    .query(
      "SELECT a.group_name, b.school_name FROM `academy_category` AS a, `academy_school` AS b WHERE a.name = b.academy_category",
      function(error, results, fields) {
        if (error) done(error);
        var newResult = {
          group: [],
          school: []
        };
        for (var i = 0; i < results.length; i++) {
          var result = results[i];
          var group = result["group_name"];
          var school = result["school_name"];
          var department = result["department_name"];
          if (newResult["group"].indexOf(group) === -1) {
            newResult["group"].push(group);
          }
          if (newResult["school"].indexOf(school) === -1) {
            newResult["school"].push(school);
          }
        }
        done(null, newResult);
      }
    );
};
//取得篩選的結果
module.exports.getAnalysisFilterResult = function(filter, done) {
  var filterType = filter.type;
  var filterValue = filter.value;
  var sqlStr = "";
  if (filterType === "0") {
    var sqlStr =
      "SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category";
    if (filterValue.group !== "") {
      sqlStr += " AND a.group_name=" + '"' + filterValue.group + '"';
    }
    if (filterValue.college !== "") {
      sqlStr += " AND b.school_name=" + '"' + filterValue.college + '"';
    }
    if (filterValue.department !== "") {
      sqlStr +=
        " AND b.department_name LIKE" + '"%' + filterValue.department + '%"';
    }
    db.get().query(sqlStr, function(error, results, fields) {
      if (error) done(error);
      done(null, results);
    });
  } else if (filterType === "1") {
    //依照選擇的學科去篩選
    var sqlStr =
      "SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category";
    db.get().query(sqlStr, function(error, results, fields) {
      if (error) done(error);
      //計算學科占比
      results.map(function(item, index) {
        //分子權重加成
        var subjectsWeightAddition = 0;
        //分母權重相加
        var subjectsWeightTotal = 0;
        for (var i = 0; i < filterValue.length; i++) {
          var subject = filterValue[i];
          subjectsWeightAddition += item[subject];
        }
        subjectsWeightTotal =
          item.chinese +
          item.english +
          item.math_a +
          item.math_b +
          item.history +
          item.geography +
          item.citizen +
          item.physics +
          item.chemistry +
          item.biology;
        //新增屬性
        item["scale"] = parseFloat(
          subjectsWeightAddition / subjectsWeightTotal
        ).toFixed(2);
      });
      done(null, results);
    });
  } else if (filterType === "2") {
    //依照選擇的學科去篩選
    var sqlStr =
      "SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology FROM academy_category AS a, academy_school AS b WHERE a.name = b.academy_category";
    db.get().query(sqlStr, function(error, results, fields) {
      if (error) done(error);
      //計算學科Pr值占比
      results.map(function(item, index) {
        //分子權重加成
        var subjectsWeightAddition = 0;
        //分母權重相加
        var subjectsWeightTotal = 0;
        var prTotal = 0;
        for (var i = 0; i < filterValue.length; i++) {
          var subject = filterValue[i].name;
          var prValue = filterValue[i].value / 100;
          if (item[subject] !== 0) {
            subjectsWeightAddition += item[subject] * prValue;
            prTotal += Math.pow(prValue, 2);
          }
        }
        prTotal = Math.sqrt(prTotal);
        subjectsWeightTotal =
          prTotal +
          Math.sqrt(
            Math.pow(item.chinese, 2) +
              Math.pow(item.english, 2) +
              Math.pow(item.math_a, 2) +
              Math.pow(item.math_b, 2) +
              Math.pow(item.history, 2) +
              Math.pow(item.geography, 2) +
              Math.pow(item.citizen, 2) +
              Math.pow(item.physics, 2) +
              Math.pow(item.chemistry, 2) +
              Math.pow(item.biology, 2)
          );
        //新增屬性
        item["similarity"] = parseFloat(
          subjectsWeightAddition / subjectsWeightTotal
        ).toFixed(7);
      });
      done(null, results);
    });
  } else if (filterType === "3") {
    //依照選擇的學科去篩選
    var sqlStr =
      "SELECT a.group_name, b.school_name, b.department_name, b.chinese, b.english, b.math_a, b.math_b, b.history, b.geography, b.citizen, b.physics, b.chemistry, b.biology, c." +
      filterValue["sexValue"] +
      " FROM academy_category AS a, academy_school AS b, academy_interest AS c WHERE a.name = b.academy_category AND c.academy_category = b.academy_category";
    db.get().query(sqlStr, function(error, results, fields) {
      if (error) done(error);
      var newResults = [];
      //計算學科Pr值占比
      results.map(function(item, index) {
        var interests = trimAll(item[filterValue["sexValue"]], "g").split("/");
        //IR,AI,BR(DB)
        for (var i = 0; i < interests.length; i++) {
          //IR
          var interest = interests[i];
          //IA,YG(user)
          for (var j = 0; j < filterValue["codesValue"].length; j++) {
            //IA
            var codeValue = filterValue["codesValue"][j];
            //長度要相同
            if (codeValue.length === interest.length) {
              var count = 0;
              //I,A
              for (var k = 0; k < codeValue.length; k++) {
                //I==I,A==A
                if (interest.indexOf(codeValue.charAt(k)) !== -1) {
                  count++;
                }
              }
              //比對成功
              if (count === codeValue.length) {
                newResults.push(item);
              }
            }
          }
        }
      });
      done(null, newResults);
    });
  }
};
module.exports.getGroupAnalysis = function(type, data, done) {
  //回傳結果
  var result = {};
  if (type === "1_1") {
    result = {
      movies: [
        {
          id: 1,
          name: "軌怪"
        },
        {
          id: 2,
          name: "蠟筆小心"
        },
        {
          id: 3,
          name: "正易聯盟"
        }
      ]
    };
  } else if (type === "1_2") {
    result = {
      students: {
        student_1: [
          {
            duration: 64,
            end: 64,
            start: 0
          },
          {
            duration: 55,
            end: 308,
            start: 253
          },
          {
            duration: 263,
            end: 488,
            start: 225
          }
        ],
        student_2: [
          {
            duration: 12,
            end: 12,
            start: 0
          },
          {
            duration: 50,
            end: 250,
            start: 200
          },
          {
            duration: 30,
            end: 500,
            start: 470
          }
        ]
      },
      count: 1
    };
  } else if (type === "2") {
    //計算學生的參與度和成就表現
    var engagement = 0;
    var achievement = 0;
    //計算平均的參與度和成就表現
    var engagementAvg = 0;
    var achievementAvg = 0;
    result = {
      axis: {
        passive: [
          {
            name: "小王2",
            x: 40,
            y: 30,
            doTest: false
          },
          {
            name: "小王3",
            x: 22,
            y: 25,
            doTest: true
          },
          {
            name: "小王4",
            x: 36,
            y: 41,
            doTest: true
          }
        ],
        change: [
          {
            name: "小王5",
            x: 14,
            y: 89,
            doTest: true
          },
          {
            name: "小6",
            x: 41,
            y: 58,
            doTest: true
          },
          {
            name: "小王7",
            x: 36,
            y: 69,
            doTest: true
          }
        ],
        smart: [
          {
            name: "小王8",
            x: 69,
            y: 32,
            doTest: false
          },
          {
            name: "小王9",
            x: 59,
            y: 42,
            doTest: true
          },
          {
            name: "小王321",
            x: 63,
            y: 22,
            doTest: true
          }
        ],
        positive: [
          {
            name: "小王21",
            x: 60,
            y: 89,
            doTest: true
          },
          {
            name: "小王543",
            x: 78,
            y: 95,
            doTest: true
          },
          {
            name: "小王698",
            x: 57,
            y: 65,
            doTest: true
          }
        ]
      },
      average: {
        axisX: 50,
        axisY: 50
      }
    };
  } else if (type === "3") {
    result = {
      axis: {
        firstGood: [
          {
            name: "小王2",
            x: 40,
            y: -20,
            compare: 20,
            doTest: false
          },
          {
            name: "小王3",
            x: 30,
            y: -10,
            compare: 20,
            doTest: true
          },
          {
            name: "小王4",
            x: 36,
            y: -26,
            compare: 10,
            doTest: true
          }
        ],
        secondGood: [
          {
            name: "小王5",
            x: 14,
            y: 60,
            compare: 74,
            doTest: false
          },
          {
            name: "小6",
            x: 41,
            y: 58,
            compare: 99,
            doTest: true
          },
          {
            name: "小王7",
            x: 36,
            y: 40,
            compare: 76,
            doTest: true
          }
        ]
      },
      average: {
        axisX: 50,
        axisY: 50
      }
    };
  }
  done(null, result);
};
//新增徽章類別
/*
"conditions":{
"1": {
				"name": "",
				"description": "",
				img:"",
				"timestamp": new Date()
	}
}

*/
//插入成就的類別
module.exports.insertBadgeCategory = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection.insert(
    {
      name: data,
      conditions: [],
      timestamp: new Date()
    },
    function(err, records) {
      if (err) done(err);
      collection
        .find()
        .sort({
          timestamp: -1
        })
        .toArray(function(error, items) {
          if (error) done(error);
          done(null, items);
        });
    }
  );
};
//插入成就的條件
module.exports.insertBadgeCondition = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection.find({ name: data.categoryName }).toArray(function(error, items) {
    if (error) done(error);
    items[0].conditions.push({
      name: data.conditionName,
      description: data.conditionDescription,
      img: data.conditionPicture,
      timestamp: new Date()
    });

    collection.update(
      { name: data.categoryName },
      { $set: { conditions: items[0].conditions } },
      function(err, records) {
        if (err) done(err);

        collection
          .find()
          .sort({
            timestamp: -1
          })
          .toArray(function(error, items2) {
            if (error) done(error);
            done(null, items2);
          });
      }
    );
  });
};
//取得成就的條件
module.exports.getBadgeCondition = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection
    .find({ name: data.categoryName })
    .sort({
      timestamp: -1
    })
    .toArray(function(error, items) {
	  if (error) done(error);
      done(null, items[0]["conditions"][parseInt(data.level)]);
    });
};
//修改成就的條件
module.exports.updateBadgeCondition = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection.find({ name: data.categoryName }).toArray(function(error, items) {
    if (error) done(error);

    items[0].conditions[data.level] = {
      name:data.conditionName
      ,description:data.conditionDescription,
      img:data.conditionPicture,
      timestamp:new Date()
    };

    collection.update(
      { name: data.categoryName },
      { $set: { conditions: items[0].conditions } },
      function(err, records) {
        if (err) done(err);

        collection
          .find()
          .sort({
            timestamp: -1
          })
          .toArray(function(error, items2) {
            if (error) done(error);
            done(null, items2);
          });
      }
    );
  });
};
//移除條件
module.exports.deleteBadgeCondition = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection.find({ name: data.categoryName }).toArray(function(error, items) {
    if (error) done(error);

    items[0].conditions.splice(data.level,1);

    collection.update(
      { name: data.categoryName },
      { $set: { conditions: items[0].conditions } },
      function(err, records) {
        if (err) done(err);

        collection
          .find()
          .sort({
            timestamp: -1
          })
          .toArray(function(error, items2) {
            if (error) done(error);
            done(null, items2);
          });
      }
    );
  });
};



//取得成就的資訊
module.exports.getBadgesInfo = function(data, done) {
  var collection = mongodb.get().collection("badge");
  collection
    .find()
    .sort({
      timestamp: -1
    })
    .toArray(function(error, items) {
      if (error) done(error);
      done(null, items);
    });
};

//學生點擊教材
module.exports.accessMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,type,action) values (?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生閱讀PDF頁面
module.exports.readPDFPage = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,duration,type,action,page) values (?,?,?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生觀看影片片段
module.exports.watchVideoClip = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,duration,type,action,start_point,end_point) values (?,?,?,?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生瀏覽網頁
module.exports.viewWeb = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,start_date,end_date,duration,type,action) values (?,?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記:影片教材
module.exports.noteVideoMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,point,complete_date) values (?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記Log:影片教材
module.exports.logNoteVideoMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,page,note) values (?,?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記:網頁教材
module.exports.noteWebMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,complete_date) values (?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記Log:網頁教材
module.exports.logNoteWebMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,note) values (?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記:PDF教材
module.exports.notePDFMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_note (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,note,type,complete_date) values (?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//學生筆記Log:PDF教材
module.exports.logNotePDFMaterial = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,material_id,creator_id,creator_cooc_id,user_cooc_id,end_date,type,action,note) values (?,?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
//取得筆記
module.exports.getMaterialNotes = function(data, done) {
  db
    .get()
    .query(
      'select *,DATE_FORMAT(complete_date, "%Y/%m/%d %H:%i:%s") as note_date,SEC_TO_TIME(point) as point_format from student_learn_note where material_id=? and user_cooc_id=? and is_delete=false order by complete_date desc',
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results);
      }
    );
};
//刪除筆記
module.exports.deleteMaterialNote = function(data, done) {
  db
    .get()
    .query(
      "update student_learn_note set is_delete=true,delete_date=now() where id=? and material_id=? and user_cooc_id=?",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, true);
      }
    );
};
//更新筆記
module.exports.updateMaterialNote = function(data, done) {
  db
    .get()
    .query(
      "update student_learn_note set note=?,update_date=now() where id=? and material_id=? and user_cooc_id=?",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, true);
      }
    );
};
//進入討論問題
module.exports.accessForumQuestion = function(data, done) {
  db
    .get()
    .query(
      "insert into student_learn_log (activity_id,creator_id,creator_cooc_id,user_cooc_id,start_date,type,action,category) values (?,?,?,?,?,?,?,?)",
      data,
      function(error, results, fields) {
        if (error) done(error);
        done(null, results.insertId);
      }
    );
};
