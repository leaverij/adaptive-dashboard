var db = require('../mongo');

//取得最近10筆 Statements
module.exports.getLastTenStatementsByUserId = function(data, done){
    var collection = db.get().collection('tincan');
    collection.find({
        userId:data.user_id
    },{
        _id:0,
        actor:1,
        verb:1,
        object:1,
        result:1,
        timestamp:1,
        userId:1
    }).sort({
        time_stamp:-1
    }).skip(data.skip).limit(data.limit).toArray(function(err, docs){
        done(null, docs);
    });  
};

//取得最近30天每天記錄數量
module.exports.getLastThirtyDaysStatementCountsPerDay = function(data, done){
    var today = new Date()
    var priorDate = new Date().setDate(today.getDate()-850);
    var collection = db.get().collection('tincan');
    collection.aggregate([
        {
            '$match':{
                'userId':data.user_id        
            }
        },
        {
            '$group': {
                '_id': {
                    '$dateToString': {
                        'format': '%Y-%m-%d',
                        'date':'$time_stamp'
                    }
                },
                'count': { '$sum': 1 }
            }
        },
        {
            '$project':{
                '_id':0,
                'date':'$_id',
                'count':1
            }
        }
    ], function(err, results){
        done(null, results);
    });
};