var db = require('../mysql');

//發布問題
module.exports.askQuestion = function(data,done){
    db.get().query('insert into activity_forum (activity_id,creator_id,creator_cooc_id,title,content,post_date,user_id,user_cooc_id,is_delete,view_counts,response_counts) values (?,?,?,?,?,?,?,?,false,0,0)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//取得所有問題
module.exports.getQuestions = function(data,done){
    var queryCountsData = [data.activity_id];
    var queryData = [data.activity_id];
    var queryCountsStr = 
                'select count(*) as counts '+
                'from activity_forum '+
                'where activity_forum.activity_id=? and activity_forum.is_delete=false';
    var queryStr = 'select '+
                     'activity_forum.id,'+
                     'activity_forum.title,'+
                     'activity_forum.content,'+
                     'DATE_FORMAT(activity_forum.post_date, "%Y/%m/%d %H:%i:%s") as post_date_format,'+
                     'account.name as user_name,'+
                     'account.id as user_id,'+
                     'account.cooc_id as user_cooc_id,'+
                     'activity_forum.view_counts,'+
                     'activity_forum.response_counts '+
                   'from activity_forum '+
                   'left join account on activity_forum.user_cooc_id=account.cooc_id '+
                   'left join activity_forum_response on activity_forum.id=activity_forum_response.question_id '+
                   'left join activity_forum_view on activity_forum.id=activity_forum_view.question_id '+
                   'where activity_forum.activity_id=? and activity_forum.is_delete=false';
    if(data.search){
        queryCountsStr += ' and activity_forum.title like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and activity_forum.title like ?';
        queryData.push('%'+data.search+'%');
    }
    queryStr += (' group by activity_forum.id');
    queryStr += (' order by activity_forum.'+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        total:0,
        data:[]
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResults, countsFields){
        returnData.total = countsResults[0]['counts'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            returnData.data = results;
            done(null, returnData);
        });
    });
};

//取得已追蹤問題
module.exports.getTraceQuestions = function(data,done){
    var queryCountsData = [data.activity_id,data.user_cooc_id];
    var queryData = [data.activity_id,data.user_cooc_id];
    var queryCountsStr = 
                'select '+
                  'count(*) as counts '+
                'from activity_forum_question_trace '+
                'left join activity_forum on activity_forum.id=activity_forum_question_trace.question_id '
                'where activity_forum_question_trace.activity_id=? and activity_forum_question_trace.user_cooc_id=? and activity_forum.is_delete=false';
    var queryStr = 'select '+
                     'activity_forum.id,'+
                     'activity_forum.title,'+
                     'activity_forum.content,'+
                     'DATE_FORMAT(activity_forum.post_date, "%Y/%m/%d %H:%i:%s") as post_date_format,'+
                     'account.name as user_name,'+
                     'account.id as user_id,'+
                     'account.cooc_id as user_cooc_id,'+
                     'activity_forum.view_counts,'+
                     'activity_forum.response_counts '+
                   'from activity_forum_question_trace '+
                   'left join activity_forum on activity_forum_question_trace.question_id=activity_forum.id '+
                   'left join account on activity_forum_question_trace.user_cooc_id=account.cooc_id '+
                   'left join activity_forum_response on activity_forum_question_trace.question_id=activity_forum_response.question_id '+
                   'left join activity_forum_view on activity_forum_question_trace.question_id=activity_forum_view.question_id '+
                   'where activity_forum_question_trace.activity_id=? and activity_forum_question_trace.user_cooc_id=? and activity_forum.is_delete=false';
    if(data.search){
        queryCountsStr += ' and activity_forum.title like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and activity_forum.title like ?';
        queryData.push('%'+data.search+'%');
    }
    //queryStr += (' group by activity_forum.id');
    queryStr += (' order by activity_forum.'+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        total:0,
        data:[]
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResults, countsFields){
        returnData.total = countsResults[0]['counts'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            returnData.data = results;
            done(null, returnData);
        });
    });
};

//取得我的問題
module.exports.getMyQuestions = function(data,done){
    var queryCountsData = [data.activity_id,data.user_cooc_id];
    var queryData = [data.activity_id,data.user_cooc_id];
    var queryCountsStr = 
                'select count(*) as counts '+
                'from activity_forum '+
                'where activity_forum.activity_id=? and activity_forum.user_cooc_id=? and activity_forum.is_delete=false';
    var queryStr = 'select '+
                     'activity_forum.id,'+
                     'activity_forum.title,'+
                     'activity_forum.content,'+
                     'DATE_FORMAT(activity_forum.post_date, "%Y/%m/%d %H:%i:%s") as post_date_format,'+
                     'account.name as user_name,'+
                     'account.id as user_id,'+
                     'account.cooc_id as user_cooc_id,'+
                     'activity_forum.view_counts,'+
                     'activity_forum.response_counts '+
                   'from activity_forum '+
                   'left join account on activity_forum.user_cooc_id=account.cooc_id '+
                   'left join activity_forum_response on activity_forum.id=activity_forum_response.question_id '+
                   'left join activity_forum_view on activity_forum.id=activity_forum_view.question_id '+
                   'where activity_forum.activity_id=? and activity_forum.user_cooc_id=? and activity_forum.is_delete=false';
    if(data.search){
        queryCountsStr += ' and activity_forum.title like ?';
        queryCountsData.push('%'+data.search+'%');
        queryStr += ' and activity_forum.title like ?';
        queryData.push('%'+data.search+'%');
    }
    queryStr += (' group by activity_forum.id');
    queryStr += (' order by activity_forum.'+data.sort+' '+data.order);
    queryStr += (' limit '+data.start+','+data.end);
    var returnData = {
        total:0,
        data:[]
    };
    db.get().query(queryCountsStr, queryCountsData, function(countsError, countsResults, countsFields){
        returnData.total = countsResults[0]['counts'];
        db.get().query(queryStr, queryData, function(error, results, fields){
            returnData.data = results;
            done(null, returnData);
        });
    });
};

//取得單一問題
module.exports.getSingleQuestionDetail = function(data,done){
    var queryQuestionStr = 'select '+
                             'account.name as user_name,'+
                             'activity_forum.user_id,'+
                             'activity_forum.user_cooc_id,'+
                             'activity_forum.id as question_id,'+
                             'activity_forum.title as question_title,'+
                             'activity_forum.content as question_content,'+
                             '(select count(*) from activity_forum_question_trace where activity_forum.id=activity_forum_question_trace.question_id) as trace_counts,'+
                             'DATE_FORMAT(activity_forum.post_date, "%Y/%m/%d %H:%i:%s") as post_date_format '+
                           'from activity_forum '+
                           'left join account on activity_forum.user_cooc_id=account.cooc_id '+
                           'where activity_forum.id=? and activity_forum.activity_id=?';
    var queryResponseStr = 'select '+
                             'activity_forum_response.id,'+
                             'activity_forum_response.response,'+
                             'DATE_FORMAT(activity_forum_response.response_date, "%Y/%m/%d %H:%i:%s") as response_date_format,'+
                             'account.cooc_id as user_cooc_id,'+
                             'account.name as user_name,'+
                             'activity_forum_response.user_id '+
                           'from activity_forum_response '+
                           'left join account on activity_forum_response.user_cooc_id=account.cooc_id '+
                           'where activity_forum_response.question_id=? and activity_forum_response.activity_id=?';
    db.get().query(queryQuestionStr, data, function(error, results, fields){
        var question = results[0];
        var question_id = question.question_id;
        db.get().query(queryResponseStr, data, function(error2, results2, fields2){
            question['responses'] = results2;
            done(null, question);
        });
    });
};

//瀏覽問題
module.exports.viewQuestion = function(data,done){
    db.get().query('insert into activity_forum_view (activity_id,question_id,user_id,user_cooc_id,view_date) values (?,?,?,?,now())', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//回應問題
module.exports.responseQuestion = function(data,done){
    db.get().query('insert into activity_forum_response (question_id,activity_id,response,response_date,user_id,user_cooc_id,is_delete) values (?,?,?,now(),?,?,false)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//更新瀏覽問題次數
module.exports.updateQuestionViewCounts = function(data,done){
    db.get().query('update activity_forum set view_counts=view_counts+1 where id=? and activity_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//更新回應問題次數
module.exports.updateQuestionResponseCounts = function(data,done){
    db.get().query('update activity_forum set response_counts=response_counts+1 where id=? and activity_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null,true);
    });
};

//取得熱門話題
module.exports.getHotQuestions = function(data,done){
    var queryStr = 'select '+
                        'activity_forum.id,'+
                        'activity_forum.title,'+
                        'activity_forum.view_counts '+
                    'from activity_forum '+
                    'where activity_forum.activity_id=? and activity_forum.is_delete=false '+
                    'order by activity_forum.view_counts desc limit 3';
    db.get().query(queryStr,data,function(error, results, fields){
        if(error) done(error);
        done(null, results);
    });
};

//追蹤問題
module.exports.traceQuestion = function(data,done){
    db.get().query('insert into activity_forum_question_trace (activity_id,question_id,user_cooc_id,trace_date,creator_cooc_id) values (?,?,?,now(),?)', data, function(error, results, fields){
        if(error) done(error);
        done(null, results.insertId);
    });
};

//取消追蹤問題
module.exports.cancelTraceQuestion = function(data,done){
    db.get().query('delete from activity_forum_question_trace where question_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, true);
    });
};

//取得問題數量
module.exports.getAllQuestionCounts = function(data,done){
    db.get().query('select count(*) as counts from activity_forum where activity_id=? and is_delete=false', data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//取得已追蹤問題數量
module.exports.getAllTraceQuestionCounts = function(data,done){
    db.get().query('select count(*) as counts from activity_forum_question_trace where activity_id=? and user_cooc_id=?', data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};

//取得我的問題數量
module.exports.getMyQuestionCounts = function(data,done){
    db.get().query('select count(*) as counts from activity_forum where activity_id=? and user_cooc_id=? and is_delete=false', data, function(error, results, fields){
        if(error) done(error);
        done(null, results[0]['counts']);
    });
};