'use strict';

//mysql
exports.mysql = {
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: '1234',
  database: 'adaptive'
};

//mongodb
exports.mongodb = {
    //mongodb://user:pass@localhost:port/database
    uri: 'mongodb://localhost:27017/xLRS'
};

//cooc
exports.cooc = {
  sso_school:'https://school.sso.tp.edu.tw/', //親師生學籍資料API
  sso_url:'https://sso.tp.edu.tw/',
  client_id:'364257692326-k0djg7r8e27f3oottmt6io147tna39g5.apps.cooc',
  client_secret:'9eBlgfJO8IErn3Kmi0_HlIbB',
  get_account_path:'accountservice/cloudservice/account/userprofile/get.so',
  get_user_path:'school/cloudservice/api/sms/user/get.so',
  get_sms_path:'school/cloudservice/api/sms/term/list.so', //取得學年學期基本資訊
  get_student_list:'school/cloudservice/api/sms/teacher/group/students/list.so', //教師取得學生名單
  learning_api:'http://learning.cooc.tp.edu.tw/coocLearning/coocAPI/',
  lrs_api:'http://lad.cooc.tp.edu.tw/taipei/WEBAPI/adaptive/'
};

//檔案上傳
module.exports.fileupload = {
  maxSize:1000*1024*1024, //1GB
  tempDir:'tmp',
  targetDir:'uploads/activity',
  accessDir:'/uploadm/activity'
};

//xapi
exports.xapi = {
  lrs:{
    endpoint:'http://localhost/visca/TCAPI/',
    key:'B26836171A1745C281AB',
    password:'FF8398919CDD61733CE5'
  },
  recipe:{
    acrossx:{
      access:'https://w3id.org/xapi/seriousgames/verbs/accessed',
      video:{
        action:{
          watch:{
            verb:'https://w3id.org/xapi/acrossx/verbs/watched'
          },
          skip:{
            verb:'https://w3id.org/xapi/acrossx/verbs/skipped'
          }
        },
        type:'https://w3id.org/xapi/acrossx/activities/video'
      },
      web:{
        type:'https://w3id.org/xapi/acrossx/activities/webpage'
      }
    },
    adb:{
      action:{
        read:{
          verb:'https://w3id.org/xapi/adb/verbs/read'
        },
        note:{
          verb:'https://w3id.org/xapi/adb/verbs/noted'
        }
      }
    }
  }
};