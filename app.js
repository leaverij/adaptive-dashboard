var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');

//多國語系
var i18n = require('i18next');  
var i18nFsBackend = require('i18next-node-fs-backend');  
var i18nMiddleware = require('i18next-express-middleware');  

var index = require('./routes/index');
var users = require('./routes/users');
var analytics = require('./routes/analytics');
var developer = require('./routes/developer');
var dashboard = require('./routes/dashboard');
var dashboard_intern=require('./routes/intern/dashboard');
var teacher = require('./routes/teacher');
var student = require('./routes/student');
var activity_api = require('./routes/api/activity');
var material_api = require('./routes/api/material');
var group_api = require('./routes/api/group');
var collection_api = require('./routes/api/collection');
var log_api = require('./routes/api/log');
var forum_api = require('./routes/api/forum');
var dashboard_api = require('./routes/api/dashboard');
var group = require('./routes/group');
var forum = require('./routes/forum');

// mysql 
var mysql_db = require('./mysql');
mysql_db.connect(function(err){
  if(err){
    console.log('Unable to connect to MySQL.');
    process.exit(1);
  }
  console.log('Connecting MySQL successfully.');
});

//mongodb
var mongodb = require('./mongo');
mongodb.connect(function(err){
  if(err){
    console.log('Unable to connect to MongoDB.');
    process.exit(1);
  }
  console.log('Connecting MongoDB successfully.');
});

var app = express();

// i18next 初始設定
i18n.use(i18nMiddleware.LanguageDetector) // 自動偵測用戶端語系
    .use(i18nFsBackend)
    .init({
      fallbackLng: "en", // 備用語系，擷取失敗時會使用到這裡
      backend: {
        loadPath: "locales/{{lng}}/translation.json",
      }
    });

app.use(i18nMiddleware.handle(i18n, {
  
}));
app.locals.t = function(key){
  return i18n.t(key);
};

// 取得酷課雲的 header 與 footer
http.get({hostname:'cooc.tp.edu.tw',path:'/header.html'},function(result){
  console.log('get cooc header status code:'+result.statusCode);
  var body = '';
  result.setEncoding("UTF-8");
  result.on('data',function(chunk){
    body += chunk;
  }).on('end',function(){
    console.log('get header end');
    app.locals.cooc_header = body;
  });
});
http.get({hostname:'cooc.tp.edu.tw',path:'/header_logon.html'},function(result){
  console.log('get cooc login header status code:'+result.statusCode);
  var body = '';
  result.setEncoding("UTF-8");
  result.on('data',function(chunk){
    body += chunk;
  }).on('end',function(){
    console.log('get login header end');
    app.locals.cooc_header_login = body;
  });
});
http.get({hostname:'cooc.tp.edu.tw',path:'/footer.html'},function(result){
  console.log('get cooc footer status code:'+result.statusCode);
  var body = '';
  result.setEncoding("UTF-8");
  result.on('data',function(chunk){
    body += chunk;
  }).on('end',function(){
    console.log('get footer end');
    app.locals.cooc_footer = body;
  });
});

// set session
app.set('trust proxy', 1); // trust first proxy
app.use(session({
  secret: 'adaptivedashboardiiideicloud',
  resave: false,
  saveUninitialized: true
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit:'50mb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploadm', express.static(path.join(__dirname, 'uploads')));
app.use('/locales', express.static(path.join(__dirname, 'locales')));

app.use('/', index);
app.use('/users', users);
app.use('/analytics', analytics);
app.use('/developer', developer);
app.use('/dashboard', dashboard);
app.use('/intern/dashboard', dashboard_intern);
app.use('/teacher', teacher);
app.use('/student', student);
app.use('/group', group);
app.use('/forum', forum);
//api
app.use('/api/activity', activity_api);
app.use('/api/material', material_api);
app.use('/api/group', group_api);
app.use('/api/collection', collection_api);
app.use('/api/log', log_api);
app.use('/api/forum', forum_api);
app.use('/api/dashboard', dashboard_api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
