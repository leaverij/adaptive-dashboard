//mongodb connection
var config = require('./config');
var mongodb = require('mongodb');

var MongoClient = require('mongodb').MongoClient;
var db;

exports.connect = function(done){
    MongoClient.connect(config.mongodb.uri, function(err, database) {
        if(err) throw err;
        db = database;
        console.log("Connected successfully to server");
        done(null);
    });
};

exports.get = function(){
    return db;
};