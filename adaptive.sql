/*
SQLyog Community Edition- MySQL GUI v7.1 Beta2
MySQL - 5.6.16-log : Database - adaptive
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`adaptive` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `adaptive`;

/*Table structure for table `academy_category` */

DROP TABLE IF EXISTS `academy_category`;

CREATE TABLE `academy_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `group_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

/*Table structure for table `academy_group` */

DROP TABLE IF EXISTS `academy_group`;

CREATE TABLE `academy_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Table structure for table `academy_interest` */

DROP TABLE IF EXISTS `academy_interest`;

CREATE TABLE `academy_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `academy_category` varchar(25) DEFAULT NULL,
  `male` varchar(64) DEFAULT NULL,
  `female` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

/*Table structure for table `academy_school` */

DROP TABLE IF EXISTS `academy_school`;

CREATE TABLE `academy_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` varchar(50) DEFAULT NULL,
  `school_name` varchar(64) DEFAULT NULL,
  `department_name` varchar(64) DEFAULT NULL,
  `academy_category` varchar(64) DEFAULT NULL,
  `chinese` float DEFAULT NULL,
  `english` float DEFAULT NULL,
  `math_a` float DEFAULT NULL,
  `math_b` float DEFAULT NULL,
  `history` float DEFAULT NULL,
  `geography` float DEFAULT NULL,
  `citizen` float DEFAULT NULL,
  `physics` float DEFAULT NULL,
  `chemistry` float DEFAULT NULL,
  `biology` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2063 DEFAULT CHARSET=utf8;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `email` varchar(256) NOT NULL COMMENT '使用者Email',
  `password` varchar(256) DEFAULT NULL COMMENT '使用者密碼',
  `name` varchar(40) DEFAULT NULL COMMENT '使用者姓名',
  `role` int(11) DEFAULT NULL COMMENT '使用者角色',
  `cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `register_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '使用者註冊時間',
  `teacher_flag` tinyint(1) DEFAULT '0' COMMENT '教師角色標籤',
  `student_flag` tinyint(1) DEFAULT '0' COMMENT '學生角色標籤',
  `parent_flag` tinyint(1) DEFAULT '0' COMMENT '家長角色標籤',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `name` varchar(30) NOT NULL COMMENT '活動名稱',
  `type` int(11) NOT NULL COMMENT '活動類型',
  `description` text COMMENT '活動說明',
  `goal` text COMMENT '活動目標',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '活動建立日期',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '活動是否被刪除',
  `delete_date` datetime DEFAULT NULL COMMENT '活動刪除日期',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `subject_id` int(11) DEFAULT NULL COMMENT '活動所屬科目ID',
  `subject_name` varchar(20) DEFAULT NULL COMMENT '活動所屬科目名稱',
  `grade` int(11) DEFAULT NULL COMMENT '活動所屬年級',
  `school` varchar(128) DEFAULT NULL COMMENT '活動所屬學校',
  `semester` varchar(64) DEFAULT NULL COMMENT '活動所屬學年學期',
  `is_share` tinyint(1) DEFAULT '0' COMMENT '活動是否共享',
  `is_public` int(11) DEFAULT '0' COMMENT '活動是否公開',
  `is_assign` tinyint(1) DEFAULT '0' COMMENT '活動是否已指派',
  `access_code` varchar(5) DEFAULT NULL COMMENT '活動代碼',
  `assign_group` int(11) DEFAULT '0' COMMENT '活動指派群組數量',
  `img_url` varchar(256) DEFAULT NULL COMMENT '活動圖片URL',
  `img_data` longtext,
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '活動建立者酷課雲ID',
  `hierarchy` longtext COMMENT '適性活動架構',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

/*Table structure for table `activity_assign` */

DROP TABLE IF EXISTS `activity_assign`;

CREATE TABLE `activity_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `assign_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '活動指派日期',
  `group_id` int(11) DEFAULT NULL COMMENT '活動指派群組ID',
  `is_open` tinyint(1) DEFAULT '1' COMMENT '活動是否開啟',
  `start_date` datetime DEFAULT NULL COMMENT '活動開始時間',
  `end_date` datetime DEFAULT NULL COMMENT '活動結束時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8;

/*Table structure for table `activity_forum` */

DROP TABLE IF EXISTS `activity_forum`;

CREATE TABLE `activity_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '活動建立者酷課雲ID',
  `title` varchar(30) DEFAULT NULL COMMENT '問題標題',
  `content` longtext COMMENT '問題內容',
  `post_date` datetime DEFAULT NULL COMMENT '問題發表日期',
  `tags` varchar(128) DEFAULT NULL COMMENT '問題標籤',
  `user_id` int(11) DEFAULT NULL COMMENT '問題發問者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '問題發問者酷課雲ID',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '問題是否被刪除',
  `delete_date` datetime DEFAULT NULL COMMENT '問題被刪除日期',
  `update_date` datetime DEFAULT NULL COMMENT '問題更新日期',
  `view_counts` int(11) DEFAULT '0' COMMENT '問題被瀏覽次數',
  `response_counts` int(11) DEFAULT '0' COMMENT '問題被回應次數',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activity_forum_question_trace` */

DROP TABLE IF EXISTS `activity_forum_question_trace`;

CREATE TABLE `activity_forum_question_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `question_id` int(11) DEFAULT NULL COMMENT '問題ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `trace_date` datetime DEFAULT NULL COMMENT '追蹤日期',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '問題發問者酷課雲ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activity_forum_response` */

DROP TABLE IF EXISTS `activity_forum_response`;

CREATE TABLE `activity_forum_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `question_id` int(11) DEFAULT NULL COMMENT '問題ID',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `response` longtext COMMENT '問題回應',
  `response_date` datetime DEFAULT NULL COMMENT '問題回應日期',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '問題回應是否被刪除',
  `delete_date` datetime DEFAULT NULL COMMENT '問題回應刪除日期',
  `update_date` datetime DEFAULT NULL COMMENT '問題回應更新日期',
  `user_id` int(11) DEFAULT NULL COMMENT '問題回應者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '問題回應者酷課雲ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activity_forum_view` */

DROP TABLE IF EXISTS `activity_forum_view`;

CREATE TABLE `activity_forum_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `question_id` int(11) DEFAULT NULL COMMENT '問題ID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `view_date` datetime DEFAULT NULL COMMENT '瀏覽日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activity_node` */

DROP TABLE IF EXISTS `activity_node`;

CREATE TABLE `activity_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '適性活動ID',
  `name` varchar(20) DEFAULT NULL COMMENT '節點名稱',
  `type` int(11) DEFAULT NULL COMMENT '節點類型',
  `is_root` tinyint(1) DEFAULT '0' COMMENT '是否是根節點',
  `parent_id` int(11) DEFAULT NULL COMMENT '父節點ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `activity_share` */

DROP TABLE IF EXISTS `activity_share`;

CREATE TABLE `activity_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `member_id` int(11) DEFAULT NULL COMMENT '活動共編者ID',
  `member_mail` varchar(256) DEFAULT NULL COMMENT '活動共編者Email',
  `member_name` varchar(20) DEFAULT NULL COMMENT '活動共編者姓名',
  `member_share` int(11) DEFAULT NULL COMMENT '活動共編者權限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `collection` */

DROP TABLE IF EXISTS `collection`;

CREATE TABLE `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `name` varchar(20) DEFAULT NULL COMMENT '收藏名稱',
  `description` text COMMENT '收藏描述',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '收藏是否被刪除',
  `creator_id` int(11) DEFAULT NULL COMMENT '收藏建立者ID',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '收藏建立日期',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '收藏建立者酷課雲ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `collection_activity` */

DROP TABLE IF EXISTS `collection_activity`;

CREATE TABLE `collection_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `collection_id` int(11) DEFAULT NULL COMMENT '收藏ID',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `collect_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '收藏日期',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `group_member` */

DROP TABLE IF EXISTS `group_member`;

CREATE TABLE `group_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `group_id` int(11) DEFAULT NULL COMMENT '群組ID',
  `user_id` int(11) DEFAULT NULL COMMENT '群組成員使用者ID',
  `user_mail` varchar(256) DEFAULT NULL COMMENT '群組成員Email',
  `user_name` varchar(20) DEFAULT NULL COMMENT '群組成員名稱',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '群組成員酷課雲使用者ID',
  `user_cooc_seat` varchar(4) DEFAULT NULL COMMENT '群組成員酷課雲使用者座號',
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '群組成員加入時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

/*Table structure for table `login_history` */

DROP TABLE IF EXISTS `login_history`;

CREATE TABLE `login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_mail` varchar(256) DEFAULT NULL COMMENT '使用者Email',
  `user_name` varchar(20) DEFAULT NULL COMMENT '使用者姓名',
  `login_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '使用者登入時間',
  `login_ip` varchar(40) DEFAULT NULL COMMENT '使用者登入IP',
  `login_role` int(11) DEFAULT NULL COMMENT '使用者登入角色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1593 DEFAULT CHARSET=utf8;

/*Table structure for table `material` */

DROP TABLE IF EXISTS `material`;

CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '教材所屬活動ID',
  `name` varchar(256) NOT NULL COMMENT '教材名稱',
  `upload_name` text COMMENT '教材原始名稱',
  `goal` text COMMENT '教材目標',
  `bloom` varchar(30) DEFAULT NULL COMMENT '教材對應Bloom',
  `description` text COMMENT '教材說明',
  `type` int(11) DEFAULT NULL COMMENT '教材類型',
  `time_limit` varchar(24) DEFAULT NULL COMMENT '教材限制時間',
  `url` varchar(256) DEFAULT NULL COMMENT '教材連結',
  `total_score` int(11) DEFAULT NULL COMMENT '教材分數',
  `weight` int(11) DEFAULT NULL COMMENT '教材權重',
  `activity_order` int(11) DEFAULT NULL COMMENT '教材順序',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '教材是否刪除',
  `creator_id` int(11) DEFAULT NULL COMMENT '教材建立者ID',
  `create_date` datetime DEFAULT NULL COMMENT '教材建立時間',
  `update_date` datetime DEFAULT NULL COMMENT '教材修改時間',
  `embedded` tinyint(1) DEFAULT '0' COMMENT '網頁教材是否允許被嵌入',
  `pass_method` varchar(2) DEFAULT '0' COMMENT '通過條件方法',
  `pass_val` int(11) DEFAULT NULL COMMENT '通過條件數字',
  `required_time_spent_val` int(11) DEFAULT '0' COMMENT '預計完成時間',
  `required_time_spent_method` varchar(2) DEFAULT '0' COMMENT '預計完成時間計算方法',
  `duration` int(11) DEFAULT NULL COMMENT '影片教材的影片長度',
  `quizs` text COMMENT '測驗教材題目',
  `pages` int(11) DEFAULT '0' COMMENT 'PDF教材頁數',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

/*Table structure for table `material_criteria` */

DROP TABLE IF EXISTS `material_criteria`;

CREATE TABLE `material_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `name` varchar(256) DEFAULT NULL COMMENT '教材通過條件名稱',
  `type` int(11) NOT NULL COMMENT '教材通過條件對應教材類型',
  `percent` int(11) DEFAULT NULL COMMENT '教材通過條件百分比',
  `duration` varchar(24) DEFAULT NULL COMMENT '教材通過條件花費時間',
  `score` int(11) DEFAULT NULL COMMENT '教材通過條件分數',
  `creator_id` int(11) NOT NULL COMMENT '教材通過條件建立者ID',
  `create_date` datetime DEFAULT NULL COMMENT '教材通過條件建立日期',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '教材通過條件是否刪除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_activity_join` */

DROP TABLE IF EXISTS `student_activity_join`;

CREATE TABLE `student_activity_join` (
  `activity_id` int(11) NOT NULL COMMENT '活動ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '活動建立者酷課雲ID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) NOT NULL COMMENT '使用者酷課雲ID',
  `join_date` datetime DEFAULT NULL COMMENT '加入活動日期',
  `completion_rate` int(11) DEFAULT NULL COMMENT '活動完成度',
  `last_access_date` datetime DEFAULT NULL COMMENT '最近一次進入活動時間',
  `access_learn_date` datetime DEFAULT NULL COMMENT '開始學習日期',
  `time_spent` int(11) DEFAULT NULL COMMENT '花費時間',
  PRIMARY KEY (`activity_id`,`user_cooc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_adaptive_material_learn` */

DROP TABLE IF EXISTS `student_adaptive_material_learn`;

CREATE TABLE `student_adaptive_material_learn` (
  `activity_id` int(11) NOT NULL COMMENT '活動ID',
  `material_id` int(11) NOT NULL COMMENT '教材ID',
  `material_uuid` varchar(40) NOT NULL COMMENT '教材UUID',
  `user_cooc_id` varchar(40) NOT NULL COMMENT '使用者酷課雲ID',
  `time_spent` int(11) DEFAULT '0' COMMENT '花費時間',
  `completion_rate` int(11) DEFAULT NULL COMMENT '完成率',
  `score` int(11) DEFAULT NULL COMMENT '分數',
  `is_complete` tinyint(1) DEFAULT '0' COMMENT '是否完成',
  `access_counts` int(11) DEFAULT NULL COMMENT '點擊次數',
  `completion_date` datetime DEFAULT NULL COMMENT '完成日期',
  `last_access_date` datetime DEFAULT NULL COMMENT '最近一次點擊日期',
  `start_date` datetime DEFAULT NULL COMMENT '開始學習日期',
  PRIMARY KEY (`activity_id`,`material_id`,`material_uuid`,`user_cooc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_adaptive_pdf_result` */

DROP TABLE IF EXISTS `student_adaptive_pdf_result`;

CREATE TABLE `student_adaptive_pdf_result` (
  `activity_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `material_uuid` varchar(40) NOT NULL,
  `page` int(11) NOT NULL,
  `time_spent` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `last_access_date` datetime DEFAULT NULL,
  `user_cooc_id` varchar(40) NOT NULL,
  `access_counts` int(11) DEFAULT NULL,
  PRIMARY KEY (`activity_id`,`material_id`,`material_uuid`,`page`,`user_cooc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_article` */

DROP TABLE IF EXISTS `student_article`;

CREATE TABLE `student_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `material_id` int(11) DEFAULT NULL COMMENT '教材ID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `article` longtext COMMENT '文章',
  `save_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '儲存日期',
  `time_spent` int(11) DEFAULT NULL COMMENT '花費時間',
  `material_uuid` varchar(40) DEFAULT NULL COMMENT '教材UUID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `student_assessment_result` */

DROP TABLE IF EXISTS `student_assessment_result`;

CREATE TABLE `student_assessment_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `material_id` int(11) DEFAULT NULL COMMENT '教材ID',
  `score` int(11) DEFAULT NULL COMMENT '分數',
  `time_spent` int(11) DEFAULT NULL COMMENT '花費時間',
  `submit_date` datetime DEFAULT NULL COMMENT '繳交日期',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `material_uuid` varchar(40) DEFAULT NULL COMMENT '教材UUID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Table structure for table `student_learn_log` */

DROP TABLE IF EXISTS `student_learn_log`;

CREATE TABLE `student_learn_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `material_id` int(11) DEFAULT NULL COMMENT '教材ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '活動建立者酷課雲ID',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `start_date` datetime DEFAULT NULL COMMENT '開始學習時間',
  `end_date` datetime DEFAULT NULL COMMENT '結束學習時間',
  `duration` double DEFAULT NULL COMMENT '花費時間',
  `type` int(11) DEFAULT NULL COMMENT '教材類型',
  `start_point` int(11) DEFAULT NULL COMMENT '教材開始點',
  `end_point` int(11) DEFAULT NULL COMMENT '教材結束點',
  `action` varchar(512) DEFAULT NULL COMMENT '動作',
  `page` int(11) DEFAULT NULL COMMENT 'PDF/電子書頁面/影片時間點',
  `note` longtext COMMENT '筆記內容',
  `category` varchar(20) DEFAULT NULL COMMENT 'LOG類別',
  `time_spent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=672 DEFAULT CHARSET=utf8;

/*Table structure for table `student_learn_note` */

DROP TABLE IF EXISTS `student_learn_note`;

CREATE TABLE `student_learn_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `activity_id` int(11) DEFAULT NULL COMMENT '活動ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '活動建立者ID',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '活動建立者酷課雲ID',
  `material_id` int(11) DEFAULT NULL COMMENT '教材ID',
  `point` int(11) DEFAULT NULL COMMENT '教材時間點',
  `complete_date` datetime DEFAULT NULL COMMENT '筆記完成時間',
  `note` longtext COMMENT '筆記內容',
  `type` int(11) DEFAULT NULL COMMENT '教材類型',
  `user_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否刪除',
  `delete_date` datetime DEFAULT NULL COMMENT '刪除日期',
  `update_date` datetime DEFAULT NULL COMMENT '更新日期',
  `material_uuid` varchar(40) DEFAULT NULL COMMENT '教材UUID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_material_learn` */

DROP TABLE IF EXISTS `student_material_learn`;

CREATE TABLE `student_material_learn` (
  `activity_id` int(11) NOT NULL COMMENT '活動ID',
  `material_id` int(11) NOT NULL COMMENT '教材ID',
  `material_uuid` varchar(256) DEFAULT NULL COMMENT '適性教材UUID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `user_cooc_id` varchar(40) NOT NULL COMMENT '使用者酷課雲ID',
  `start_date` datetime DEFAULT NULL COMMENT '使用者進入活動日期',
  `completion_rate` int(11) DEFAULT '0' COMMENT '活動完成度',
  `is_complete` tinyint(1) DEFAULT '0' COMMENT '教材是否完成',
  `time_spent` int(11) DEFAULT '0' COMMENT '花費時間',
  `completion_date` datetime DEFAULT NULL COMMENT '完成日期',
  `last_access_date` datetime DEFAULT NULL COMMENT '最近一次存取時間',
  `access_counts` int(11) DEFAULT NULL COMMENT '瀏覽次數',
  `score` int(11) DEFAULT '0' COMMENT '分數',
  PRIMARY KEY (`activity_id`,`material_id`,`user_cooc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `student_pdf_result` */

DROP TABLE IF EXISTS `student_pdf_result`;

CREATE TABLE `student_pdf_result` (
  `activity_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `time_spent` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `last_access_date` datetime DEFAULT NULL,
  `user_cooc_id` varchar(40) NOT NULL,
  `access_counts` int(11) DEFAULT NULL,
  `material_uuid` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`activity_id`,`material_id`,`page`,`user_cooc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `teach_group` */

DROP TABLE IF EXISTS `teach_group`;

CREATE TABLE `teach_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `name` varchar(20) DEFAULT NULL COMMENT '群組名稱',
  `description` text COMMENT '群組描述',
  `creator_id` int(11) DEFAULT NULL COMMENT '群組建立者ID',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '群組建立日期',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '群組是否被刪除',
  `creator_cooc_id` varchar(40) DEFAULT NULL COMMENT '使用者酷課雲ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
