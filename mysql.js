// mysql connection
var config = require('./config');
var mysql = require('mysql');

var pool = null;

exports.connect = function(done){
    pool = mysql.createPool({
        host: config.mysql.host,
        user: config.mysql.user,
        port:(config.mysql.port)?config.mysql.port:'3306',
        password: config.mysql.password,
        database: config.mysql.database,
        multipleStatements:true
    });
    done();
};

exports.get = function(){
    return pool;
};

