var multer = require('multer');
var md5 = require('md5');
var config = require('../config');

//檔案儲存
var storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, 'public/uploads');
    },
    filename: function(req, file, cb){
        var fileFormat = (file.originalname).split(".");
        var fileCode = md5(file);
        cb(null, fileCode + "." + fileFormat[fileFormat.length - 1]);
    }
});

//檔案過濾
var videoFilter = function(req, file, cb){
    if (!file.originalname.match(/\.(mp4)$/)) {
        return cb(new Error('Only mp4 files are allowed!'), false);
    }
    cb(null, true);
};
var pdfFilter = function(req, file, cb){
    if (!file.originalname.match(/\.(pdf)$/)) {
        return cb(new Error('Only pdf files are allowed!'), false);
    }
    cb(null, true);
};
var imageFilter = function(req, file, cb){
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

//添加配置文件到muler对象。
var uploadVideo = multer({
    storage: storage,
    fileFilter: videoFilter,
    limits:{
        fileSize:4*1024*1024
    }
});
var uploadPdf = multer({
    storage: storage,
    fileFilter: pdfFilter
});
var uploadImage = multer({
    storage: storage,
    fileFilter: imageFilter
});
var upload = {
    video:uploadVideo,
    pdf:pdfFilter,
    image:imageFilter
};

//导出对象
module.exports = upload;