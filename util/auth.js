//處理酷課雲SSO
var config = require('../config');
var request = require('request');
var sha1 = function(input){
    return require('crypto').createHash('sha1').update(input).digest('hex')
}
var parseString = require('xml2js').parseString;

function getHttpResult(targetPath,done){
    request(config.cooc.sso_url+targetPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            done(null,body);
        }else{
            done(error,body);
        }
    });
}

function computeSign(client_id,client_secret,queryParams){
    var code = client_id;
    for(queryKey in queryParams){
        code += queryKey;
        code += queryParams[queryKey];
    }
    code += client_secret;
    return sha1(code);
}

module.exports.getAccessToken = function(token,redirect_uri,done){
    var targetPath = 'oauth2/oauth/token?' 
                    + 'grant_type=authorization_code' 
                    + '&client_id=' + config.cooc.client_id
				    + '&redirect_uri=' + redirect_uri 
                    + '&client_secret=' + config.cooc.client_secret
                    + '&code=' + token;
    getHttpResult(targetPath,done);
};

module.exports.getUserInfo = function(accessToken,done){
    var queryParams = {
        accessToken:accessToken
    };
    var sign = computeSign(config.cooc.client_id,config.cooc.client_secret,queryParams);
    var getAccountInfoPath = config.cooc.sso_url+(config.cooc.get_account_path+'?accessToken='+accessToken+'&appKey='+config.cooc.client_id+'&sign='+sign);
    request(getAccountInfoPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            parseString(body, function (parseError, jsonResult) {
                if(parseError) done(parseError);
                done(null,jsonResult);
            });          
        }else{
            done(err,body);
        }
    });
}