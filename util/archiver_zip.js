var fs = require('fs');
var path = require('path');
var archiver = require('archiver');

module.exports.zipActivity = function(name, done){
    var zipDate = new Date();
    
    var os = fs.createWriteStream(path.resolve('zip')+'/'+name+'_'+zipDate.getTime()+'.zip');
    var zipper = archiver('zip');

    os.on('close', function(){
        done(null, zipper.pointer());
    });

    zipper.on('error', function(error){
        throw error;
    });

    zipper.pipe(os);
    /*
    var file = path.resolve('uploads')+'/activity/35/material/b0cce79a3146811e12b61c30acaccaf3_1503566663752.pdf'
    zipper.append(file, { name:'cp.pdf' });
    */
    /*
    zipper.directory(path.resolve('uploads'),path.resolve('zip'));
    */
    zipper.finalize();
    
};