# Adaptive-Dashboard (WAT 2.0)  

## Requirement
* Nodejs 6+
* MySQL 5+
* Mongo DB 3+  

## Introduction  
* IP: 140.92.18.219
* Site: http://140.92.18.219:3000
* Repository: https://gitlab.com/leaverij/adaptive-dashboard.git
* Issue Tracking: https://gitlab.com/leaverij/adaptive-dashboard/issues  

## Settings  
### Make Nodejs App run forever
* Use node module 'pm2'
```
npm install --save pm2
```
* change to the project directory and start pm2
```
pm2 start bin/www
```

### Make pm2 run on server startup
* change to the project directory and start pm2
```
pm2 start bin/www
```
* auto generate system startup file
```
pm2 startup
```
* save it
```
pm2 save
```  

## Login Information  

Real Teacher
* Account:chuan0613@gmail.com
* Password:26379521  

* Account:cooctp001@gmail.com
* Password:@dmin27535316  


Student
* Account:cooctp007@gmail.com
* Password:cooc123456

Test Teacher
* cooctp001@gmail.com
* @dmin27535316

